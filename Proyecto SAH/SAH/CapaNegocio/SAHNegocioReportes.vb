﻿Imports System.Data.SqlClient

Public Class SAHNegocioReportes

    Dim dataReportes As New SAHDataReportes

    Function getTablaAutorizadosAhorroEscolar() As DataTable
        Return dataReportes.getTablaAutorizadosAhorroEscolar()
    End Function

    Function getTablaAutorizadosOtrosAhorros() As DataTable
        Return dataReportes.getTablaAutorizadosOtrosAhorros()
    End Function

    Function getTablaBeneficiariosAhorroEscolar() As DataTable
        Return dataReportes.getTablaBeneficiariosAhorroEscolar()
    End Function

    Function getTablaBeneficiariosOtrosAhorros() As DataTable
        Return dataReportes.getTablaBeneficiariosOtrosAhorros()
    End Function

    Function getTablaBeneficiariosAsociado(idAsociado As Integer) As DataTable
        Return dataReportes.getTablaBeneficiariosAsociado(idAsociado)
    End Function

    Function getTablaBloqueosFechaEstado(tipo As String) As DataTable
        Return dataReportes.getTablaBloqueosFechaEstado(tipo)
    End Function

    Function getTablaCertificadosCancelados() As DataTable
        Return dataReportes.getTablaCertificadosCancelados()
    End Function

    Function getTablaCertificadosCustodia() As DataTable
        Return dataReportes.getTablaCertificadosCustodia()
    End Function

    Function getTablaCertificadosAsociado(idAsociado As Integer) As DataTable
        Return dataReportes.getTablaCertificadosAsociado(idAsociado)
    End Function

    Function getTablaCertificadosAsociadoMoneda(idAsociado As Integer, moneda As String, consulta As String) As DataTable
        Return dataReportes.getTablaCertificadosAsociadoMoneda(idAsociado, moneda, consulta)
    End Function

    Function getTablaCertificadosVencidos() As DataTable
        Return dataReportes.getTablaCertificadosVencidos()
    End Function

    Function getTablaCuentasActivasAsociado(idAsociado As Integer) As DataTable
        Return dataReportes.getTablaCuentasActivasAsociado(idAsociado)
    End Function

    Function getTablaCuentasAhorroEscolar() As DataTable
        Return dataReportes.getTablaCuentasAhorroEscolar()
    End Function

    Function getTablaCuponesAsociadoCertificado(id As Integer, consulta As String) As DataTable
        Return dataReportes.getTablaCuponesAsociadoCertificado(id, consulta)
    End Function

    Function getTablaRangosCalculoInteres(consulta As String) As DataTable
        Return dataReportes.getTablaRangosCalculoInteres(consulta)
    End Function

    Function getTablaCertificadosReimpresos() As DataTable
        Return dataReportes.getTablaCertificadosReimpresos()
    End Function

    Function getTablaCastigosAtrasos() As DataTable
        Return dataReportes.getTablaCastigosAtrasos()
    End Function

    Function getTablaDescuentosRetiroAnticipado() As DataTable
        Return dataReportes.getTablaDescuentosRetiroAnticipado()
    End Function

    Function getTablaTransaccionesCuentaTipoAhorro(consulta As String, id As Integer) As DataTable
        Return dataReportes.getTablaTransaccionesCuentaTipoAhorro(consulta, id)
    End Function

    Function getTablaVencimientoCertificados() As DataTable
        Return dataReportes.getTablaVencimientoCertificados()
    End Function

    Function getTablaVencimientoCupones() As DataTable
        Return dataReportes.getTablaVencimientoCupones()
    End Function
End Class

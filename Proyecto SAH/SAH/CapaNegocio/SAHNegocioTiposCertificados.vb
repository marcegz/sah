﻿Imports System.Data.SqlClient

Public Class SAHNegocioTiposCertificados

    Dim dataTiposCertificados As New SAHDataTiposCertificados

    Function insertarTipoCertificado(tipoCertificado As SAHTiposCertificados) As String
        Return dataTiposCertificados.insertarTipoCertificado(tipoCertificado)
    End Function


    Function cargarTiposCertificadosTabla() As DataTable
        cargarTiposCertificadosTabla = dataTiposCertificados.cargarTiposCertificadosTabla
    End Function

    Function CodigoNuevoTiposCertificados() As Integer
        Return dataTiposCertificados.CodigoNuevoIdTipoCertificado()
    End Function

    Function getTablaConsultaTiposCertificados(nombre As String) As DataTable
        Return dataTiposCertificados.getTablaConsultaTiposCertificados(nombre)
    End Function

    Function getTipoCertificado(idTipoCertificado As Integer) As SAHTiposCertificados
        Return dataTiposCertificados.getTipoCertificado(idTipoCertificado)
    End Function

    Function actualizarTipoCertificado(tipoCertificado As SAHTiposCertificados) As String
        Return dataTiposCertificados.actualizarTipoCertificado(tipoCertificado)
    End Function

End Class

﻿Imports System.Data.SqlClient

Public Class SAHNegocioAhorros

    Dim dataAhorros As New SAHDataAhorros

    Function insertarAhorro(ahorro As SAHAhorros) As String
        Return dataAhorros.insertarAhorro(ahorro)
    End Function

    Function CodigoNuevoAhorro() As Integer
        CodigoNuevoAhorro = dataAhorros.CodigoNuevoAhorro()
    End Function

    Function getNombre(cedula As String) As String
        Return dataAhorros.getNombre(cedula)
    End Function

    Function getIdAsociado(cedula As String) As Integer
        Return dataAhorros.getIdAsociado(cedula)
    End Function

    Function getIdPersona(cedula As String) As Integer
        Return dataAhorros.getIdPersona(cedula)
    End Function

    Function getAhorro(idAhorro As Integer) As SAHAhorros
        Return dataAhorros.getAhorro(idAhorro)
    End Function

    Function getTablaConsultaAhorros(consulta As String) As DataTable
        Return dataAhorros.getTablaConsultaAhorros(consulta)
    End Function

    Function consultarAhorros() As List(Of SAHAhorros)
        Return dataAhorros.consultaAhorros
    End Function


    Function actualizarAhorro(idAhorro As Integer, nombre As String, estado As String, idAutorizado As Integer) As String
        Return dataAhorros.actualizarAhorro(idAhorro, nombre, estado, idAutorizado)
    End Function

    Function verificarUsuarioAdmin(idUsuario As Integer) As Boolean
        Return dataAhorros.verificarUsuarioAdmin(idUsuario)
    End Function

    Function verificarUsuarioAhorros(idUsuario As Integer) As Boolean
        Return dataAhorros.verificarUsuarioAhorros(idUsuario)
    End Function

    Function getAhorrosCombo() As DataTable
        Return dataAhorros.getAhorrosCombo()
    End Function
End Class

﻿Public Class SAHNegocioBloqueos

    Dim dataBloqueos As New SAHDataBloqueos

    Sub insertarBloqueo(bloqueo As SAHBloqueos)
        dataBloqueos.insertarBloqueo(bloqueo)
    End Sub

    Function getListaBloqueosAhorros(idAhorro As Integer) As DataTable
        Return dataBloqueos.getListaBloqueosAhorros(idAhorro)
    End Function

    Function getTablaConsultaBloqueos(consulta As String) As DataTable
        Return dataBloqueos.getTablaConsultaBloqueos(consulta)
    End Function

    Function getTablaBloqueos(nombre As String) As DataTable
        Return dataBloqueos.getTablaBloqueos(nombre)
    End Function

    Sub generarBloqueo()
        dataBloqueos.generarBloqueo()
    End Sub

End Class

﻿Imports System.Data.SqlClient

Public Class SAHNegocioDescuentosCastigos

    Dim dataDescuentosCastigos As New SAHDataDescuentosCatigos

    Function insertarDescuentosCatigos(descuentoCastigo As Descuentos_Castigos) As String
        Return dataDescuentosCastigos.insertarDescuentosCatigos(descuentoCastigo)
    End Function

    Function CodigoNuevoDescuentoCastigo() As Integer
        Return dataDescuentosCastigos.CodigoNuevoDescuentoCastigo()
    End Function

    Function getTablaConsultaDescuentosCastigos() As DataTable
        Return dataDescuentosCastigos.getTablaConsultaDescuentosCastigos()
    End Function

    Sub actualizarTasaInteres(idDescuentos_Castigos As Integer, monto_aplicado As Integer)
        dataDescuentosCastigos.actualizarTasaInteres(idDescuentos_Castigos, monto_aplicado)
    End Sub

End Class

﻿Imports System.Data.SqlClient

Public Class SAHNegocioCertificados

    Dim dataCertificados As New SAHDataCertificados


    Function insertarCertificado(certificado As SAHCertificados) As String
        Return dataCertificados.insertarCertificado(certificado)
    End Function


    Function CodigoNuevoIDCertificado() As Integer
        Return dataCertificados.CodigoNuevoIDCertificado()
    End Function

    Function getIdCertificadoPlazo(idAhorro As Integer) As Integer
        Return dataCertificados.getIdCertificadoPlazo(idAhorro)
    End Function

    Function getTablaConsultaCertificados(nombre As String) As DataTable
        Return dataCertificados.getTablaConsultaCertificados(nombre)
    End Function

    Function getCertificado(idCertificado As Integer) As SAHCertificados
        Return dataCertificados.getCertificado(idCertificado)
    End Function

    Function actualizarCertificado(idCertificado As Integer, fechaVencimiento As Date, estadoVencimiento As String) As String
        Return dataCertificados.actualizarCertificado(idCertificado, fechaVencimiento, estadoVencimiento)
    End Function


End Class

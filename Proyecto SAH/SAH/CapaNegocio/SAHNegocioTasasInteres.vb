﻿Public Class SAHNegocioTasasInteres

    Dim dataTasasInteres As New SAHDataTasasInteres

    Sub insertarTasaInteres(tasaInteres As SAHTasasInteres)
        dataTasasInteres.insertarTasaInteres(tasaInteres)
    End Sub

    Function getPorcentaje(duracion As Integer, idTipoAhorro As Integer) As Double
        Return dataTasasInteres.getPorcentaje(duracion, idTipoAhorro)
    End Function


    Function getTablaConsultaTasasInteres(idTipoAhorro) As DataTable
        Return dataTasasInteres.getTablaConsultaTasasInteres(idTipoAhorro)
    End Function

    Sub actualizarTasaInteres(idTasaInteres As Integer, porcentaje As Double)
        dataTasasInteres.actualizarTasaInteres(idTasaInteres, porcentaje)
    End Sub

    Function verificarPorcentajesTipoAhorro(idTipoAhorro As Integer) As Boolean
        Return dataTasasInteres.verificarPorcentajesTipoAhorro(idTipoAhorro)
    End Function

    Function getTablaTasasInteres(nombre As String) As DataTable
        Return dataTasasInteres.getTablaTasasInteres(nombre)
    End Function

End Class

﻿Public Class SAHNegocioCupones

    Dim dataCupones As New SAHDataCupones

    Sub insertarCupon(cupon As SAHCupones)
        dataCupones.insertarCupon(cupon)
    End Sub

    Function getIntereses(idCertificado As Integer) As Double
        Return dataCupones.getIntereses(idCertificado)
    End Function

    Function getListaFechasCupones(fechaInicio As Date, fechaFinal As Date, frecuencia As Integer) As List(Of Date)
        Return dataCupones.getListaFechasCupones(fechaInicio, fechaFinal, frecuencia)
    End Function

    Function verificarExisteCupon(idCertificado As Integer, fechaGenerado As Date) As Boolean
        Return dataCupones.verificarExisteCupon(idCertificado, fechaGenerado)
    End Function

    Sub generarCupones()
        dataCupones.generarCupones()
    End Sub

    Function getTablaConsultaCupones(nombre As String) As DataTable
        Return dataCupones.getTablaConsultaCupones(nombre)
    End Function

End Class

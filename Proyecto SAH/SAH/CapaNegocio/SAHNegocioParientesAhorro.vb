﻿Imports System.Data.SqlClient

Public Class SAHNegocioParientesAhorro

    Dim dataParientesAhorro As New SAHDataParientesAhorros

    Sub insertarParienteAhorro(idPariente As Integer, idAhorro As Integer)
        dataParientesAhorro.insertarParienteAhorro(idPariente, idAhorro)
    End Sub

    Sub eliminarParienteAhorro(idPariente As Integer, idAhorro As Integer)
        dataParientesAhorro.eliminarParienteAhorro(idPariente, idAhorro)
    End Sub

    Function verificarExisteParienteAhorro(idPariente As Integer, idAhorro As Integer) As Boolean
        Return dataParientesAhorro.verificarExisteParienteAhorro(idPariente, idAhorro)
    End Function

    Function getListaParientesAsociado(idAsociado As Integer) As DataTable
        Return dataParientesAhorro.getListaParientesAsociado(idAsociado)
    End Function

    Function getCedulaPariente(idPariente As Integer) As String
        Return dataParientesAhorro.getCedulaPariente(idPariente)
    End Function

    Function getIdPariente(idPersona As Integer) As Integer
        Return dataParientesAhorro.getIdPariente(idPersona)
    End Function

End Class

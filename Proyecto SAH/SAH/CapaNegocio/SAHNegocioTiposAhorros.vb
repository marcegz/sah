﻿Imports System.Data.SqlClient

Public Class SAHNegocioTiposAhorros


    Dim dataTiposAhorros As New SAHDataTiposAhorros


    Function insertarTipoAhorro(tipoAhorro As SAHTiposAhorros) As Integer
        Return dataTiposAhorros.insertarTipoAhorro(tipoAhorro)
    End Function

    Function CodigoNuevoTiposAhorros() As Integer
        CodigoNuevoTiposAhorros = dataTiposAhorros.CodigoNuevoTipoAhorro()
    End Function


    Function getTablaConsultaTiposAhorros(nombre As String) As DataTable
        Return dataTiposAhorros.getTablaConsultaTiposAhorros(nombre)
    End Function

    Function getTipoAhorro(idTipoAhorro As Integer) As SAHTiposAhorros
        Return dataTiposAhorros.getTipoAhorro(idTipoAhorro)
    End Function

    Function getTiposAhorrosActivos() As DataTable
        Return dataTiposAhorros.getTiposAhorrosActivos()
    End Function

    Function getFecha(nombre As String) As Date
        Return dataTiposAhorros.getFecha(nombre)
    End Function

    Function getIdTipoAhorro(nombre As String) As Integer
        Return dataTiposAhorros.getIdTipoAhorro(nombre)
    End Function

    Function getPersonalizado(nombre As String) As Boolean
        Return dataTiposAhorros.getPersonalizado(nombre)
    End Function

    Sub actualizarEstadoTipoAhorro(idTipoAhorro As Integer, estado As Boolean)
        dataTiposAhorros.actualizarEstadoTipoAhorro(idTipoAhorro, estado)
    End Sub

    Function actualizarTipoAhorro(idTipoAhorro As Integer, nombre As String, estado As Integer, fechaEntrega As Date, personalizado As Boolean) As Integer
        Return dataTiposAhorros.actualizarTipoAhorro(idTipoAhorro, nombre, estado, fechaEntrega, personalizado)
    End Function

End Class

﻿Public Class SAHTiposAhorros

    Private idTipoAhorro As Integer
    Private nombre As String
    Private estado As Boolean
    Private fechaEntrega? As Date
    Private personalizado As Boolean

    Public Sub New()
    End Sub

    Public Sub New(idTipoAhorro As Integer, nombre As String, estado As Boolean, fechaEntrega As Date?, personalizado As Boolean)
        Me.IdTipoAhorro1 = idTipoAhorro
        Me.Nombre1 = nombre
        Me.Estado1 = estado
        Me.FechaEntrega1 = fechaEntrega
        Me.Personalizado1 = personalizado
    End Sub

    Public Sub New(nombre As String, estado As Boolean, fechaEntrega As Date?, personalizado As Boolean)
        Me.Nombre1 = nombre
        Me.Estado1 = estado
        Me.FechaEntrega1 = fechaEntrega
        Me.Personalizado1 = personalizado
    End Sub

    Public Property IdTipoAhorro1 As Integer
        Get
            Return idTipoAhorro
        End Get
        Set(value As Integer)
            idTipoAhorro = value
        End Set
    End Property

    Public Property Nombre1 As String
        Get
            Return nombre
        End Get
        Set(value As String)
            nombre = value
        End Set
    End Property

    Public Property Estado1 As Boolean
        Get
            Return estado
        End Get
        Set(value As Boolean)
            estado = value
        End Set
    End Property

    Public Property FechaEntrega1 As Date?
        Get
            Return fechaEntrega
        End Get
        Set(value As Date?)
            fechaEntrega = value
        End Set
    End Property

    Public Property Personalizado1 As Boolean
        Get
            Return personalizado
        End Get
        Set(value As Boolean)
            personalizado = value
        End Set
    End Property
End Class

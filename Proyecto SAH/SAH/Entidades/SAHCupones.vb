﻿Public Class SAHCupones

    Private idCupon As Integer
    Private fechaGenerado As Date
    Private fechaVencimiento As Date
    Private estadoVencimiento As String
    Private intereses As Double
    Private monto As Integer
    Private pago As Boolean
    Private idCertificado As Integer

    Public Sub New()
    End Sub

    Public Sub New(idCupon As Integer, fechaGenerado As Date, fechaVencimiento As Date, estadoVencimiento As String, intereses As Double, monto As Integer, pago As Boolean, idCertificado As Integer)
        Me.IdCupon1 = idCupon
        Me.FechaGenerado1 = fechaGenerado
        Me.FechaVencimiento1 = fechaVencimiento
        Me.EstadoVencimiento1 = estadoVencimiento
        Me.Intereses1 = intereses
        Me.Monto1 = monto
        Me.Pago1 = pago
        Me.IdCertificado1 = idCertificado
    End Sub

    Public Sub New(fechaGenerado As Date, fechaVencimiento As Date, estadoVencimiento As String, intereses As Double, monto As Integer, pago As Boolean, idCertificado As Integer)
        Me.FechaGenerado1 = fechaGenerado
        Me.FechaVencimiento1 = fechaVencimiento
        Me.EstadoVencimiento1 = estadoVencimiento
        Me.Intereses1 = intereses
        Me.Monto1 = monto
        Me.Pago1 = pago
        Me.IdCertificado1 = idCertificado
    End Sub

    Public Property IdCupon1 As Integer
        Get
            Return idCupon
        End Get
        Set(value As Integer)
            idCupon = value
        End Set
    End Property

    Public Property FechaGenerado1 As Date
        Get
            Return fechaGenerado
        End Get
        Set(value As Date)
            fechaGenerado = value
        End Set
    End Property

    Public Property FechaVencimiento1 As Date
        Get
            Return fechaVencimiento
        End Get
        Set(value As Date)
            fechaVencimiento = value
        End Set
    End Property

    Public Property EstadoVencimiento1 As String
        Get
            Return estadoVencimiento
        End Get
        Set(value As String)
            estadoVencimiento = value
        End Set
    End Property

    Public Property Intereses1 As Double
        Get
            Return intereses
        End Get
        Set(value As Double)
            intereses = value
        End Set
    End Property

    Public Property Monto1 As Integer
        Get
            Return monto
        End Get
        Set(value As Integer)
            monto = value
        End Set
    End Property

    Public Property Pago1 As Boolean
        Get
            Return pago
        End Get
        Set(value As Boolean)
            pago = value
        End Set
    End Property

    Public Property IdCertificado1 As Integer
        Get
            Return idCertificado
        End Get
        Set(value As Integer)
            idCertificado = value
        End Set
    End Property
End Class

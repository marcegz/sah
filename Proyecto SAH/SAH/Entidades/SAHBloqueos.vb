﻿Public Class SAHBloqueos

    Private idBloqueo As Integer
    Private tipo As String
    Private descripcion As String
    Private fecha As Date
    Private idAhorro As Integer

    Public Sub New()
    End Sub

    Public Sub New(idBloqueo As Integer, tipo As String, descripcion As String, fecha As Date, idAhorro As Integer)
        Me.IdBloqueo1 = idBloqueo
        Me.Tipo1 = tipo
        Me.Descripcion1 = descripcion
        Me.Fecha1 = fecha
        Me.IdAhorro1 = idAhorro
    End Sub

    Public Sub New(tipo As String, descripcion As String, fecha As Date, idAhorro As Integer)
        Me.Tipo1 = tipo
        Me.Descripcion1 = descripcion
        Me.Fecha1 = fecha
        Me.IdAhorro1 = idAhorro
    End Sub

    Public Property IdBloqueo1 As Integer
        Get
            Return idBloqueo
        End Get
        Set(value As Integer)
            idBloqueo = value
        End Set
    End Property

    Public Property Tipo1 As String
        Get
            Return tipo
        End Get
        Set(value As String)
            tipo = value
        End Set
    End Property

    Public Property Descripcion1 As String
        Get
            Return descripcion
        End Get
        Set(value As String)
            descripcion = value
        End Set
    End Property

    Public Property Fecha1 As Date
        Get
            Return fecha
        End Get
        Set(value As Date)
            fecha = value
        End Set
    End Property

    Public Property IdAhorro1 As Integer
        Get
            Return idAhorro
        End Get
        Set(value As Integer)
            idAhorro = value
        End Set
    End Property
End Class

﻿Public Class SAHCertificados

    Private idCertificado As Integer
    Private fechaGenerado As Date
    Private nombreAhorro As String
    Private nombreTipoCertificado As String
    Private fechaVencimiento As Date
    Private estadoVencimiento As String
    Private reimpreso As Integer
    Private pago As Boolean
    Private idAhorro As Integer
    Private idTipoCertificado As Integer

    Public Sub New()
    End Sub

    Public Sub New(idCertificado As Integer, fechaGenerado As Date, fechaVencimiento As Date, estadoVencimiento As String, reimpreso As Integer, pago As Boolean, idAhorro As Integer, idTipoCertificado As Integer)
        Me.IdCertificado1 = idCertificado
        Me.FechaGenerado1 = fechaGenerado
        Me.FechaVencimiento1 = fechaVencimiento
        Me.EstadoVencimiento1 = estadoVencimiento
        Me.Reimpreso1 = reimpreso
        Me.Pago1 = pago
        Me.IdAhorro1 = idAhorro
        Me.IdTipoCertificado1 = idTipoCertificado
    End Sub

    Public Sub New(idCertificado As Integer, fechaGenerado As Date, fechaVencimiento As Date, estadoVencimiento As String, nombreAhorro As String, nombreTipoCertificado As String)
        Me.IdCertificado1 = idCertificado
        Me.FechaGenerado1 = fechaGenerado
        Me.FechaVencimiento1 = fechaVencimiento
        Me.EstadoVencimiento1 = estadoVencimiento
        Me.NombreAhorro1 = nombreAhorro
        Me.NombreTipoCertificado1 = nombreTipoCertificado
    End Sub


    Public Sub New(fechaGenerado As Date, fechaVencimiento As Date, estadoVencimiento As String, reimpreso As Integer, pago As Boolean, idAhorro As Integer, idTipoCertificado As Integer)
        Me.FechaGenerado1 = fechaGenerado
        Me.FechaVencimiento1 = fechaVencimiento
        Me.EstadoVencimiento1 = estadoVencimiento
        Me.Reimpreso1 = reimpreso
        Me.Pago1 = pago
        Me.IdAhorro1 = idAhorro
        Me.IdTipoCertificado1 = idTipoCertificado
    End Sub

    Public Property IdCertificado1 As Integer
        Get
            Return idCertificado
        End Get
        Set(value As Integer)
            idCertificado = value
        End Set
    End Property

    Public Property FechaGenerado1 As Date
        Get
            Return fechaGenerado
        End Get
        Set(value As Date)
            fechaGenerado = value
        End Set
    End Property

    Public Property FechaVencimiento1 As Date
        Get
            Return fechaVencimiento
        End Get
        Set(value As Date)
            fechaVencimiento = value
        End Set
    End Property

    Public Property EstadoVencimiento1 As String
        Get
            Return estadoVencimiento
        End Get
        Set(value As String)
            estadoVencimiento = value
        End Set
    End Property

    Public Property Reimpreso1 As Integer
        Get
            Return reimpreso
        End Get
        Set(value As Integer)
            reimpreso = value
        End Set
    End Property

    Public Property Pago1 As Boolean
        Get
            Return pago
        End Get
        Set(value As Boolean)
            pago = value
        End Set
    End Property

    Public Property IdAhorro1 As Integer
        Get
            Return idAhorro
        End Get
        Set(value As Integer)
            idAhorro = value
        End Set
    End Property

    Public Property IdTipoCertificado1 As Integer
        Get
            Return idTipoCertificado
        End Get
        Set(value As Integer)
            idTipoCertificado = value
        End Set
    End Property

    Public Property NombreAhorro1 As String
        Get
            Return nombreAhorro
        End Get
        Set(value As String)
            nombreAhorro = value
        End Set
    End Property

    Public Property NombreTipoCertificado1 As String
        Get
            Return nombreTipoCertificado
        End Get
        Set(value As String)
            nombreTipoCertificado = value
        End Set
    End Property
End Class

﻿Public Class SAHTasasInteres

    Dim idTasaInteres As Integer
    Dim rangoDias As String
    Dim rangoMeses As String
    Dim porcentaje As Double
    Dim idTipoAhorro As Integer

    Public Sub New()
    End Sub

    Public Sub New(idTasaInteres As Integer, rangoDias As String, rangoMeses As String, porcentaje As Double, idTipoAhorro As Integer)
        Me.IdTasaInteres1 = idTasaInteres
        Me.RangoDias1 = rangoDias
        Me.RangoMeses1 = rangoMeses
        Me.Porcentaje1 = porcentaje
        Me.IdTipoAhorro1 = idTipoAhorro
    End Sub

    Public Sub New(rangoDias As String, rangoMeses As String, porcentaje As Double, idTipoAhorro As Integer)
        Me.RangoDias1 = rangoDias
        Me.RangoMeses1 = rangoMeses
        Me.Porcentaje1 = porcentaje
        Me.IdTipoAhorro1 = idTipoAhorro
    End Sub

    Public Property IdTasaInteres1 As Integer
        Get
            Return idTasaInteres
        End Get
        Set(value As Integer)
            idTasaInteres = value
        End Set
    End Property

    Public Property RangoDias1 As String
        Get
            Return rangoDias
        End Get
        Set(value As String)
            rangoDias = value
        End Set
    End Property

    Public Property RangoMeses1 As String
        Get
            Return rangoMeses
        End Get
        Set(value As String)
            rangoMeses = value
        End Set
    End Property

    Public Property Porcentaje1 As Double
        Get
            Return porcentaje
        End Get
        Set(value As Double)
            porcentaje = value
        End Set
    End Property

    Public Property IdTipoAhorro1 As Integer
        Get
            Return idTipoAhorro
        End Get
        Set(value As Integer)
            idTipoAhorro = value
        End Set
    End Property
End Class

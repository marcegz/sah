﻿Public Class Descuentos_Castigos

    Private idDescuentos_Castigos As Integer
    Private tipo As Boolean
    Private descripcion As String
    Private atrasado_adelantado As Integer
    Private monto_aplicado As Integer
    Private estado As Boolean

    Public Sub New(idDescuentos_Castigos As Integer, tipo As Boolean, descripcion As String, atrasado_adelantado As Integer, monto_aplicado As Integer, estado As Boolean)
        Me.IdDescuentos_Castigos1 = idDescuentos_Castigos
        Me.Tipo1 = tipo
        Me.Descripcion1 = descripcion
        Me.Atrasado_adelantado1 = atrasado_adelantado
        Me.Monto_aplicado1 = monto_aplicado
        Me.Estado1 = estado
    End Sub

    Public Sub New(tipo As Boolean, descripcion As String, atrasado_adelantado As Integer, monto_aplicado As Integer, estado As Boolean)
        Me.Tipo1 = tipo
        Me.Descripcion1 = descripcion
        Me.Atrasado_adelantado1 = atrasado_adelantado
        Me.Monto_aplicado1 = monto_aplicado
        Me.Estado1 = estado
    End Sub

    Public Sub New()
    End Sub


    Public Property IdDescuentos_Castigos1 As Integer
        Get
            Return idDescuentos_Castigos
        End Get
        Set(value As Integer)
            idDescuentos_Castigos = value
        End Set
    End Property

    Public Property Tipo1 As Boolean
        Get
            Return tipo
        End Get
        Set(value As Boolean)
            tipo = value
        End Set
    End Property

    Public Property Descripcion1 As String
        Get
            Return descripcion
        End Get
        Set(value As String)
            descripcion = value
        End Set
    End Property

    Public Property Atrasado_adelantado1 As Integer
        Get
            Return atrasado_adelantado
        End Get
        Set(value As Integer)
            atrasado_adelantado = value
        End Set
    End Property

    Public Property Monto_aplicado1 As Integer
        Get
            Return monto_aplicado
        End Get
        Set(value As Integer)
            monto_aplicado = value
        End Set
    End Property

    Public Property Estado1 As Boolean
        Get
            Return estado
        End Get
        Set(value As Boolean)
            estado = value
        End Set
    End Property



End Class

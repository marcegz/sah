﻿Public Class SAHAhorros

    Private idAhorro As Integer
    Private nombreAhorro As String
    Private nombreTipoAhorro As String
    Private cedula As String
    Private duracion As Integer
    Private fechaInicio As Date
    Private fechaFinal As Date
    Private diaAplicacion As Integer
    Private frecuenciaIntereses As Integer
    Private frecuenciaCuota As Integer
    Private moneda As String
    Private montoCuota As Double
    Private montoAhorrado As Double
    Private estado As String
    Private tipo_pago As Boolean
    Private idTipoAhorro As Integer
    Private idAsociado As Integer
    Private idAutorizado As Integer
    Private idEmpleado As Integer

    Public Sub New()
    End Sub

    Public Sub New(nombreAhorro As String, duracion As Integer, fechaInicio As Date, fechaFinal As Date, diaAplicacion As Integer, frecuenciaIntereses As Integer, frecuenciaCuota As Integer, moneda As String, montoCuota As Double, montoAhorrado As Double, estado As String, tipo_pago As Boolean, idTipoAhorro As Integer, idAsociado As Integer, idAutorizado As Integer, idEmpleado As Integer)
        Me.NombreAhorro1 = nombreAhorro
        Me.Duracion1 = duracion
        Me.FechaInicio1 = fechaInicio
        Me.FechaFinal1 = fechaFinal
        Me.DiaAplicacion1 = diaAplicacion
        Me.FrecuenciaIntereses1 = frecuenciaIntereses
        Me.FrecuenciaCuota1 = frecuenciaCuota
        Me.Moneda1 = moneda
        Me.MontoCuota1 = montoCuota
        Me.MontoAhorrado1 = montoAhorrado
        Me.Estado1 = estado
        Me.Tipo_pago1 = tipo_pago
        Me.IdTipoAhorro1 = idTipoAhorro
        Me.IdAsociado1 = idAsociado
        Me.IdAutorizado1 = idAutorizado
        Me.IdEmpleado1 = idEmpleado
    End Sub

    Public Sub New(idAhorro As Integer, nombreAhorro As String, nombreTipoAhorro As String, cedula As String,
                   duracion As Integer, fechaInicio As Date, fechaFinal As Date, diaAplicacion As Integer,
                   frecuenciaIntereses As Integer, frecuenciaCuota As Integer, moneda As String, montoCuota As Double,
                   montoAhorrado As Double, estado As String, tipo_pago As Boolean, idAutorizado As Integer, idTipoAhorro As Integer)
        Me.IdAhorro1 = idAhorro
        Me.NombreAhorro1 = nombreAhorro
        Me.NombreTipoAhorro1 = nombreTipoAhorro
        Me.Cedula1 = cedula
        Me.Duracion1 = duracion
        Me.FechaInicio1 = fechaInicio
        Me.FechaFinal1 = fechaFinal
        Me.DiaAplicacion1 = diaAplicacion
        Me.FrecuenciaIntereses1 = frecuenciaIntereses
        Me.FrecuenciaCuota1 = frecuenciaCuota
        Me.Moneda1 = moneda
        Me.MontoCuota1 = montoCuota
        Me.MontoAhorrado1 = montoAhorrado
        Me.Estado1 = estado
        Me.Tipo_pago1 = tipo_pago
        Me.IdAutorizado1 = idAutorizado
        Me.IdTipoAhorro1 = idTipoAhorro
    End Sub


    Public Sub New(idAhorro As Integer, fechaInicio As Date, fechaFinal As Date, duracion As Integer, frecuenciaIntereses As Integer, montoCuota As Double, idTipoAhorro As Integer)
        Me.IdAhorro1 = idAhorro
        Me.FechaInicio1 = fechaInicio
        Me.FechaFinal1 = fechaFinal
        Me.Duracion1 = duracion
        Me.FrecuenciaIntereses1 = frecuenciaIntereses
        Me.MontoCuota1 = montoCuota
        Me.IdTipoAhorro1 = idTipoAhorro
    End Sub

    Public Property IdAhorro1 As Integer
        Get
            Return idAhorro
        End Get
        Set(value As Integer)
            idAhorro = value
        End Set
    End Property

    Public Property NombreAhorro1 As String
        Get
            Return nombreAhorro
        End Get
        Set(value As String)
            nombreAhorro = value
        End Set
    End Property

    Public Property NombreTipoAhorro1 As String
        Get
            Return nombreTipoAhorro
        End Get
        Set(value As String)
            nombreTipoAhorro = value
        End Set
    End Property

    Public Property Cedula1 As String
        Get
            Return cedula
        End Get
        Set(value As String)
            cedula = value
        End Set
    End Property

    Public Property Duracion1 As Integer
        Get
            Return duracion
        End Get
        Set(value As Integer)
            duracion = value
        End Set
    End Property

    Public Property FechaInicio1 As Date
        Get
            Return fechaInicio
        End Get
        Set(value As Date)
            fechaInicio = value
        End Set
    End Property

    Public Property FechaFinal1 As Date
        Get
            Return fechaFinal
        End Get
        Set(value As Date)
            fechaFinal = value
        End Set
    End Property

    Public Property DiaAplicacion1 As Integer
        Get
            Return diaAplicacion
        End Get
        Set(value As Integer)
            diaAplicacion = value
        End Set
    End Property

    Public Property FrecuenciaIntereses1 As Integer
        Get
            Return frecuenciaIntereses
        End Get
        Set(value As Integer)
            frecuenciaIntereses = value
        End Set
    End Property

    Public Property FrecuenciaCuota1 As Integer
        Get
            Return frecuenciaCuota
        End Get
        Set(value As Integer)
            frecuenciaCuota = value
        End Set
    End Property

    Public Property Moneda1 As String
        Get
            Return moneda
        End Get
        Set(value As String)
            moneda = value
        End Set
    End Property

    Public Property MontoCuota1 As Double
        Get
            Return montoCuota
        End Get
        Set(value As Double)
            montoCuota = value
        End Set
    End Property

    Public Property MontoAhorrado1 As Double
        Get
            Return montoAhorrado
        End Get
        Set(value As Double)
            montoAhorrado = value
        End Set
    End Property

    Public Property Estado1 As String
        Get
            Return estado
        End Get
        Set(value As String)
            estado = value
        End Set
    End Property

    Public Property Tipo_pago1 As Boolean
        Get
            Return tipo_pago
        End Get
        Set(value As Boolean)
            tipo_pago = value
        End Set
    End Property


    Public Property IdAsociado1 As Integer
        Get
            Return idAsociado
        End Get
        Set(value As Integer)
            idAsociado = value
        End Set
    End Property

    Public Property IdAutorizado1 As Integer
        Get
            Return idAutorizado
        End Get
        Set(value As Integer)
            idAutorizado = value
        End Set
    End Property

    Public Property IdEmpleado1 As Integer
        Get
            Return idEmpleado
        End Get
        Set(value As Integer)
            idEmpleado = value
        End Set
    End Property

    Public Property IdTipoAhorro1 As Integer
        Get
            Return idTipoAhorro
        End Get
        Set(value As Integer)
            idTipoAhorro = value
        End Set
    End Property
End Class


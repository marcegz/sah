﻿Public Class SAHTiposCertificados

    Private idTipoCertificado As Integer
    Private nombre As String
    Private descripcion As String
    Private estado As Boolean
    Private monto As Integer

    Public Property IdTipoCertificado1 As Integer
        Get
            Return idTipoCertificado
        End Get
        Set(value As Integer)
            idTipoCertificado = value
        End Set
    End Property

    Public Property Nombre1 As String
        Get
            Return nombre
        End Get
        Set(value As String)
            nombre = value
        End Set
    End Property

    Public Property Descripcion1 As String
        Get
            Return descripcion
        End Get
        Set(value As String)
            descripcion = value
        End Set
    End Property

    Public Property Estado1 As Boolean
        Get
            Return estado
        End Get
        Set(value As Boolean)
            estado = value
        End Set
    End Property

    Public Property Monto1 As Integer
        Get
            Return monto
        End Get
        Set(value As Integer)
            monto = value
        End Set
    End Property

    Public Sub New()
    End Sub

    Public Sub New(idTipoCertificado As Integer, nombre As String, descripcion As String, estado As Boolean, monto As Integer)
        Me.IdTipoCertificado1 = idTipoCertificado
        Me.Nombre1 = nombre
        Me.Descripcion1 = descripcion
        Me.Estado1 = estado
        Me.Monto1 = monto
    End Sub


    Public Sub New(nombre As String, descripcion As String, estado As Boolean, monto As Integer)
        Me.Nombre1 = nombre
        Me.Descripcion1 = descripcion
        Me.Estado1 = estado
        Me.Monto1 = monto
    End Sub
End Class



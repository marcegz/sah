﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataDescuentosCatigos

    Function insertarDescuentosCatigos(descuentoCastigo As Descuentos_Castigos) As String

        Dim sqlSP As String = "sp_insertar_descuentos_castigos"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@tipo", descuentoCastigo.Tipo1))
        cmdInsertar.Parameters.Add(New SqlParameter("@descripcion", descuentoCastigo.Descripcion1))
        cmdInsertar.Parameters.Add(New SqlParameter("@atrasado_adelantado", descuentoCastigo.Atrasado_adelantado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@monto_aplicado", descuentoCastigo.Monto_aplicado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@estado", descuentoCastigo.Estado1))

        Try
            cmdInsertar.ExecuteNonQuery()
            Return "Ahorro registrado exitosamente."

        Catch e As SqlException
            Return "Ha ocurrido un error al insertar los datos del ahorro."
        End Try

    End Function

    Function CodigoNuevoDescuentoCastigo() As Integer

        Dim NuevoIdDescuentoCastigo As Integer
        Dim rs As New SqlCommand("SELECT IDENT_CURRENT('descuentos_castigos') As idDescuentoCastigo", Conn)
        Dim ds As SqlDataReader
        ds = rs.ExecuteReader

        Dim dt As New DataTable
        Dim command As New SqlCommand("select * from descuentos_castigos;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)
        End If

        If ds.Read Then
            If dt.Rows.Count = 0 Then
                NuevoIdDescuentoCastigo = ds.Item("idDescuentoCastigo")
            Else
                NuevoIdDescuentoCastigo = ds.Item("idDescuentoCastigo") + 1
            End If
        End If

        ds.Close()

        Return NuevoIdDescuentoCastigo

    End Function

    Function getTablaConsultaDescuentosCastigos() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("select * from descuentos_castigos;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Sub actualizarTasaInteres(idDescuentos_Castigos As Integer, monto_aplicado As Integer)

        Dim sqlSP As String = "sp_actualizar_descuentos_castigos"
        Dim cmdActualizar As New SqlCommand(sqlSP, Conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@idDescuentos_Castigos", idDescuentos_Castigos))
        cmdActualizar.Parameters.Add(New SqlParameter("@monto_aplicado", monto_aplicado))

        Try

            cmdActualizar.ExecuteNonQuery()

        Catch e As SqlException

        End Try

    End Sub


End Class

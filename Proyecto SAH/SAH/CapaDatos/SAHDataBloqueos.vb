﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataBloqueos

    Dim negocioAhorros As New SAHNegocioAhorros

    Sub insertarBloqueo(bloqueo As SAHBloqueos)

        Dim cod As Integer = 0
        Dim sqlSP As String = "sp_insertar_bloqueo"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@tipo", bloqueo.Tipo1))
        cmdInsertar.Parameters.Add(New SqlParameter("@descripcion", bloqueo.Descripcion1))
        cmdInsertar.Parameters.Add(New SqlParameter("@fecha", bloqueo.Fecha1))
        cmdInsertar.Parameters.Add(New SqlParameter("@idAhorro", bloqueo.IdAhorro1))

        Try
            cmdInsertar.ExecuteNonQuery()

        Catch e As SqlException

        End Try

    End Sub

    Function getBloqueos() As List(Of SAHBloqueos)
        Dim table As DataTable
        Dim dt As New DataTable
        Dim bloqueo As New SAHBloqueos
        Dim listaBloqueos As New List(Of SAHBloqueos)

        Dim command As New SqlCommand("select * from bloqueos;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)
            table = dt

            For Each row As DataRow In dt.Rows
                bloqueo = New SAHBloqueos(Convert.ToInt32(row("idBloqueo")), Convert.ToString(row("tipo")),
                                               Convert.ToString(row("descripcion")), Convert.ToDateTime(row("fecha")),
                                               Convert.ToInt32(row("idAhorro")))
                listaBloqueos.Add(bloqueo)

            Next

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return listaBloqueos

    End Function

    Function getListaBloqueosAhorros(idAhorro As Integer) As DataTable

        Dim adapter As SqlDataAdapter
        Dim param As SqlParameter
        Dim dt As New DataTable

        Dim sqlSP As String = "sp_consultar_bloqueos_ahorro"
        Dim command As New SqlCommand(sqlSP, Conn)
        command.CommandType = CommandType.StoredProcedure


        param = New SqlParameter("@idAhorro", idAhorro)
        param.Direction = ParameterDirection.Input
        param.DbType = DbType.Int32
        command.Parameters.Add(param)

        adapter = New SqlDataAdapter(command)
        adapter.Fill(dt)
        Return dt


    End Function


    Function getTablaConsultaBloqueos(idAhorro As Integer) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_buscar_bloqueos", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idAhorro", idAhorro)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getTablaBloqueos(nombre As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_buscar_bloqueos", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@nombre", nombre)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Sub generarBloqueo()

        Dim dt As DataTable = negocioAhorros.getTablaConsultaAhorros("")
        Dim bloqueo As New SAHBloqueos

        For Each row As DataRow In dt.Rows

            If Convert.ToDateTime(row("fechaFinal")) <= Date.Now Then
                negocioAhorros.actualizarAhorro(Convert.ToInt32(row("idAhorro")), Convert.ToString(row("nombre")), "Finalizado", Convert.ToInt32(row("idAutorizado")))
                bloqueo = New SAHBloqueos("Fecha", "El ahorro ha llegado a su fecha de finalización", Date.Now, Convert.ToInt32(row("idAhorro")))
                insertarBloqueo(bloqueo)

            End If

        Next


    End Sub


End Class

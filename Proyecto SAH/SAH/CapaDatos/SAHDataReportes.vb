﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataReportes

    Function getTablaAutorizadosAhorroEscolar() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_autorizados_ahorro_escolar", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaAutorizadosOtrosAhorros() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_autorizados_otros_ahorros", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaBeneficiariosAhorroEscolar() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_beneficiarios_ahorro_escolar", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaBeneficiariosOtrosAhorros() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_beneficiarios_otros_ahorros", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaBeneficiariosAsociado(idAsociado As Integer) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_beneficiarios_asociado", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idAsociado", idAsociado)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaBloqueosFechaEstado(tipo As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_bloqueos_estado_fecha", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@tipo", tipo)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getTablaCertificadosCancelados() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_certificados_cancelados", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getTablaCertificadosCustodia() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_certificados_custodia", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getTablaCertificadosAsociado(idAsociado As Integer) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_certificados_asociado", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idAsociado", idAsociado)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaCertificadosAsociadoMoneda(idAsociado As Integer, moneda As String, consulta As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_certificados_asociado_moneda", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idAsociado", idAsociado)
        command.Parameters.AddWithValue("@moneda", moneda)
        command.Parameters.AddWithValue("@consulta", consulta)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaCertificadosVencidos() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_certificados_vencidos", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getTablaCuentasActivasAsociado(idAsociado As Integer) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_cuentas_activas_asociado", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idAsociado", idAsociado)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getTablaCuentasAhorroEscolar() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_cuentas_ahorro_escolar", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getTablaCuponesAsociadoCertificado(id As Integer, consulta As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_cupones_asociado_certificado", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@id", id)
        command.Parameters.AddWithValue("@consulta", consulta)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaRangosCalculoInteres(consulta As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_rangos_calculo_interes", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@consulta", consulta)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaCertificadosReimpresos() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_certificados_reimpresos", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaCastigosAtrasos() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_castigos_atraso", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaDescuentosRetiroAnticipado() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_descuentos_retiro_anticipado", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaTransaccionesCuentaTipoAhorro(consulta As String, id As Integer) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_transacciones_cuenta_tipo_ahorro", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@consulta", consulta)
        command.Parameters.AddWithValue("@id", id)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getTablaVencimientoCertificados() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_vencimiento_certificados", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTablaVencimientoCupones() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_vencimiento_cupones", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


End Class

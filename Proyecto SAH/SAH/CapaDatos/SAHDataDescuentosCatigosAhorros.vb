﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataDescuentosCatigosAhorros

    Sub insertarDescuentosCatigosAhorros(ByVal idAhorro As Integer, ByVal idDescuentos_Castigos As Integer,
                        ByVal fecha_aplicacion As Date)

        Dim cod As Integer = 0
        Dim sqlSP As String = "sp_insertar_descuentos_castigos_ahorros"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@idAhorro", idAhorro))
        cmdInsertar.Parameters.Add(New SqlParameter("@idDescuentos_Castigos", idDescuentos_Castigos))
        cmdInsertar.Parameters.Add(New SqlParameter("@fecha_aplicacion", fecha_aplicacion))

        Try
            cmdInsertar.ExecuteNonQuery()
        Catch e As SqlException

        End Try

    End Sub





    Function getTablaDC_Ahorros(nombre As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_buscar_dc_ahorros", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@nombre", nombre)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getMovimientos() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("select * from movimiento_ahorros", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function



End Class

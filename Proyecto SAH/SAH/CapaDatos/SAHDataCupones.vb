﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataCupones

    Dim negocioAhorros As New SAHNegocioAhorros
    Dim negocioTasasInteres As New SAHNegocioTasasInteres
    Dim negocioCertificados As New SAHNegocioCertificados

    Sub insertarCupon(cupon As SAHCupones)

        Dim sqlSP As String = "sp_insertar_cupon"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@fechaGenerado", cupon.FechaGenerado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@fechaVencimiento", cupon.FechaVencimiento1))
        cmdInsertar.Parameters.Add(New SqlParameter("@estadoVencimiento", cupon.EstadoVencimiento1))
        cmdInsertar.Parameters.Add(New SqlParameter("@intereses", cupon.Intereses1))
        cmdInsertar.Parameters.Add(New SqlParameter("@monto", cupon.Monto1))
        cmdInsertar.Parameters.Add(New SqlParameter("@pago", cupon.Pago1))
        cmdInsertar.Parameters.Add(New SqlParameter("@idCertificado", cupon.IdCertificado1))

        Try
            cmdInsertar.ExecuteNonQuery()

        Catch e As SqlException

        End Try

    End Sub

    Function getCupones() As List(Of SAHCupones)
        Dim table As DataTable
        Dim dt As New DataTable
        Dim cupon As New SAHCupones
        Dim listaCupones As New List(Of SAHCupones)

        Dim command As New SqlCommand("select * from cupones;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)
            table = dt

            For Each row As DataRow In dt.Rows
                cupon = New SAHCupones(Convert.ToInt32(row("idCupon")), Convert.ToDateTime(row("fechaGenerado")),
                                               Convert.ToDateTime(row("fechaVencimiento")), Convert.ToString(row("estadoVencimiento")),
                                               Convert.ToDouble(row("intereses")), Convert.ToInt32(row("monto")),
                                               Convert.ToBoolean(row("pago")), Convert.ToInt32(row("idCertificado")))
                listaCupones.Add(cupon)

            Next

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return listaCupones

    End Function


    Function getIntereses(idCertificado As Integer) As Double

        Dim intereses As Double = 0.00

        For Each lista As SAHCupones In getCupones()

            If lista.IdCertificado1 = idCertificado Then
                intereses += lista.Intereses1
            End If

        Next

        Return intereses

    End Function

    Function getListaFechasCupones(fechaInicio As Date, fechaFinal As Date, frecuencia As Integer) As List(Of Date)

        Dim fecha As Date = fechaInicio
        Dim listaFechasCupones As New List(Of Date)

        While fecha <= fechaFinal

            fecha = fecha.AddDays(frecuencia)

            If fecha <= fechaFinal Then
                listaFechasCupones.Add(fecha)

            End If

        End While

        Return listaFechasCupones

    End Function


    Function verificarExisteCupon(idCertificado As Integer, fechaGenerado As Date) As Boolean

        Dim sqlSP As String = "sp_consultar_verificar_existe_cupon"
        Dim command As New SqlCommand(sqlSP, Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idCertificado", idCertificado)
        command.Parameters.AddWithValue("@fechaGenerado", fechaGenerado)


        Dim ds As SqlDataReader
        ds = command.ExecuteReader

        If CBool(ds.Read) = True Then
            verificarExisteCupon = True
        Else
            verificarExisteCupon = False
        End If

        ds.Close()
        Return verificarExisteCupon

    End Function


    Sub generarCupones()

        For Each ahorro As SAHAhorros In negocioAhorros.consultarAhorros()

            For Each fecha As Date In getListaFechasCupones(ahorro.FechaInicio1, ahorro.FechaFinal1, ahorro.FrecuenciaIntereses1)

                If Date.Now = fecha Then

                    If verificarExisteCupon(negocioCertificados.getIdCertificadoPlazo(ahorro.IdAhorro1), Date.Now) = True Then

                        Dim interes As Double = (ahorro.MontoAhorrado1 * (negocioTasasInteres.getPorcentaje(ahorro.Duracion1, ahorro.IdTipoAhorro1) / 100)) / (ahorro.Duracion1 / 30)

                        Dim cupon As New SAHCupones(Date.Now, Date.Now.AddDays(30), "Vigente", interes, 500, 0, negocioCertificados.getIdCertificadoPlazo(ahorro.IdAhorro1))

                        insertarCupon(cupon)

                    End If



                End If

            Next

        Next

    End Sub


    Function getTablaConsultaCupones(nombre As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_buscar_cupones", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@nombre", nombre)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function



End Class

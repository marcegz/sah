﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataTiposCertificados

    Function insertarTipoCertificado(tipoCertificado As SAHTiposCertificados) As String

        Dim sqlSP As String = "sp_insertar_tipo_certificado"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@nombre", tipoCertificado.Nombre1))
        cmdInsertar.Parameters.Add(New SqlParameter("@descripcion", tipoCertificado.Descripcion1))
        cmdInsertar.Parameters.Add(New SqlParameter("@estado", tipoCertificado.Estado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@monto", tipoCertificado.Monto1))

        Try
            cmdInsertar.ExecuteNonQuery()
            Return "Tipo de certificado registrado exitosamente."

        Catch e As SqlException

            Return "Ha ocurrido un error al insertar los datos del tipo de certificado."

        End Try

    End Function


    Function cargarTiposCertificadosTabla() As DataTable
        Dim rsdatos As New SqlDataAdapter("select * from tiposCertificados", Conn)
        Dim ds As New DataSet
        rsdatos.Fill(ds, "tiposCertificados")
        cargarTiposCertificadosTabla = ds.Tables("tiposCertificados")
    End Function

    Function CodigoNuevoIdTipoCertificado() As Integer

        Dim NuevoIdTipoCertificado As Integer
        Dim rs As New SqlCommand("SELECT IDENT_CURRENT('tiposCertificados') As idTipoCertificado", Conn)
        Dim ds As SqlDataReader
        ds = rs.ExecuteReader

        Dim dt As New DataTable
        Dim command As New SqlCommand("select * from tiposCertificados;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)
        End If

        If ds.Read Then
            If dt.Rows.Count = 0 Then
                NuevoIdTipoCertificado = ds.Item("idTipoCertificado")
            Else
                NuevoIdTipoCertificado = ds.Item("idTipoCertificado") + 1
            End If
        End If

        ds.Close()

        Return NuevoIdTipoCertificado

    End Function

    Function getTablaConsultaTiposCertificados(nombre As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_buscar_tipos_certificados", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@nombre", nombre)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTipoCertificado(idTipoCertificado As Integer) As SAHTiposCertificados
        Dim table As DataTable
        Dim dt As New DataTable
        Dim tipoCertificado As New SAHTiposCertificados

        Dim command As New SqlCommand("sp_obtener_tipo_certificado", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idTipoCertificado", idTipoCertificado)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)
            table = dt

            For Each row As DataRow In dt.Rows
                tipoCertificado = New SAHTiposCertificados(Convert.ToInt32(row("idTipoCertificado")), Convert.ToString(row("nombre")),
                                     Convert.ToString(row("descripcion")), Convert.ToBoolean(row("estado")), Convert.ToInt32(row("monto")))
            Next

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return tipoCertificado

    End Function

    Function actualizarTipoCertificado(tipoCertificado As SAHTiposCertificados) As String

        Dim sqlSP As String = "sp_actualizar_tipo_certificado"
        Dim cmdActualizar As New SqlCommand(sqlSP, Conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@idTipoCertificado", tipoCertificado.IdTipoCertificado1))
        cmdActualizar.Parameters.Add(New SqlParameter("@nombre", tipoCertificado.Nombre1))
        cmdActualizar.Parameters.Add(New SqlParameter("@descripcion", tipoCertificado.Descripcion1))
        cmdActualizar.Parameters.Add(New SqlParameter("@estado", tipoCertificado.Estado1))
        cmdActualizar.Parameters.Add(New SqlParameter("@monto", tipoCertificado.Monto1))


        Try

            cmdActualizar.ExecuteNonQuery()
            Return "Tipo de certificado actualizado exitosamente."

        Catch e As SqlException

            Return "Ha ocurrido un error al actualizar los datos del tipo de certificado."

        End Try

    End Function


End Class

﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataTiposAhorros


    Function insertarTipoAhorro(tipoAhorro As SAHTiposAhorros) As Integer

        Dim i As Integer

        Dim sqlSP As String = "sp_insertar_tipo_ahorro"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@nombre", tipoAhorro.Nombre1))
        cmdInsertar.Parameters.Add(New SqlParameter("@estado", tipoAhorro.Estado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@fechaEntrega", tipoAhorro.FechaEntrega1))
        cmdInsertar.Parameters.Add(New SqlParameter("@personalizado", tipoAhorro.Personalizado1))

        Try
            cmdInsertar.ExecuteNonQuery()

            i = 0
            MsgBox("Tipo de ahorro registrado exitosamente.", MsgBoxStyle.Information, "SAH")

        Catch e As SqlException

            i = 1
            MsgBox("Ingrese un nombre de tipo de ahorro distinto a los registrados.", MsgBoxStyle.Information, "SAH")

        End Try

        Return i

    End Function


    Function CodigoNuevoTipoAhorro() As Integer
        Dim NuevoIdTipoAhorro As Integer
        Dim rs As New SqlCommand("SELECT IDENT_CURRENT('tiposAhorros') As idTipoAhorro", Conn)
        Dim ds As SqlDataReader
        ds = rs.ExecuteReader

        Dim dt As New DataTable
        Dim command As New SqlCommand("select * from tiposAhorros;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)
        End If

        If ds.Read Then
            If dt.Rows.Count = 0 Then
                NuevoIdTipoAhorro = ds.Item("idTipoAhorro")
            Else
                NuevoIdTipoAhorro = ds.Item("idTipoAhorro") + 1
            End If
        End If

        ds.Close()

        Return NuevoIdTipoAhorro
    End Function



    Function getTablaConsultaTiposAhorros(nombre As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_buscar_tipos_ahorros", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@nombre", nombre)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function

    Function getTipoAhorro(idTipoAhorro As Integer) As SAHTiposAhorros
        Dim table As DataTable
        Dim dt As New DataTable
        Dim tipoAhorro As New SAHTiposAhorros

        Dim command As New SqlCommand("sp_obtener_tipo_ahorro", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idTipoAhorro", idTipoAhorro)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)
            table = dt

            For Each row As DataRow In dt.Rows

                If IsDBNull(row("fechaEntrega")) Then

                    tipoAhorro = New SAHTiposAhorros(Convert.ToInt32(row("idTipoAhorro")), Convert.ToString(row("nombre")),
                                             Convert.ToBoolean(row("estado")), Nothing,
                                               Convert.ToBoolean(row("personalizado")))


                Else
                    tipoAhorro = New SAHTiposAhorros(Convert.ToInt32(row("idTipoAhorro")), Convert.ToString(row("nombre")),
                                             Convert.ToBoolean(row("estado")), Convert.ToDateTime(row("fechaEntrega")),
                                               Convert.ToBoolean(row("personalizado")))
                End If

            Next

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return tipoAhorro

    End Function

    Function getTablaTiposAhorros() As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("SELECT * from tiposAhorros", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(ds, "tiposAhorros")
            dt = ds.Tables("tiposAhorros")

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getTiposAhorrosActivos() As DataTable
        Dim dt As DataTable = getTablaTiposAhorros()
        Dim tabla As New DataTable
        Dim dc1 As New DataColumn("idTipoAhorro")
        Dim dc2 As New DataColumn("nombre")
        Dim dc3 As New DataColumn("estado")
        Dim dc4 As New DataColumn("fechaEntrega")
        Dim dc5 As New DataColumn("personalizado")

        tabla.Columns.AddRange(New DataColumn() {dc1, dc2, dc3, dc4, dc5})

        For Each row As DataRow In dt.Rows

            If Convert.ToBoolean(row("estado")) = True Then
                Dim drNewRow As DataRow
                drNewRow = tabla.NewRow
                drNewRow.Item("idTipoAhorro") = Convert.ToInt32(row("idTipoAhorro"))
                drNewRow.Item("nombre") = Convert.ToString(row("nombre"))
                drNewRow.Item("estado") = Convert.ToBoolean(row("estado"))
                If IsDBNull(row("fechaEntrega")) Then
                    drNewRow.Item("fechaEntrega") = Nothing
                Else
                    drNewRow.Item("fechaEntrega") = Convert.ToDateTime(row("fechaEntrega"))
                End If
                drNewRow.Item("personalizado") = Convert.ToBoolean(row("personalizado"))

                tabla.Rows.Add(drNewRow)
            End If

        Next

        Return tabla

    End Function


    Function getFecha(nombre As String) As Date

        Dim fecha As Date
        Dim nombreAhorro As String
        Dim dt As DataTable = getTablaTiposAhorros()

        For Each row As DataRow In dt.Rows


            nombreAhorro = Convert.ToString(row("nombre"))

            If nombreAhorro = nombre Then
                fecha = Convert.ToDateTime(row("fechaEntrega"))
            End If
        Next

        Return fecha

    End Function


    Function getIdTipoAhorro(nombre As String) As Integer

        Dim idTipoAhorro As Integer
        Dim nombreAhorro As String
        Dim dt As DataTable = getTablaTiposAhorros()

        For Each row As DataRow In dt.Rows


            nombreAhorro = Convert.ToString(row("nombre"))

            If nombreAhorro = nombre Then
                idTipoAhorro = Convert.ToInt32(row("idTipoAhorro"))
            End If
        Next

        Return idTipoAhorro

    End Function

    Function getPersonalizado(nombre As String) As Boolean

        Dim personalizado As Boolean
        Dim rs As New SqlCommand("select t.personalizado As personalizado from tiposAhorros as t where t.nombre = '" & nombre & "'", Conn)
        Dim ds As SqlDataReader

        ds = rs.ExecuteReader

        If ds.Read Then
            personalizado = ds("personalizado")
        End If

        ds.Close()

        Return personalizado

    End Function


    Sub actualizarEstadoTipoAhorro(idTipoAhorro As Integer, estado As Boolean)

        Dim sqlSP As String = "sp_actualizar_estado_tipo_ahorro"
        Dim cmdActualizar As New SqlCommand(sqlSP, Conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@idTipoAhorro", idTipoAhorro))
        cmdActualizar.Parameters.Add(New SqlParameter("@estado", estado))

        Try

            cmdActualizar.ExecuteNonQuery()

        Catch e As SqlException

        End Try

    End Sub


    Function actualizarTipoAhorro(idTipoAhorro As Integer, nombre As String, estado As Integer, fechaEntrega As Date, personalizado As Boolean) As Integer

        Dim i As Integer

        Dim sqlSP As String = "sp_actualizar_tipo_ahorro"
        Dim cmdActualizar As New SqlCommand(sqlSP, Conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@idTipoAhorro", idTipoAhorro))
        cmdActualizar.Parameters.Add(New SqlParameter("@nombre", nombre))
        cmdActualizar.Parameters.Add(New SqlParameter("@estado", estado))
        cmdActualizar.Parameters.Add(New SqlParameter("@fechaEntrega", fechaEntrega))
        cmdActualizar.Parameters.Add(New SqlParameter("@personalizado", personalizado))

        Try
            MessageBox.Show(fechaEntrega)
            cmdActualizar.ExecuteNonQuery()

            i = 0
            MsgBox("Tipo de ahorro actualizado exitosamente.", MsgBoxStyle.Information, "SAH")

        Catch e As SqlException

            i = 1
            MsgBox("Ingrese un nombre de tipo de ahorro distinto a los registrados.", MsgBoxStyle.Information, "SAH")

        End Try

        Return i

    End Function

End Class

﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataTasasInteres

    Sub insertarTasaInteres(tasaInteres As SAHTasasInteres)

        Dim sqlSP As String = "sp_insertar_tasa_interes"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@rangoDias", tasaInteres.RangoDias1))
        cmdInsertar.Parameters.Add(New SqlParameter("@rangoMeses", tasaInteres.RangoMeses1))
        cmdInsertar.Parameters.Add(New SqlParameter("@porcentaje", tasaInteres.Porcentaje1))
        cmdInsertar.Parameters.Add(New SqlParameter("@idTipoAhorro", tasaInteres.IdTipoAhorro1))

        Try
            cmdInsertar.ExecuteNonQuery()
        Catch e As SqlException

        End Try

    End Sub


    Function getTasasInteres() As List(Of SAHTasasInteres)
        Dim table As DataTable
        Dim dt As New DataTable
        Dim tasaInteres As New SAHTasasInteres
        Dim listaTasasInteres As New List(Of SAHTasasInteres)

        Dim command As New SqlCommand("select * from tasasInteres;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)
            table = dt

            For Each row As DataRow In dt.Rows
                tasaInteres = New SAHTasasInteres(Convert.ToInt32(row("idTasaInteres")), Convert.ToString(row("rangoDias")),
                                               Convert.ToString(row("rangoMeses")), Convert.ToDouble(row("porcentaje")),
                                               Convert.ToInt32(row("idTipoAhorro")))
                listaTasasInteres.Add(tasaInteres)

            Next

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return listaTasasInteres

    End Function


    Function getTablaConsultaTasasInteres(idTipoAhorro As Integer) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_consultar_tasas_interes_tipo_ahorro", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idTipoAhorro", idTipoAhorro)


        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            da.Fill(dt)


        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If



        Return dt

    End Function


    Function getPorcentaje(duracion As Integer, idTipoAhorro As Integer) As Double

        Dim porcentaje As Double
        Dim vectoraux() As String

        For Each lista As SAHTasasInteres In getTasasInteres()
            If lista.IdTipoAhorro1 = idTipoAhorro Then

                vectoraux = lista.RangoDias1.Split("-")

                If duracion >= Convert.ToInt32(vectoraux(0)) And duracion <= Convert.ToInt32(vectoraux(1)) Then
                    porcentaje = lista.Porcentaje1
                    Exit For
                End If

            End If
        Next

        Return porcentaje
    End Function


    Sub actualizarTasaInteres(idTasaInteres As Integer, porcentaje As Double)

        Dim sqlSP As String = "sp_actualizar_tasa_interes"
        Dim cmdActualizar As New SqlCommand(sqlSP, Conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@idTasaInteres", idTasaInteres))
        cmdActualizar.Parameters.Add(New SqlParameter("@porcentaje", porcentaje))

        Try

            cmdActualizar.ExecuteNonQuery()

        Catch e As SqlException

        End Try

    End Sub

    Function verificarPorcentajesTipoAhorro(idTipoAhorro As Integer) As Boolean
        Dim salida As Boolean = True

        For Each lista As SAHTasasInteres In getTasasInteres()

            If lista.IdTipoAhorro1 = idTipoAhorro And lista.Porcentaje1 = 0.0 Then

                salida = False
                Exit For

            End If

        Next

        Return salida

    End Function


    Function getTablaTasasInteres(nombre As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_buscar_tasas_interes", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@nombre", nombre)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


End Class

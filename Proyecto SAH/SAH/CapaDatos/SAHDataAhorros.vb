﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataAhorros

    Function insertarAhorro(ahorro As SAHAhorros) As String

        Dim sqlSP As String = "sp_insertar_ahorro"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@nombre", ahorro.NombreAhorro1))
        cmdInsertar.Parameters.Add(New SqlParameter("@duracion", ahorro.Duracion1))
        cmdInsertar.Parameters.Add(New SqlParameter("@fechaInicio", ahorro.FechaInicio1))
        cmdInsertar.Parameters.Add(New SqlParameter("@fechaFinal", ahorro.FechaFinal1))
        cmdInsertar.Parameters.Add(New SqlParameter("@diaAplicacion", ahorro.DiaAplicacion1))
        cmdInsertar.Parameters.Add(New SqlParameter("@frecuenciaIntereses", ahorro.FrecuenciaIntereses1))
        cmdInsertar.Parameters.Add(New SqlParameter("@frecuenciaCuota", ahorro.FrecuenciaCuota1))
        cmdInsertar.Parameters.Add(New SqlParameter("@moneda", ahorro.Moneda1))
        cmdInsertar.Parameters.Add(New SqlParameter("@montoCuota", ahorro.MontoCuota1))
        cmdInsertar.Parameters.Add(New SqlParameter("@montoAhorrado", ahorro.MontoAhorrado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@estado", ahorro.Estado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@tipo_pago", ahorro.Tipo_pago1))
        cmdInsertar.Parameters.Add(New SqlParameter("@idTipoAhorro", ahorro.IdTipoAhorro1))
        cmdInsertar.Parameters.Add(New SqlParameter("@idAsociado", ahorro.IdAsociado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@idAutorizado", ahorro.IdAutorizado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@idEmpleado", ahorro.IdEmpleado1))

        Try

            cmdInsertar.ExecuteNonQuery()
            Return "Ahorro registrado exitosamente."

        Catch e As SqlException

            Return "Ha ocurrido un error al insertar los datos del ahorro."
            MessageBox.Show(String.Concat(e))

        End Try

    End Function


    Function CodigoNuevoAhorro() As Integer

        Dim NuevoIdAhorro As Integer
        Dim rs As New SqlCommand("SELECT IDENT_CURRENT('ahorros') As idAhorro", Conn)
        Dim ds As SqlDataReader
        ds = rs.ExecuteReader

        Dim dt As New DataTable
        Dim command As New SqlCommand("select * from ahorros;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)
        End If

        If ds.Read Then
            If dt.Rows.Count = 0 Then
                NuevoIdAhorro = ds.Item("idAhorro")
            Else
                NuevoIdAhorro = ds.Item("idAhorro") + 1
            End If
        End If

        ds.Close()

        Return NuevoIdAhorro

    End Function


    Function getTablaConsultaAhorros(consulta As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_buscar_ahorros", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@consulta", consulta)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getAhorrosCombo() As DataTable
        Dim dt As DataTable = getTablaConsultaAhorros("")
        Dim tabla As New DataTable
        Dim dc1 As New DataColumn("idAhorro")
        Dim dc2 As New DataColumn("nombre")


        tabla.Columns.AddRange(New DataColumn() {dc1, dc2})

        For Each row As DataRow In dt.Rows

            If Convert.ToString(row("estado")) = "Vigente" Then
                Dim drNewRow As DataRow
                drNewRow = tabla.NewRow
                drNewRow.Item("idAhorro") = Convert.ToInt32(row("idAhorro"))
                drNewRow.Item("nombre") = Convert.ToInt32(row("idAhorro")) & "-" & Convert.ToString(row("nombre"))
                tabla.Rows.Add(drNewRow)
            End If


        Next

        Return tabla

    End Function



    Function getNombre(cedula As String) As String

        Dim nombre As String = ""
        Dim rs As New SqlCommand("sp_consultar_nombre @cedula = '" & cedula & "'", Conn)
        Dim ds As SqlDataReader
        ds = rs.ExecuteReader

        If ds.Read Then
            nombre = String.Concat(ds("nombre"), " ", ds("apellidoUno"), " ", ds("apellidoDos"))

        Else


            MsgBox("La cédula digitada no pertenece a ningún asociado", MsgBoxStyle.Information, "Mensaje Información")
        End If

        ds.Close()

        Return nombre

    End Function


    Function getIdAsociado(cedula As String) As Integer

        Dim idAsociado As Integer = -1
        Dim rs As New SqlCommand("sp_consultar_id_asociado @cedula = '" & cedula & "'", Conn)
        Dim ds As SqlDataReader
        ds = rs.ExecuteReader

        If ds.Read Then
            idAsociado = ds("idAsociado")
        End If

        ds.Close()

        Return idAsociado


    End Function


    Function getIdPersona(cedula As String) As Integer

        Dim idPersona As Integer
        Dim rs As New SqlCommand("select idPersona from Personas where cedula = '" & cedula & "'", Conn)
        Dim dso As SqlDataReader
        dso = rs.ExecuteReader

        If dso.Read Then
            idPersona = dso.Item("idPersona")
        End If

        dso.Close()
        Return idPersona

    End Function


    Function getAhorro(idAhorro As Integer) As SAHAhorros
        Dim table As DataTable
        Dim dt As New DataTable
        Dim ahorro As New SAHAhorros

        Dim command As New SqlCommand("sp_obtener_ahorro", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idAhorro", idAhorro)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)
            table = dt

            For Each row As DataRow In dt.Rows
                ahorro = New SAHAhorros(Convert.ToInt32(row("idAhorro")), Convert.ToString(row("nombreAhorro")),
                                     Convert.ToString(row("nombreTipoAhorro")), Convert.ToString(row("cedula")),
                                     Convert.ToInt32(row("duracion")), Convert.ToDateTime(row("fechaInicio")),
                                     Convert.ToDateTime(row("fechaFinal")), Convert.ToInt32(row("diaAplicacion")),
                                     Convert.ToInt32(row("frecuenciaIntereses")), Convert.ToInt32(row("frecuenciaCuota")),
                                     Convert.ToString(row("moneda")), Convert.ToDouble(row("montoCuota")),
                                     Convert.ToDouble(row("montoAhorrado")), Convert.ToString(row("estado")),
                                     Convert.ToBoolean(row("tipoPago")), Convert.ToInt32(row("idAutorizado")),
                                     Convert.ToInt32(row("idTipoAhorro")))
            Next

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return ahorro

    End Function

    Function consultaAhorros() As List(Of SAHAhorros)
        Dim table As DataTable
        Dim dt As New DataTable
        Dim ahorro As New SAHAhorros
        Dim listaAhorros As New List(Of SAHAhorros)

        Dim command As New SqlCommand("sp_consultar_ahorros", Conn)
        command.CommandType = CommandType.StoredProcedure

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)
            table = dt

            For Each row As DataRow In dt.Rows
                ahorro = New SAHAhorros(Convert.ToInt32(row("idAhorro")), Convert.ToDateTime(row("fechaInicio")),
                                     Convert.ToDateTime(row("fechaFinal")), Convert.ToInt32(row("duracion")),
                                     Convert.ToInt32(row("frecuenciaIntereses")), Convert.ToInt32(row("montoAhorrado")),
                                     Convert.ToInt32(row("idTipoAhorro")))

                listaAhorros.Add(ahorro)
            Next

        End If

        Return listaAhorros

    End Function


    Function actualizarAhorro(idAhorro As Integer, nombre As String, estado As String, idAutorizado As Integer) As String

        Dim sqlSP As String = "sp_actualizar_ahorro"
        Dim cmdActualizar As New SqlCommand(sqlSP, Conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@idAhorro", idAhorro))
        cmdActualizar.Parameters.Add(New SqlParameter("@nombre", nombre))
        cmdActualizar.Parameters.Add(New SqlParameter("@estado", estado))
        cmdActualizar.Parameters.Add(New SqlParameter("@idAutorizado", idAutorizado))

        Try

            cmdActualizar.ExecuteNonQuery()
            Return "Ahorro actualizado exitosamente."

        Catch e As SqlException

            Return "Ha ocurrido un error al actualizar los datos del ahorro."

        End Try

    End Function

    Function verificarUsuarioAdmin(idUsuario As Integer) As Boolean

        Dim sqlSP As String = "sp_verificar_usuario_admin"
        Dim command As New SqlCommand(sqlSP, Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idUsuario", idUsuario)

        Dim ds As SqlDataReader
        ds = command.ExecuteReader

        If CBool(ds.Read) = True Then
            verificarUsuarioAdmin = True
        Else
            verificarUsuarioAdmin = False
        End If

        ds.Close()
        Return verificarUsuarioAdmin

    End Function

    Function verificarUsuarioAhorros(idUsuario As Integer) As Boolean

        Dim sqlSP As String = "sp_verificar_usuario_ahorros"
        Dim command As New SqlCommand(sqlSP, Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idUsuario", idUsuario)

        Dim ds As SqlDataReader
        ds = command.ExecuteReader

        If CBool(ds.Read) = True Then
            verificarUsuarioAhorros = True
        Else
            verificarUsuarioAhorros = False
        End If

        ds.Close()
        Return verificarUsuarioAhorros

    End Function



End Class

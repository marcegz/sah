﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataCertificados

    Function insertarCertificado(certfificado As SAHCertificados) As String

        Dim sqlSP As String = "sp_insertar_certificado"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@fechaGenerado", certfificado.FechaGenerado1))
        cmdInsertar.Parameters.Add(New SqlParameter("@fechaVencimiento", certfificado.FechaVencimiento1))
        cmdInsertar.Parameters.Add(New SqlParameter("@estadoVencimiento", certfificado.EstadoVencimiento1))
        cmdInsertar.Parameters.Add(New SqlParameter("@reimpreso", certfificado.Reimpreso1))
        cmdInsertar.Parameters.Add(New SqlParameter("@pago", certfificado.Pago1))
        cmdInsertar.Parameters.Add(New SqlParameter("@idAhorro", certfificado.IdAhorro1))
        cmdInsertar.Parameters.Add(New SqlParameter("@idTipoCertificado", certfificado.IdTipoCertificado1))

        Try
            cmdInsertar.ExecuteNonQuery()
            Return "Certificado registrado exitosamente."

        Catch e As SqlException

            Return "Ha ocurrido un error al insertar los datos del certificado."
        End Try

    End Function


    Function CodigoNuevoIDCertificado() As Integer

        Dim NuevoIdCertificado As Integer
        Dim rs As New SqlCommand("SELECT IDENT_CURRENT('certificados') As idCertificado", Conn)
        Dim ds As SqlDataReader
        ds = rs.ExecuteReader

        Dim dt As New DataTable
        Dim command As New SqlCommand("select * from certificados;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)
            da.Fill(dt)
        End If

        If ds.Read Then
            If dt.Rows.Count = 0 Then
                NuevoIdCertificado = ds.Item("idCertificado")
            Else
                NuevoIdCertificado = ds.Item("idCertificado") + 1
            End If
        End If

        ds.Close()

        Return NuevoIdCertificado

    End Function


    Function getCertificados() As List(Of SAHCertificados)
        Dim table As DataTable
        Dim dt As New DataTable
        Dim certificado As New SAHCertificados
        Dim listaCertificados As New List(Of SAHCertificados)

        Dim command As New SqlCommand("select * from certificados;", Conn)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)
            table = dt

            For Each row As DataRow In dt.Rows
                certificado = New SAHCertificados(Convert.ToInt32(row("idCertificado")), Convert.ToDateTime(row("fechaGenerado")),
                                               Convert.ToDateTime(row("fechaVencimiento")), Convert.ToString(row("estadoVencimiento")),
                                               Convert.ToInt32(row("reimpreso")), Convert.ToBoolean(row("pago")),
                                               Convert.ToInt32(row("idAhorro")), Convert.ToInt32(row("idTipoCertificado")))
                listaCertificados.Add(certificado)

            Next

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return listaCertificados

    End Function


    Function getIdCertificadoPlazo(idAhorro As Integer) As Integer

        Dim idCertificado As Integer

        For Each lista As SAHCertificados In getCertificados()

            If lista.IdAhorro1 = idAhorro And lista.IdTipoCertificado1 = 1 Then
                idCertificado = lista.IdCertificado1
            End If

        Next

        Return idCertificado

    End Function

    Function getTablaConsultaCertificados(nombre As String) As DataTable
        Dim dt As New DataTable

        Dim command As New SqlCommand("sp_buscar_certificados", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@nombre", nombre)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return dt

    End Function


    Function getCertificado(idCertificado As Integer) As SAHCertificados
        Dim table As DataTable
        Dim dt As New DataTable
        Dim certificado As New SAHCertificados

        Dim command As New SqlCommand("sp_obtener_certificado", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idCertificado", idCertificado)

        If CBool(command.ExecuteNonQuery) Then
            Dim da = New SqlDataAdapter(command)

            Dim ds As New DataSet
            da.Fill(dt)
            table = dt

            For Each row As DataRow In dt.Rows
                certificado = New SAHCertificados(Convert.ToInt32(row("idCertificado")), Convert.ToDateTime(row("fechaGenerado")),
                                     Convert.ToDateTime(row("fechaVencimiento")), Convert.ToString(row("estadoVencimiento")),
                                     Convert.ToInt32(row("reimpreso")), Convert.ToBoolean(row("pago")), Convert.ToInt32(row("idAhorro")),
                                     Convert.ToInt32(row("idTipoCertificado")))
            Next

        Else
            MsgBox("Error al realizar la consulta", MsgBoxStyle.Critical, "SAH")
        End If

        Return certificado

    End Function


    Function actualizarCertificado(idCertificado As Integer, fechaVencimiento As Date, estadoVencimiento As String) As String

        Dim sqlSP As String = "sp_actualizar_certificado"
        Dim cmdActualizar As New SqlCommand(sqlSP, Conn)
        cmdActualizar.CommandType = System.Data.CommandType.StoredProcedure
        cmdActualizar.Parameters.Add(New SqlParameter("@idCertificado", idCertificado))
        cmdActualizar.Parameters.Add(New SqlParameter("@fechaVencimiento", fechaVencimiento))
        cmdActualizar.Parameters.Add(New SqlParameter("@estadoVencimiento", estadoVencimiento))

        Try

            cmdActualizar.ExecuteNonQuery()
            Return "Certificado actualizado exitosamente."

        Catch e As SqlException

            Return "Ha ocurrido un error al actualizar los datos del certificado."

        End Try

    End Function

End Class

﻿Imports System.Data.SqlClient
Imports System.Windows.Forms

Public Class SAHDataParientesAhorros

    Sub insertarParienteAhorro(idPariente As Integer, idAhorro As Integer)

        Dim sqlSP As String = "sp_insertar_pariente_ahorro"
        Dim cmdInsertar As New SqlCommand(sqlSP, Conn)
        cmdInsertar.CommandType = System.Data.CommandType.StoredProcedure
        cmdInsertar.Parameters.Add(New SqlParameter("@idPariente", idPariente))
        cmdInsertar.Parameters.Add(New SqlParameter("@idAhorro", idAhorro))

        Try

            cmdInsertar.ExecuteNonQuery()

        Catch e As SqlException

        End Try

    End Sub

    Sub eliminarParienteAhorro(idPariente As Integer, idAhorro As Integer)

        Dim sqlSP As String = "sp_eliminar_pariente_ahorro"
        Dim cmdEliminar As New SqlCommand(sqlSP, Conn)
        cmdEliminar.CommandType = System.Data.CommandType.StoredProcedure
        cmdEliminar.Parameters.Add(New SqlParameter("@idPariente", idPariente))
        cmdEliminar.Parameters.Add(New SqlParameter("@idAhorro", idAhorro))

        Try
            cmdEliminar.ExecuteNonQuery()

        Catch e As SqlException

        End Try

    End Sub

    Function verificarExisteParienteAhorro(idPariente As Integer, idAhorro As Integer) As Boolean

        Dim sqlSP As String = "sp_verificar_parientes_ahorro"
        Dim command As New SqlCommand(sqlSP, Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idPariente", idPariente)
        command.Parameters.AddWithValue("@idAhorro", idAhorro)


        Dim ds As SqlDataReader
        ds = command.ExecuteReader

        If CBool(ds.Read) = True Then
            verificarExisteParienteAhorro = True
        Else
            verificarExisteParienteAhorro = False
        End If

        ds.Close()
        Return verificarExisteParienteAhorro

    End Function

    Function getCedulaPariente(idPariente As Integer) As String
        Dim cedula As String = ""

        Dim command As New SqlCommand("sp_get_cedula_pariente", Conn)
        command.CommandType = CommandType.StoredProcedure
        command.Parameters.AddWithValue("@idPariente", idPariente)

        Dim ds As SqlDataReader
        ds = command.ExecuteReader

        If CBool(ds.Read) = True Then
            cedula = ds("cedula")
        End If

        ds.Close()
        Return cedula

    End Function


    Function getIdPariente(idPersona As Integer) As Integer

        Dim idPariente As Integer
        Dim rs As New SqlCommand("select idPariente from Parientes where idPersona = " & idPersona & "", Conn)
        Dim ds As SqlDataReader
        ds = rs.ExecuteReader

        If ds.Read Then
            idPariente = ds.Item("idPariente")
        End If

        ds.Close()
        Return idPariente

    End Function


    Function getListaParientesAsociado(idAsociado As Integer) As DataTable

        Dim adapter As SqlDataAdapter
        Dim param As SqlParameter
        Dim dt As New DataTable

        Dim sqlSP As String = "sp_consultar_parientes_asociado"
        Dim command As New SqlCommand(sqlSP, Conn)
        command.CommandType = CommandType.StoredProcedure


        param = New SqlParameter("@idAsociado", idAsociado)
        param.Direction = ParameterDirection.Input
        param.DbType = DbType.Int32
        command.Parameters.Add(param)

        adapter = New SqlDataAdapter(command)
        adapter.Fill(dt)
        Return dt


    End Function

End Class

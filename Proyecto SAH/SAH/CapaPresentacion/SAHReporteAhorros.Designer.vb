﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SAHReporteAhorros
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.DataSpReportes = New SAH.DataSpReportes()
        Me.sp_consultar_beneficiarios_ahorro_escolarBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_consultar_beneficiarios_ahorro_escolarTableAdapter = New SAH.DataSpReportesTableAdapters.sp_consultar_beneficiarios_ahorro_escolarTableAdapter()
        Me.sp_consultar_beneficiarios_otros_ahorrosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.sp_consultar_beneficiarios_otros_ahorrosTableAdapter = New SAH.DataSpReportesTableAdapters.sp_consultar_beneficiarios_otros_ahorrosTableAdapter()
        CType(Me.DataSpReportes, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_consultar_beneficiarios_ahorro_escolarBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.sp_consultar_beneficiarios_otros_ahorrosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        ReportDataSource1.Name = "DataSetBeneficiariosOtrosAhorros"
        ReportDataSource1.Value = Me.sp_consultar_beneficiarios_otros_ahorrosBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "SAH.SAHRtpBeneficiariosOtrosAhorros.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(-1, 1)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.ServerReport.BearerToken = Nothing
        Me.ReportViewer1.Size = New System.Drawing.Size(959, 451)
        Me.ReportViewer1.TabIndex = 0
        '
        'DataSpReportes
        '
        Me.DataSpReportes.DataSetName = "DataSpReportes"
        Me.DataSpReportes.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'sp_consultar_beneficiarios_ahorro_escolarBindingSource
        '
        Me.sp_consultar_beneficiarios_ahorro_escolarBindingSource.DataMember = "sp_consultar_beneficiarios_ahorro_escolar"
        Me.sp_consultar_beneficiarios_ahorro_escolarBindingSource.DataSource = Me.DataSpReportes
        '
        'sp_consultar_beneficiarios_ahorro_escolarTableAdapter
        '
        Me.sp_consultar_beneficiarios_ahorro_escolarTableAdapter.ClearBeforeFill = True
        '
        'sp_consultar_beneficiarios_otros_ahorrosBindingSource
        '
        Me.sp_consultar_beneficiarios_otros_ahorrosBindingSource.DataMember = "sp_consultar_beneficiarios_otros_ahorros"
        Me.sp_consultar_beneficiarios_otros_ahorrosBindingSource.DataSource = Me.DataSpReportes
        '
        'sp_consultar_beneficiarios_otros_ahorrosTableAdapter
        '
        Me.sp_consultar_beneficiarios_otros_ahorrosTableAdapter.ClearBeforeFill = True
        '
        'SAHReporteAhorros
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(960, 450)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "SAHReporteAhorros"
        Me.Text = "ReporteAhorros"
        CType(Me.DataSpReportes, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_consultar_beneficiarios_ahorro_escolarBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.sp_consultar_beneficiarios_otros_ahorrosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents sp_consultar_beneficiarios_ahorro_escolarBindingSource As BindingSource
    Friend WithEvents DataSpReportes As DataSpReportes
    Friend WithEvents sp_consultar_beneficiarios_ahorro_escolarTableAdapter As DataSpReportesTableAdapters.sp_consultar_beneficiarios_ahorro_escolarTableAdapter
    Friend WithEvents sp_consultar_beneficiarios_otros_ahorrosBindingSource As BindingSource
    Friend WithEvents sp_consultar_beneficiarios_otros_ahorrosTableAdapter As DataSpReportesTableAdapters.sp_consultar_beneficiarios_otros_ahorrosTableAdapter
End Class

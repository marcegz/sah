﻿Imports System.Drawing
Public Class SAHFrmListaParientesAhorro

    Dim negocioAhorros As New SAHNegocioAhorros
    Dim negocioParientesAhorros As New SAHNegocioParientesAhorro
    Dim frmAhorro As SAHFrmAhorro

    Public Sub New()
        InitializeComponent()
    End Sub


    Public Sub New(idAsociado As Integer, frmAhorroU As SAHFrmAhorro)
        InitializeComponent()
        frmAhorro = frmAhorroU
        cargaDatos(idAsociado)
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles BtnCerrar.Click
        Me.Close()
    End Sub

    Sub cargaDatos(idAsociado As Integer)
        dgvParientes.DataSource = negocioParientesAhorros.getListaParientesAsociado(idAsociado)
        modificaAtributosTabla()
    End Sub

    Sub marcarPariente()
        For Each oRow As DataGridViewRow In dgvParientes.Rows

            If oRow.Cells(1).Value = frmAhorro.txtCedulaAutorizado.Text Then

                oRow.Cells(0).Value = True

            End If
        Next
    End Sub

    Sub modificaAtributosTabla()
        dgvParientes.Columns(1).HeaderText = "Identificación"
        dgvParientes.Columns(2).HeaderText = "Nombre"
        dgvParientes.Columns(3).HeaderText = "Apellido Uno"
        dgvParientes.Columns(4).HeaderText = "Apellido Dos"
        dgvParientes.Columns(5).HeaderText = "Parentesco"
    End Sub


    Private Sub btnCancelar_Click(sender As Object, e As EventArgs)
        Me.Hide()
    End Sub


    Function g() As Boolean
        For Each oRow As DataGridViewRow In dgvParientes.Rows

            If oRow.Cells(0).Value = True Then

                g = True

            End If
        Next

        Return g

    End Function

    Private Sub dgvParientes_RowPostPaint(sender As Object, e As DataGridViewRowPostPaintEventArgs) Handles dgvParientes.RowPostPaint

        Dim cell As DataGridViewCell
        Dim n As Integer = 0

        If g() Then

            For Each oRow As DataGridViewRow In dgvParientes.Rows

                If oRow.Cells(0).Value = False Then


                    cell = dgvParientes.Rows(n).Cells(0)
                    cell.ReadOnly = True

                End If
                n = n + 1

            Next

        Else

            For Each oRow As DataGridViewRow In dgvParientes.Rows

                cell = dgvParientes.Rows(n).Cells(0)
                cell.ReadOnly = False


                n = n + 1

            Next

        End If

    End Sub


    Private Sub btnCancelar_Click_1(sender As Object, e As EventArgs) Handles btnCancelar.Click
        Me.Hide()
    End Sub

    Private Sub btnGuardar_Click_1(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim cedula As String

        For Each oRow As DataGridViewRow In dgvParientes.Rows

            cedula = oRow.Cells(1).Value

            If oRow.Cells(0).Value = True Then
                frmAhorro.refrescarTxtIdAutorizado(cedula)

            End If
        Next


        Me.Hide()
    End Sub

    Private Sub FrmListaParientesAhorro_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        marcarPariente()
    End Sub
End Class
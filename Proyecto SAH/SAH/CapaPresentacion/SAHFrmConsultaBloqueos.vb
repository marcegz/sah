﻿Public Class SAHFrmConsultaBloqueos

    Dim fPrincipal As New SAHFrmPrincipal
    Dim negocioBloqueos As New SAHNegocioBloqueos

    Private Sub FrmConsultaBloqueos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarTablaBloqueos()
    End Sub


    Sub cargarTablaBloqueos()
        dgvBloqueos.DataSource = negocioBloqueos.getTablaBloqueos(txtConsultar.Text)
        modificaAtributosTabla()
    End Sub

    Sub modificaAtributosTabla()
        dgvBloqueos.Columns(0).HeaderText = "Tipo"
        dgvBloqueos.Columns(1).HeaderText = "Descripción"
        dgvBloqueos.Columns(2).HeaderText = "Fecha"
        dgvBloqueos.Columns(3).HeaderText = "Nombre Ahorro"
        dgvBloqueos.Columns(4).HeaderText = "Nombre Tipo Ahorro"
    End Sub

    Private Sub txtConsultar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtConsultar.KeyUp
        cargarTablaBloqueos()
    End Sub

    Private Sub btnGenerarReporte_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click
        Dim reporte As New SAHReporteBloqueos
        reporte.Show()
    End Sub

End Class
﻿Public Class SAHFrmCertificados

    Dim negociosTiposCertificados As New SAHNegocioTiposCertificados
    Dim negociosCertificados As New SAHNegocioCertificados
    Dim negociosAhorros As New SAHNegocioAhorros
    Dim certificado As SAHCertificados
    Dim frmPrincipal As SAHFrmPrincipal
    Dim frmConsultasCertificado As SAHFrmConsultaCertificados
    Dim n As Integer = 3
    Dim idUsuario As Integer

    Private Sub FrmCertificados_Load(sender As Object, e As EventArgs) Handles MyBase.Load



    End Sub

    Public Sub New()
        InitializeComponent()
        cambiarCodigo()
        cargarComboTiposCertificados()
        cargarComboAhorros()
        cbAhorros.SelectedIndex = 0
        cbTiposCertificados.SelectedIndex = 0

        n = 0
    End Sub

    Public Sub New(certificadoU As SAHCertificados, frmPrincipalU As SAHFrmPrincipal, frmConsultasCertificadoU As SAHFrmConsultaCertificados, idUsuarioU As Integer)
        InitializeComponent()
        certificado = certificadoU
        frmPrincipal = frmPrincipalU
        idUsuario = idUsuarioU
        cargarComboTiposCertificados()
        cargarComboAhorros()
        frmConsultasCertificado = frmConsultasCertificadoU
        cargarCertificado()
        lEstado.Visible = True
        cbEstado.Visible = True
        cbReimpreso.Visible = True
        lReimpreso.Visible = True
        cbReimpreso.Enabled = False
        dtpFecha.Enabled = False
        cbEstado.Enabled = False
        cbTiposCertificados.Enabled = False
        cbAhorros.Enabled = False

        If negociosAhorros.verificarUsuarioAdmin(idUsuario) Then
            btnEditar.Visible = True
            btnAtras.Visible = True
        Else
            btnAtras.Visible = True
        End If
        n = 1
    End Sub

    Sub cargarCertificado()
        txtId.Text = certificado.IdCertificado1
        cbTiposCertificados.SelectedValue = certificado.IdTipoCertificado1
        cbAhorros.SelectedValue = certificado.IdAhorro1
        dtpFecha.Value = certificado.FechaVencimiento1

        If certificado.EstadoVencimiento1 = "Vigente" Then
            cbEstado.SelectedIndex = 0
        Else

            If certificado.EstadoVencimiento1 = "Cancelado" Then
                cbEstado.SelectedIndex = 1
            Else
                cbEstado.SelectedIndex = 2
            End If
        End If

        If certificado.Reimpreso1 > 1 Then
            cbReimpreso.Checked = True
        End If

    End Sub

    Sub cargarComboTiposCertificados()

        cbTiposCertificados.DataSource = negociosTiposCertificados.getTablaConsultaTiposCertificados("")
        cbTiposCertificados.DisplayMember = "nombre"
        cbTiposCertificados.ValueMember = "idTipoCertificado"

    End Sub

    Sub cargarComboAhorros()

        cbAhorros.DataSource = negociosAhorros.getAhorrosCombo()
        cbAhorros.DisplayMember = "nombre"
        cbAhorros.ValueMember = "idAhorro"

    End Sub

    Sub cambiarCodigo()
        txtId.Text = negociosCertificados.CodigoNuevoIDCertificado()
    End Sub

    Sub limpiarForm()
        txtId.Text = negociosCertificados.CodigoNuevoIDCertificado()
        cbTiposCertificados.SelectedIndex = 0
        cbAhorros.SelectedIndex = 0
        dtpFecha.Value = Date.Now
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        If n = 0 Then
            Dim certificado As New SAHCertificados(Date.Now, dtpFecha.Value, "  Vigente", 0, 0, cbAhorros.SelectedValue, cbTiposCertificados.SelectedValue)

            MsgBox(negociosCertificados.insertarCertificado(certificado), MsgBoxStyle.Information, "Mensaje Información")
            limpiarForm()

        Else

            MsgBox(negociosCertificados.actualizarCertificado(txtId.Text, dtpFecha.Value, cbEstado.SelectedItem.ToString), MsgBoxStyle.Information, "Mensaje Información")
            frmPrincipal.AbrirFormEnPanel(frmConsultasCertificado)
        End If



    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click

        If MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then

            If cbReimpreso.Visible = True Then
                frmPrincipal.AbrirFormEnPanel(frmConsultasCertificado)
            Else
                frmPrincipal.AbrirFormEnPanel(New SAHFrmInicio)

            End If

        End If

    End Sub

    Private Sub btnAtras_Click(sender As Object, e As EventArgs) Handles btnAtras.Click

        frmPrincipal.AbrirFormEnPanel(frmConsultasCertificado)
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        dtpFecha.Enabled = True
        cbEstado.Enabled = True
        btnAtras.Visible = False
        btnEditar.Visible = False
    End Sub
End Class
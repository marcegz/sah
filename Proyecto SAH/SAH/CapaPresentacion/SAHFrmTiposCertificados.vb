﻿Public Class SAHFrmTiposCertificados

    Dim negocioTiposCertificados As New SAHNegocioTiposCertificados
    Dim tipoCertificado As SAHTiposCertificados
    Dim frmConsultasTiposCertificados As SAHFrmConsultaTiposCertificados
    Dim frmPrincipal As SAHFrmPrincipal
    Dim n As Integer = 3

    Private Sub FrmTiposCertificados_Load(sender As Object, e As EventArgs) Handles MyBase.Load
    End Sub

    Public Sub New()
        InitializeComponent()
        CargarCodigoTipoCertificado()
        n = 0

    End Sub

    Public Sub New(tipoCertificadoU As SAHTiposCertificados, frmPrincipalU As SAHFrmPrincipal, frmConsultasTiposCertificadosU As SAHFrmConsultaTiposCertificados)
        InitializeComponent()
        frmConsultasTiposCertificados = frmConsultasTiposCertificadosU
        frmPrincipal = frmPrincipalU
        tipoCertificado = tipoCertificadoU

        txtDescripcion.Enabled = False
        txtNombre.Enabled = False
        txtMonto.Enabled = False
        cargarDatos()
        btnAtras.Visible = True
        btnEditar.Visible = True
        n = 1
    End Sub

    Sub cargarDatos()
        txtId.Text = tipoCertificado.IdTipoCertificado1
        txtNombre.Text = tipoCertificado.Nombre1
        txtDescripcion.Text = tipoCertificado.Descripcion1
        txtMonto.Text = tipoCertificado.Monto1
    End Sub


    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        If n = 0 Then

            If (txtNombre.Text = "" And txtMonto.Text = "") Then

                MsgBox("Verifique que los campos esten completos.", MsgBoxStyle.Information, "SAH")

            Else
                Dim tipoCertificado As New SAHTiposCertificados(txtNombre.Text, txtDescripcion.Text, 0, txtMonto.Text)

                MsgBox(negocioTiposCertificados.insertarTipoCertificado(tipoCertificado), MsgBoxStyle.Information, "Mensaje Información")
                Limpiar()
                CargarCodigoTipoCertificado()
            End If

        Else

            Dim tipoCertificado As New SAHTiposCertificados
                If cbEstado.SelectedItem = "Activo" Then

                    tipoCertificado = New SAHTiposCertificados(txtId.Text, txtNombre.Text, txtDescripcion.Text, 1, txtMonto.Text)
                Else
                    tipoCertificado = New SAHTiposCertificados(txtId.Text, txtNombre.Text, txtDescripcion.Text, 0, txtMonto.Text)

                End If

                MsgBox(negocioTiposCertificados.actualizarTipoCertificado(tipoCertificado), MsgBoxStyle.Information, "Mensaje Información")
                frmPrincipal.AbrirFormEnPanel(frmConsultasTiposCertificados)



        End If

    End Sub

    Sub CargarCodigoTipoCertificado()
        txtId.Text = negocioTiposCertificados.CodigoNuevoTiposCertificados()
    End Sub

    Sub Limpiar()
        txtNombre.Text = ""
        txtDescripcion.Text = ""
        txtMonto.Text = ""
        txtNombre.Focus()
    End Sub

    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click
        txtDescripcion.Enabled = True
        txtNombre.Enabled = True
        txtMonto.Enabled = True
        btnEditar.Visible = False
        btnAtras.Visible = False
    End Sub

    Private Sub btnAtras_Click(sender As Object, e As EventArgs) Handles btnAtras.Click
        frmPrincipal.AbrirFormEnPanel(frmConsultasTiposCertificados)
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then

            If n = 0 Then
                frmPrincipal.AbrirFormEnPanel(New SAHFrmInicio)
            Else
                frmPrincipal.AbrirFormEnPanel(frmConsultasTiposCertificados)

            End If

        End If
    End Sub


    Function SoloNumeros(e As KeyPressEventArgs) As Boolean


        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False

        Else

            If (Char.IsControl(e.KeyChar)) Then
                e.Handled = False
            Else
                e.Handled = True
                MsgBox("Por favor, digite solo números.", MsgBoxStyle.Critical, "Información")

            End If

        End If

        Return e.Handled

    End Function

    Private Sub txtMonto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMonto.KeyPress

        SoloNumeros(e)
    End Sub

End Class
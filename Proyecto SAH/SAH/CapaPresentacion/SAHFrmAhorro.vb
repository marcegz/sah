﻿Public Class SAHFrmAhorro

    Dim negocioAhorros As New SAHNegocioAhorros
    Dim negocioTiposAhorros As New SAHNegocioTiposAhorros
    Dim negocioTasasInteres As New SAHNegocioTasasInteres
    Dim negocioParientesAhorro As New SAHNegocioParientesAhorro
    Dim negocioBloqueos As New SAHNegocioBloqueos
    Dim negocioCupones As New SAHNegocioCupones
    Dim negocioCertificados As New SAHNegocioCertificados
    Dim nuevoCodigoAhorro As Integer
    Dim dias As Integer
    Dim frecuenciaInteres As Integer
    Dim frecuenciaCuota
    Dim diaAplicacion As Integer
    Dim tipoPago As Integer
    Dim idUsuario As Integer
    Dim frmPrincipal As SAHFrmPrincipal
    Dim ahorro As SAHAhorros
    Dim n As Integer = 3


    Public Sub New(idUsuarioU As Integer, frmPrincipalU As SAHFrmPrincipal)

        InitializeComponent()
        idUsuario = idUsuarioU
        frmPrincipal = frmPrincipalU
        CargarCodigoAhorro()
        cargarComboTiposAhorros()
        cbMoneda.SelectedIndex = 0

        cbDiaAplicacion.SelectedIndex = 0
        cbIntereses.SelectedIndex = 0
        cbTipoPago.SelectedIndex = 0
        cbCuotas.SelectedIndex = 0
        cbDuracion.SelectedIndex = 0

        n = 0

        Try
            cbTiposAhorros.SelectedIndex = 0
            dtpFinalizacion.Value = negocioTiposAhorros.getFecha(cbTiposAhorros.SelectedItem("nombre"))

        Catch ex As Exception

            MsgBox("Por favor, realice el registro de al menos un tipo de ahorro.", MsgBoxStyle.Information, "Mensaje Información")


        End Try



    End Sub

    Public Sub New(frmPrincipalU As SAHFrmPrincipal, ahorroU As SAHAhorros)
        InitializeComponent()
        ahorro = ahorroU
        frmPrincipal = frmPrincipalU
        lDuracion.Visible = True
        cbDuracion.Visible = False
        lTiempoDuracion.Visible = True
        lEstado.Visible = True
        cbEstado.Visible = True
        lMontoAhorrado.Visible = True
        lMonto.Visible = True
        lTasaInteres.Visible = True
        lPorcentaje.Visible = True
        lInteres.Visible = True
        lMontoInteres.Visible = True
        btnGuardar.Visible = False
        btnActualizar.Visible = True
        BtnConsultarBloqueos.Visible = True
        BtnSeleccionarBeneficiarios.Visible = True
        txtIdAhorro.Enabled = False
        txtNombreAhorro.Enabled = False
        txtMontoCuota.Enabled = False
        cbDiaAplicacion.Enabled = False
        cbIntereses.Enabled = False
        cbMoneda.Enabled = False
        cbCuotas.Enabled = False
        txtCedulaAsociado.Enabled = False
        cbEstado.Enabled = False
        cbDuracion.Enabled = False
        cbTiposAhorros.Enabled = False
        dtpInicio.Enabled = False
        cbTipoPago.Enabled = False
        btnBuscarAutorizado.Visible = False
        btnAtras.Visible = True

        n = 1
        cargarComboTiposAhorros()
        cargarAhorro()

    End Sub
    Private Sub cargarAhorro()

        txtIdAhorro.Text = ahorro.IdAhorro1
        txtNombreAhorro.Text = ahorro.NombreAhorro1
        lMonto.Text = ahorro.MontoAhorrado1
        txtMontoCuota.Text = ahorro.MontoCuota1
        cbDiaAplicacion.SelectedIndex = ahorro.DiaAplicacion1 - 1
        cbIntereses.SelectedIndex = definirFrecuenciaInteres(ahorro.FrecuenciaIntereses1)
        cbCuotas.SelectedIndex = definirFrecuenciaCuota(ahorro.FrecuenciaCuota1)
        txtCedulaAsociado.Text = ahorro.Cedula1
        txtNombreAsociado.Text = negocioAhorros.getNombre(ahorro.Cedula1)
        txtCedulaAutorizado.Text = negocioParientesAhorro.getCedulaPariente(ahorro.IdAutorizado1)
        txtNombreAutorizado.Text = negocioAhorros.getNombre(txtCedulaAutorizado.Text)
        dtpInicio.Value = ahorro.FechaInicio1
        dtpFinalizacion.Value = ahorro.FechaFinal1
        cbTiposAhorros.SelectedValue = ahorro.IdTipoAhorro1
        lPorcentaje.Text = negocioTasasInteres.getPorcentaje(ahorro.Duracion1, ahorro.IdTipoAhorro1)
        lMontoInteres.Text = negocioCupones.getIntereses(negocioCertificados.getIdCertificadoPlazo(ahorro.IdAhorro1))
        negocioCupones.getListaFechasCupones(ahorro.FechaInicio1, ahorro.FechaFinal1, ahorro.FrecuenciaIntereses1)

        If negocioTiposAhorros.getPersonalizado(ahorro.NombreTipoAhorro1) Then
            lTiempoDuracion.Text = ahorro.Duracion1 / 30 & " meses"
        Else
            lTiempoDuracion.Text = getDuracion()
        End If

        If ahorro.Moneda1 = "Colones" Then
            cbMoneda.SelectedIndex = 0
        Else
            cbMoneda.SelectedIndex = 1
        End If

        If ahorro.Tipo_pago1 = 0 Then
            cbTipoPago.SelectedIndex = 0
        Else
            cbTipoPago.SelectedIndex = 1
        End If

        If ahorro.Estado1 = "Vigente" Then
            cbEstado.SelectedIndex = 0
        Else
            cbEstado.SelectedIndex = 1
        End If

    End Sub

    Function getDuracion() As String

        Dim duracion As String = ""
        Dim tiempo As Integer = 0
        Dim meses As Integer = 0
        Dim dias As Integer = 0
        tiempo = ahorro.Duracion1

        While tiempo > 0

            If tiempo >= 30 Then
                meses += 1
                tiempo -= 30

            Else
                dias = tiempo
                tiempo = 0
            End If

        End While

        duracion = meses & " meses y " & dias & " días"

        Return duracion

    End Function


    Sub CargarCodigoAhorro()
        txtIdAhorro.Text = negocioAhorros.CodigoNuevoAhorro
    End Sub

    Sub cargarComboTiposAhorros()

        cbTiposAhorros.DataSource = negocioTiposAhorros.getTiposAhorrosActivos()
        cbTiposAhorros.DisplayMember = "nombre"
        cbTiposAhorros.ValueMember = "idTipoAhorro"

    End Sub


    Private Sub btnBuscarAutorizado_Click(sender As Object, e As EventArgs) Handles btnBuscarAutorizado.Click
        Dim frm As New SAHFrmListaParientesAhorro(negocioAhorros.getIdAsociado(txtCedulaAsociado.Text), Me)
        frm.Show()
    End Sub

    Private Sub BtnSeleccionarBeneficiarios_Click(sender As Object, e As EventArgs) Handles BtnSeleccionarBeneficiarios.Click
        Dim fr As New SAHFrmSeleccionarBeneficiarios(negocioAhorros.getIdAsociado(txtCedulaAsociado.Text), Integer.Parse(txtIdAhorro.Text))
        fr.Show()

    End Sub

    Private Sub BtnConsultarBloqueos_Click(sender As Object, e As EventArgs) Handles BtnConsultarBloqueos.Click
        Dim frm As New SAHFrmConsultaBloqueosAhorro(ahorro.IdAhorro1)
        frm.Show()
    End Sub


    Public Sub refrescarTxtIdAutorizado(cedula As String)
        txtCedulaAutorizado.Text = cedula
        txtNombreAutorizado.Text = negocioAhorros.getNombre(cedula)
    End Sub



    Private Sub definirDias()

        Dim eleccion As String = cbDuracion.SelectedItem

        Select Case eleccion

            Case "3 Meses"
                dias = 90

            Case "4 Meses"
                dias = 120

            Case "5 Meses"
                dias = 150

            Case "6 Meses"
                dias = 180

            Case "7 Meses"
                dias = 210

            Case "8 Meses"
                dias = 240

            Case "9 Meses"
                dias = 270

            Case "12 Meses"
                dias = 360

            Case "18 Meses"
                dias = 540

            Case "24 Meses"
                dias = 720

            Case "36 Meses"
                dias = 1080

            Case "48 Meses"
                dias = 1440

            Case "60 Meses"
                dias = 1800

        End Select

    End Sub

    Private Sub definirFrecuenciaInteres()

        Dim eleccion As String = cbIntereses.SelectedItem

        Select Case eleccion

            Case "Mensual"
                frecuenciaInteres = 30

            Case "Trimestral"
                frecuenciaInteres = 90

            Case "Semestral"
                frecuenciaInteres = 180

            Case "Anual"
                frecuenciaInteres = 360

        End Select

    End Sub

    Private Function definirFrecuenciaInteres(interes As Integer) As Integer

        Dim seleccion As Integer
        Dim eleccion As Integer = interes

        Select Case eleccion

            Case 30
                seleccion = 0

            Case 60
                seleccion = 1

            Case 180
                seleccion = 2

            Case 360
                seleccion = 3

        End Select

        Return seleccion

    End Function

    Private Sub definirDiaAplicacion()

        Dim eleccion As String = cbDiaAplicacion.SelectedItem

        Select Case eleccion

            Case "Lunes"
                diaAplicacion = 1

            Case "Martes"
                diaAplicacion = 2

            Case "Miércoles"
                diaAplicacion = 3

            Case "Jueves"
                diaAplicacion = 4

            Case "Viernes"
                diaAplicacion = 5

            Case "Sábado"
                diaAplicacion = 6

            Case "Domingo"
                diaAplicacion = 7

        End Select

    End Sub

    Private Sub definirTipoPago()

        Dim eleccion As String = cbTipoPago.SelectedItem

        Select Case eleccion

            Case "Planilla"
                tipoPago = 0

            Case "Depósito"
                tipoPago = 1

        End Select

    End Sub

    Private Sub definirFrecuenciaCuota()

        Dim eleccion As String = cbCuotas.SelectedItem

        Select Case eleccion

            Case "Semanal"
                frecuenciaCuota = 7

            Case "Quincenal"
                frecuenciaCuota = 14

            Case "Mensual"
                frecuenciaCuota = 28

        End Select

    End Sub

    Private Function definirFrecuenciaCuota(frecuencia As Integer) As Integer

        Dim cuota As Integer = 0

        Select Case frecuencia

            Case 7
                cuota = 0

            Case 14
                cuota = 1

            Case 28
                cuota = 2

        End Select

        Return cuota

    End Function

    Private Sub cbDuracion_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbDuracion.SelectedValueChanged
        definirDias()
        dtpFinalizacion.Value = dtpInicio.Value.Date.AddDays(dias)

    End Sub

    Private Sub dtpInicio_ValueChanged(sender As Object, e As EventArgs) Handles dtpInicio.ValueChanged

        definirDias()
        dtpFinalizacion.Value = dtpInicio.Value.Date.AddDays(dias)

    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        Dim ahorro As SAHAhorros
        Dim idTipoAhorro As Integer = negocioTiposAhorros.getIdTipoAhorro(cbTiposAhorros.SelectedItem("nombre"))
        Dim idAsociado As Integer = negocioAhorros.getIdAsociado(txtCedulaAsociado.Text)
        Dim idAutorizado As Integer = negocioParientesAhorro.getIdPariente(negocioAhorros.getIdPersona(txtCedulaAutorizado.Text))

        Dim fechaInicio As Date = dtpInicio.Value
        Dim fechaFinal As Date = dtpFinalizacion.Value
        Dim diasA As Integer = DateDiff("d", fechaInicio, fechaFinal)
        Dim monto As Double = Double.Parse(txtMontoCuota.Text)

        If n = 0 Then

            definirFrecuenciaInteres()
            definirFrecuenciaCuota()
            definirDiaAplicacion()
            definirTipoPago()

            If (txtNombreAhorro.Text = "" Or txtCedulaAsociado.Text = "" Or txtNombreAsociado.Text = "" Or
            txtCedulaAutorizado.Text = "" Or txtNombreAutorizado.Text = "" Or txtMontoCuota.Text = "") Then

                MsgBox("Verifique que los campos esten completos.", MsgBoxStyle.Critical, "SAH")

            Else
                If negocioTiposAhorros.getPersonalizado(cbTiposAhorros.SelectedItem("nombre")) Then

                    ahorro = New SAHAhorros(txtNombreAhorro.Text, dias, dtpInicio.Value, dtpFinalizacion.Value, diaAplicacion,
                                     frecuenciaInteres, frecuenciaCuota, cbMoneda.SelectedItem, monto, 0, "Vigente", tipoPago,
                                     idTipoAhorro, idAsociado, idAutorizado, idUsuario)
                Else
                    ahorro = New SAHAhorros(txtNombreAhorro.Text, diasA, dtpInicio.Value, dtpFinalizacion.Value, diaAplicacion,
                                     frecuenciaInteres, frecuenciaCuota, cbMoneda.SelectedItem, monto, 0,
                                     "Vigente", tipoPago, idTipoAhorro, idAsociado, idAutorizado, idUsuario)
                End If

                MsgBox(negocioAhorros.insertarAhorro(ahorro), MsgBoxStyle.Information, "Mensaje Información")
                Dim certificado As New SAHCertificados(Date.Now, dtpFinalizacion.Value, "Vigente", 1, 0, negocioAhorros.CodigoNuevoAhorro - 1, 1)
                negocioCertificados.insertarCertificado(certificado)
                limpliarForm()

            End If

        Else

            Dim estado As String = ""

            If cbEstado.SelectedItem("nombre") = "Cancelado" Then
                estado = "Finalizado"
            End If

            MsgBox(negocioAhorros.actualizarAhorro(txtIdAhorro.Text, txtNombreAhorro.Text, estado, idAutorizado), MsgBoxStyle.Information, "Mensaje Información")

            If estado = "Finalizado" Then
                Dim bloqueo = New SAHBloqueos("Estado", "El ahorro ha sido cancelado o finalizado", Date.Now, Integer.Parse(txtIdAhorro.Text))
                negocioBloqueos.insertarBloqueo(bloqueo)
            End If

            btnGuardar.Visible = False
            btnCancelar.Visible = False
            btnActualizar.Visible = True
            btnAtras.Visible = True
            txtNombreAhorro.Enabled = False
            cbEstado.Enabled = False
            btnBuscarAutorizado.Enabled = False
            BtnSeleccionarBeneficiarios.Enabled = False
            BtnConsultarBloqueos.Enabled = False

        End If

    End Sub

    Sub limpliarForm()
        CargarCodigoAhorro()
        txtNombreAhorro.Text = ""
        cbMoneda.SelectedIndex = 0
        cbTiposAhorros.SelectedIndex = 0
        cbDiaAplicacion.SelectedIndex = 0
        cbIntereses.SelectedIndex = 0
        cbTipoPago.SelectedIndex = 0
        cbCuotas.SelectedIndex = 0
        cbDuracion.SelectedIndex = 0
        txtCedulaAsociado.Text = ""
        txtNombreAsociado.Text = ""
        txtCedulaAutorizado.Text = ""
        txtNombreAutorizado.Text = ""
        txtMontoCuota.Text = ""
        txtNombreAhorro.Focus()
    End Sub



    Function SoloNumeros(e As KeyPressEventArgs) As Boolean


        If Char.IsDigit(e.KeyChar) Then
            e.Handled = False

        Else

            If (Char.IsControl(e.KeyChar)) Then
                e.Handled = False
            Else
                e.Handled = True
                MsgBox("Por favor, digite solo números.", MsgBoxStyle.Critical, "Información")

            End If

        End If

        Return e.Handled

    End Function

    Private Sub txtMontoCuota_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtMontoCuota.KeyPress

        SoloNumeros(e)
    End Sub


    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then

            If BtnConsultarBloqueos.Visible = True And btnGuardar.Visible = True Then
                frmPrincipal.AbrirFormEnPanel(New SAHFrmConsultaAhorros(frmPrincipal))
            Else
                frmPrincipal.AbrirFormEnPanel(New SAHFrmInicio)

            End If

        End If
    End Sub

    Private Sub cbTiposAhorros_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbTiposAhorros.SelectedValueChanged

        If n = 0 Then

            If negocioTiposAhorros.getPersonalizado(cbTiposAhorros.SelectedItem("nombre")) = False Then
                dtpFinalizacion.Value = negocioTiposAhorros.getFecha(cbTiposAhorros.SelectedItem("nombre"))
            Else
                lDuracion.Visible = True
                cbDuracion.Visible = True
                definirDias()
                dtpFinalizacion.Value = dtpInicio.Value.Date.AddDays(dias)
            End If

        End If

    End Sub

    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        txtNombreAhorro.Enabled = True
        cbEstado.Enabled = True
        btnBuscarAutorizado.Visible = True
        btnGuardar.Visible = True
        btnCancelar.Visible = True
        btnActualizar.Visible = False
        BtnConsultarBloqueos.Enabled = True
        BtnSeleccionarBeneficiarios.Enabled = True
        txtNombreAhorro.Focus()
        btnAtras.Visible = False
    End Sub

    Private Sub btnAtras_Click(sender As Object, e As EventArgs) Handles btnAtras.Click
        frmPrincipal.AbrirFormEnPanel(New SAHFrmConsultaAhorros(frmPrincipal))
    End Sub

    Private Sub txtCedulaAsociado_KeyDown(sender As Object, e As KeyEventArgs) Handles txtCedulaAsociado.KeyDown
        If e.KeyCode = Keys.Enter Then
            txtNombreAsociado.Text = negocioAhorros.getNombre(txtCedulaAsociado.Text)
        End If
    End Sub

    Private Sub SAHFrmAhorro_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
End Class


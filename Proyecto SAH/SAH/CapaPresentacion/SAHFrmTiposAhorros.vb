﻿Public Class SAHFrmTiposAhorros

    Dim negocioTiposAhorros As New SAHNegocioTiposAhorros
    Dim negocioAhorros As New SAHNegocioAhorros
    Dim negocioTasasInteres As New SAHNegocioTasasInteres
    Dim fecha As Date
    Dim tipoAhorro As SAHTiposAhorros
    Dim frmPrincipal As SAHFrmPrincipal
    Dim n As Integer
    Dim idUsuario As Integer


    Public Sub New(frmPrincipalU As SAHFrmPrincipal, tipoAhorroU As SAHTiposAhorros, idUsuarioU As Integer)
        InitializeComponent()
        frmPrincipal = frmPrincipalU
        tipoAhorro = tipoAhorroU
        idUsuario = idUsuarioU
        txtNombre.Enabled = False
        dtpFechaEntrega.Enabled = False
        cbEstado.Enabled = False
        cbPersonalizado.Enabled = False
        btnGuardar.Visible = False
        btnCancelar.Visible = False

        If negocioAhorros.verificarUsuarioAdmin(idUsuario) Then
            btnActualizar.Visible = True
            btnAtras.Visible = True
        Else
            btnAtras.Visible = True
        End If

        btnTasasInteres.Visible = True
        cargarTipoAhorro()
        cbEstado.Visible = True
        lEstado.Visible = True
        n = 1
    End Sub


    Public Sub New(frmPrincipalU As SAHFrmPrincipal)
        InitializeComponent()
        frmPrincipal = frmPrincipalU
        CargarCodigoTipoAhorro()
        n = 0

    End Sub


    Sub cargarTipoAhorro()

        txtId.Text = tipoAhorro.IdTipoAhorro1
        txtNombre.Text = tipoAhorro.Nombre1

        If tipoAhorro.Personalizado1 = False Then
            dtpFechaEntrega.Value = tipoAhorro.FechaEntrega1

        Else
            cbPersonalizado.Checked = True
        End If

        If tipoAhorro.Estado1 = True Then
            cbEstado.SelectedIndex = 0

        Else
            cbEstado.SelectedIndex = 1
        End If



    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click

        If n = 0 Then

            Dim tipoAhorro As SAHTiposAhorros

            If (txtNombre.Text = "") Then
                MsgBox("Verifique que los campos esten completos.", MsgBoxStyle.Critical, "SAH")
            Else
                If cbPersonalizado.Checked Then
                    tipoAhorro = New SAHTiposAhorros(txtNombre.Text, 0, #06/04/2019#, 1)

                Else
                    fecha = dtpFechaEntrega.Value.Date
                    tipoAhorro = New SAHTiposAhorros(txtNombre.Text, 0, fecha, 0)
                End If

                If negocioTiposAhorros.insertarTipoAhorro(tipoAhorro) = 0 Then
                    insertarTasas(tipoAhorro.Personalizado1, negocioTiposAhorros.CodigoNuevoTiposAhorros() - 1)
                    Dim frmTasasInteres As New SAHFrmTasasInteres(negocioTiposAhorros.CodigoNuevoTiposAhorros() - 1, txtNombre.Text, frmPrincipal, 0, Me, idUsuario)
                    frmPrincipal.AbrirFormEnPanel(frmTasasInteres)


                End If


            End If

        Else

            If cbPersonalizado.Checked Then

                If cbEstado.SelectedItem = "Activo" Then


                    If negocioTiposAhorros.actualizarTipoAhorro(txtId.Text, txtNombre.Text, 1, #06/04/2019#, 1) = 0 Then

                        frmPrincipal.AbrirFormEnPanel(New SAHFrmConsultaTiposAhorros(frmPrincipal, idUsuario))


                    End If
                Else
                    If negocioTiposAhorros.actualizarTipoAhorro(txtId.Text, txtNombre.Text, 0, #06/04/2019#, 1) = 0 Then

                        frmPrincipal.AbrirFormEnPanel(New SAHFrmConsultaTiposAhorros(frmPrincipal, idUsuario))


                    End If
                End If

            Else
                If cbEstado.SelectedItem = "Activo" Then
                    fecha = dtpFechaEntrega.Value.Date

                    If negocioTiposAhorros.actualizarTipoAhorro(txtId.Text, txtNombre.Text, 1, fecha, 0) = 0 Then

                        frmPrincipal.AbrirFormEnPanel(New SAHFrmConsultaTiposAhorros(frmPrincipal, idUsuario))


                    End If


                Else
                    fecha = dtpFechaEntrega.Value.Date
                    If negocioTiposAhorros.actualizarTipoAhorro(txtId.Text, txtNombre.Text, 0, fecha, 0) = 0 Then

                        frmPrincipal.AbrirFormEnPanel(New SAHFrmConsultaTiposAhorros(frmPrincipal, idUsuario))


                    End If


                End If
            End If

        End If



    End Sub

    Sub insertarTasas(personalizado As Boolean, idTipoAhorro As Integer)

        Dim listaDias = {"90-119", "120-149", "150-179", "180-209", "210-239", "240-269", "270-299", "300-329"}
        Dim listaDiasP = {"90-119", "120-149", "150-179", "180-209", "210-239", "240-269", "270-359", "360-539", "540-719", "720-1079", "1080-1439", "1440-1799", "1800"}
        Dim listaMeses = {"3-4", "4-5", "5-6", "6-7", "7-8", "8-9", "9-10", "10-11"}
        Dim listaMesesP = {"3-4", "4-5", "5-6", "6-7", "7-8", "8-9", "9-12", "12-18", "18-24", "24-36", "36-48", "48-60", "60"}

        If personalizado = True Then

            For i = 0 To 12

                Dim tasa As New SAHTasasInteres(listaDiasP(i), listaMesesP(i), 0, idTipoAhorro)
                negocioTasasInteres.insertarTasaInteres(tasa)
            Next

        Else
            For i = 0 To 7
                Dim tasa As New SAHTasasInteres(listaDias(i), listaMeses(i), 0, idTipoAhorro)
                negocioTasasInteres.insertarTasaInteres(tasa)
            Next

        End If

    End Sub

    Private Sub limpiarForm()
        CargarCodigoTipoAhorro()
        lFecha.Visible = True
        dtpFechaEntrega.Visible = True
        txtNombre.Text = ""
        cbPersonalizado.Checked = False
        dtpFechaEntrega.Value = Now()
        txtNombre.Focus()
    End Sub


    Sub CargarCodigoTipoAhorro()
        txtId.Text = negocioTiposAhorros.CodigoNuevoTiposAhorros()
    End Sub

    Private Sub cbPersonalizado_CheckedChanged(sender As Object, e As EventArgs) Handles cbPersonalizado.CheckedChanged

        If cbPersonalizado.Checked = True Then
            lFecha.Visible = False
            dtpFechaEntrega.Visible = False
        Else
            lFecha.Visible = True
            dtpFechaEntrega.Visible = True

        End If

    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then

            If btnTasasInteres.Visible = True Then
                frmPrincipal.AbrirFormEnPanel(New SAHFrmConsultaTiposAhorros(frmPrincipal, idUsuario))
            Else
                frmPrincipal.AbrirFormEnPanel(New SAHFrmInicio)
            End If

        End If
    End Sub

    Private Sub btnAtras_Click(sender As Object, e As EventArgs) Handles btnAtras.Click
        frmPrincipal.AbrirFormEnPanel(New SAHFrmConsultaTiposAhorros(frmPrincipal, idUsuario))
    End Sub

    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        txtNombre.Enabled = True
        cbPersonalizado.Enabled = False
        dtpFechaEntrega.Enabled = True
        btnActualizar.Visible = False
        btnAtras.Visible = False
        btnGuardar.Visible = True
        btnCancelar.Visible = True


        If negocioTasasInteres.verificarPorcentajesTipoAhorro(tipoAhorro.IdTipoAhorro1) = False Then
            cbEstado.Enabled = False

        Else
            cbEstado.Enabled = True
        End If
    End Sub

    Private Sub FrmTiposAhorros_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnTasasInteres_Click(sender As Object, e As EventArgs) Handles btnTasasInteres.Click

        Dim frmTasasInteres As New SAHFrmTasasInteres(tipoAhorro, frmPrincipal, 1, Me)
        frmPrincipal.AbrirFormEnPanel(frmTasasInteres)


    End Sub

End Class
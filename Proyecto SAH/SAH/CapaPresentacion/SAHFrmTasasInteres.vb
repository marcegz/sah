﻿Public Class SAHFrmTasasInteres

    Dim idTipoAhorro As Integer
    Dim nombre As String
    Dim frmPrincipal As SAHFrmPrincipal
    Dim frmTiposAhorros As SAHFrmTiposAhorros
    Dim negocioTasasInteres As New SAHNegocioTasasInteres
    Dim negocioTiposAhorros As New SAHNegocioTiposAhorros
    Dim negocioAhorros As New SAHNegocioAhorros
    Dim tipo As Integer
    Dim tipoAhorro As SAHTiposAhorros
    Dim idUsuario As Integer


    Public Sub New(idTipoAhorroU As Integer, nombreU As String, frmPrincipalU As SAHFrmPrincipal, tipoU As Integer, frmTiposAhorrosU As SAHFrmTiposAhorros, idUsuarioU As Integer)
        InitializeComponent()
        idTipoAhorro = idTipoAhorroU
        frmPrincipal = frmPrincipalU
        frmTiposAhorros = frmTiposAhorrosU
        nombre = nombreU
        idUsuario = idUsuarioU
        tipo = tipoU
        txtNombre.Text = nombre
        cargarTasaInteres(idTipoAhorro)

        If tipo = 0 Then

            btnActualizar.Visible = False
            btnAtras.Visible = False

        Else


            dgvTasasInteres.Enabled = False

        End If

    End Sub


    Public Sub New(tipoAhorroU As SAHTiposAhorros, frmPrincipalU As SAHFrmPrincipal, tipoU As Integer, frmTiposAhorrosU As SAHFrmTiposAhorros)
        InitializeComponent()
        tipoAhorro = tipoAhorroU
        frmPrincipal = frmPrincipalU
        frmTiposAhorros = frmTiposAhorrosU
        tipo = tipoU
        txtNombre.Text = tipoAhorroU.Nombre1
        cargarTasaInteres(tipoAhorroU.IdTipoAhorro1)

        If negocioAhorros.verificarUsuarioAdmin(idUsuario) Then
            btnActualizar.Visible = True
            btnAtras.Visible = True
        Else
            btnActualizar.Visible = False
            btnCancelar.Visible = False
            btnGuardar.Visible = False
            btnAtras.Visible = True
        End If

    End Sub

    Sub cargarTasaInteres(idTipoAhorro)
        dgvTasasInteres.DataSource = negocioTasasInteres.getTablaConsultaTasasInteres(idTipoAhorro)
        modificaAtributosTabla()
        dgvTasasInteres.Columns(0).Visible = False
        dgvTasasInteres.Columns(1).ReadOnly = True
        dgvTasasInteres.Columns(2).ReadOnly = True
    End Sub

    Sub modificaAtributosTabla()
        dgvTasasInteres.Columns(0).HeaderText = "ID"
        dgvTasasInteres.Columns(1).HeaderText = "Días"
        dgvTasasInteres.Columns(2).HeaderText = "Meses"
        dgvTasasInteres.Columns(3).HeaderText = "Porcentaje"
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click

        If tipo = 0 Then

            If MsgBox("¿Está seguro que desea cancelar?  Si es así el tipo de ahorro quedará inactivo.", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then

                frmPrincipal.AbrirFormEnPanel(New SAHFrmTiposAhorros(frmPrincipal))

            End If
        Else
            frmPrincipal.AbrirFormEnPanel(frmTiposAhorros)
        End If

    End Sub

    Private Sub FrmTasasInteres_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        If tipo = 0 Then

            MsgBox("Por favor ingrese el porcentaje de las tasas de interés al tipo de ahorro registrado previamente.", MsgBoxStyle.Information, "SAH")

        End If


    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim idTasaInteres As Integer
        Dim porcentaje As Double

        If tipo = 0 Then

            If verificarPorcentajes() Then


                For Each oRow As DataGridViewRow In dgvTasasInteres.Rows
                    idTasaInteres = oRow.Cells(0).Value
                    porcentaje = oRow.Cells(3).Value

                    negocioTasasInteres.actualizarTasaInteres(idTasaInteres, porcentaje)
                Next

                MsgBox("Tasas de interés registradas correctamente.", MsgBoxStyle.Information, "SAH")
                negocioTiposAhorros.actualizarEstadoTipoAhorro(idTipoAhorro, True)

                frmPrincipal.AbrirFormEnPanel(New SAHFrmTiposAhorros(frmPrincipal))

            Else
                MsgBox("Por favor ingrese todos los porcentajes.", MsgBoxStyle.Information, "SAH")
            End If

        Else

            If verificarPorcentajes() Then

                For Each oRow As DataGridViewRow In dgvTasasInteres.Rows
                    idTasaInteres = oRow.Cells(0).Value
                    porcentaje = oRow.Cells(3).Value

                    negocioTasasInteres.actualizarTasaInteres(idTasaInteres, porcentaje)
                Next
                negocioTiposAhorros.actualizarEstadoTipoAhorro(tipoAhorro.IdTipoAhorro1, True)
                MsgBox("Tasas de interés actualizadas correctamente.", MsgBoxStyle.Information, "SAH")
                frmPrincipal.AbrirFormEnPanel(New SAHFrmTiposAhorros(frmPrincipal, negocioTiposAhorros.getTipoAhorro(tipoAhorro.IdTipoAhorro1), idUsuario))

            Else
                MsgBox("Por favor ingrese todos los porcentajes.", MsgBoxStyle.Information, "SAH")
            End If

        End If

    End Sub


    Function verificarPorcentajes() As Boolean
        Dim porcentaje As Double
        Dim salida As Boolean = True

        For Each oRow As DataGridViewRow In dgvTasasInteres.Rows

            porcentaje = oRow.Cells(3).Value

            If porcentaje = 0.0 Then


                salida = False
                Exit For

            End If
        Next

        Return salida

    End Function

    Private Sub btnActualizar_Click(sender As Object, e As EventArgs) Handles btnActualizar.Click
        dgvTasasInteres.Enabled = True
        btnActualizar.Visible = False
        btnAtras.Visible = False
        btnGuardar.Visible = True
        btnCancelar.Visible = True
    End Sub

    Private Sub btnAtras_Click(sender As Object, e As EventArgs) Handles btnAtras.Click

        frmPrincipal.AbrirFormEnPanel(frmTiposAhorros)

    End Sub

End Class
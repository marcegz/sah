﻿Public Class SAHFrmDescuentosCastigos

    Dim frmPrincipal As SAHFrmPrincipal
    Dim negocioDescuentosCastigos As New SAHNegocioDescuentosCastigos

    Private Sub SAHFrmDescuentosCastigos_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarDatos()
    End Sub

    Public Sub New(frmPrincipalU As SAHFrmPrincipal)
        InitializeComponent()
        frmPrincipal = frmPrincipalU
    End Sub

    Sub cargarDatos()
        dgvDescuentosCastigos.DataSource = negocioDescuentosCastigos.getTablaConsultaDescuentosCastigos()
        modificaAtributosTabla()
        dgvDescuentosCastigos.Columns(3).Visible = False
        dgvDescuentosCastigos.Columns(5).Visible = False
        dgvDescuentosCastigos.Columns(0).ReadOnly = True
        dgvDescuentosCastigos.Columns(1).ReadOnly = True
        dgvDescuentosCastigos.Columns(2).ReadOnly = True
        dgvDescuentosCastigos.Columns(0).ReadOnly = True
    End Sub

    Sub modificaAtributosTabla()
        dgvDescuentosCastigos.Columns(0).HeaderText = "ID"
        dgvDescuentosCastigos.Columns(1).HeaderText = "Tipo"
        dgvDescuentosCastigos.Columns(2).HeaderText = "Meses"
        dgvDescuentosCastigos.Columns(4).HeaderText = "Monto"
    End Sub



    Private Sub btnEditar_Click(sender As Object, e As EventArgs) Handles btnEditar.Click

        dgvDescuentosCastigos.Columns(4).ReadOnly = False
        btnCancelar.Visible = True
        btnNuevo.Visible = True
        btnEditar.Visible = False
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click

        If MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then

            frmPrincipal.AbrirFormEnPanel(New SAHFrmInicio)

        End If

    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click

        Dim id As Integer
        Dim monto As Integer

        If verificarPorcentajes() Then


            For Each oRow As DataGridViewRow In dgvDescuentosCastigos.Rows
                id = oRow.Cells(0).Value
                monto = oRow.Cells(4).Value

                negocioDescuentosCastigos.actualizarTasaInteres(id, monto)
            Next

            MsgBox("Descuentos y castigos actualizados correctamente.", MsgBoxStyle.Information, "SAH")

            dgvDescuentosCastigos.Columns(4).ReadOnly = True
            btnCancelar.Visible = False
            btnNuevo.Visible = False
            btnEditar.Visible = True

        Else
            MsgBox("Por favor ingrese todos los montos.", MsgBoxStyle.Information, "SAH")
        End If

    End Sub

    Function verificarPorcentajes() As Boolean
        Dim porcentaje As Double
        Dim salida As Boolean = True

        For Each oRow As DataGridViewRow In dgvDescuentosCastigos.Rows

            porcentaje = oRow.Cells(4).Value

            If porcentaje = 0 Then


                salida = False
                Exit For

            End If
        Next

        Return salida

    End Function


End Class
﻿Public Class SAHFrmConsultaAhorros

    Dim f As New SAHFrmPrincipal
    Dim negocioAhorros As New SAHNegocioAhorros

    Public Sub New(frmPrincipal As SAHFrmPrincipal)
        InitializeComponent()
        f = frmPrincipal
    End Sub

    Private Sub FrmConsultaAhorros_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarTablaAhorros()
        Dim btnclm As DataGridViewButtonColumn = New DataGridViewButtonColumn()
        btnclm.Name = "Consultar"
        btnclm.FlatStyle = FlatStyle.Flat
        dgvAhorros.Columns.Add(btnclm)
        modificaAtributosTabla()
    End Sub


    Private Sub dgvAhorros_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvAhorros.CellContentClick

        If e.ColumnIndex = 0 Then
            Dim rows As DataGridViewRow
            rows = dgvAhorros.Rows(e.RowIndex)

            Dim ahorro As SAHAhorros
            ahorro = negocioAhorros.getAhorro(rows.Cells(1).Value)

            Dim fAhorro As New SAHFrmAhorro(f, ahorro)
            f.AbrirFormEnPanel(fAhorro)

        End If

    End Sub

    Sub cargarTablaAhorros()
        dgvAhorros.DataSource = negocioAhorros.getTablaConsultaAhorros(txtConsultar.Text)
    End Sub

    Sub modificaAtributosTabla()
        dgvAhorros.Columns(0).HeaderText = "ID Ahorro"
        dgvAhorros.Columns(1).HeaderText = "Nombre Ahorro"
        dgvAhorros.Columns(2).HeaderText = "Tipo Ahorro"
        dgvAhorros.Columns(3).HeaderText = "Estado"
        dgvAhorros.Columns(4).HeaderText = "Monto Ahorrado"
        dgvAhorros.Columns(5).HeaderText = "Cédula"
        dgvAhorros.Columns(6).HeaderText = "Nombre"
        dgvAhorros.Columns(7).HeaderText = "Apellido Uno"
        dgvAhorros.Columns(8).HeaderText = "Apellido Dos"
        dgvAhorros.Columns(9).HeaderText = "Consultar"
    End Sub

    Private Sub txtConsultar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtConsultar.KeyUp
        cargarTablaAhorros()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click

        Dim reporte As New SAHReporteAhorros(txtConsultar.Text)
        reporte.Show()


    End Sub

    Private Sub dgvAhorros_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvAhorros.CellPainting
        If e.ColumnIndex >= 0 AndAlso Me.dgvAhorros.Columns(e.ColumnIndex).Name = "Consultar" AndAlso e.RowIndex >= 0 Then

            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = TryCast(Me.dgvAhorros.Rows(e.RowIndex).Cells("Consultar"), DataGridViewButtonCell)
            'Dim icoAtomico As Icon = New Icon("C:\Users\Marce\source\repos\project\Proyecto SAH\SAH\Resources\consultarICON.ico")
            Dim icoAtomico As Icon = New Icon("C:\Users\Marce\source\repos\project\Proyecto SAH\SAH\Resources\consultarICON.ico")
            e.Graphics.DrawIcon(icoAtomico, e.CellBounds.Left + 17, e.CellBounds.Top + 4)
            Me.dgvAhorros.Rows(e.RowIndex).Height = icoAtomico.Height + 7
            Me.dgvAhorros.Columns(e.ColumnIndex).Width = icoAtomico.Width + 35


            e.Handled = True
        End If
    End Sub



End Class
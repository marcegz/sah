﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SAHFrmPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SAHFrmPrincipal))
        Me.panelMenu = New System.Windows.Forms.Panel()
        Me.btnTasasInteres = New System.Windows.Forms.Button()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.btnReportes = New System.Windows.Forms.Button()
        Me.btnMenu = New System.Windows.Forms.PictureBox()
        Me.btnCertificado = New System.Windows.Forms.Button()
        Me.pictureBox5 = New System.Windows.Forms.PictureBox()
        Me.btnCupones = New System.Windows.Forms.Button()
        Me.pictureBox6 = New System.Windows.Forms.PictureBox()
        Me.btnBloqueos = New System.Windows.Forms.Button()
        Me.pictureBox3 = New System.Windows.Forms.PictureBox()
        Me.btnDescuentosCastigos = New System.Windows.Forms.Button()
        Me.lbFecha = New System.Windows.Forms.Label()
        Me.pictureBox4 = New System.Windows.Forms.PictureBox()
        Me.lblHora = New System.Windows.Forms.Label()
        Me.pictureBox2 = New System.Windows.Forms.PictureBox()
        Me.btnAhorros = New System.Windows.Forms.Button()
        Me.pictureBox1 = New System.Windows.Forms.PictureBox()
        Me.btnInicio = New System.Windows.Forms.Button()
        Me.tmMostrarMenu = New System.Windows.Forms.Timer(Me.components)
        Me.tmOcultarMenu = New System.Windows.Forms.Timer(Me.components)
        Me.tmFechaHora = New System.Windows.Forms.Timer(Me.components)
        Me.panelContenedor = New System.Windows.Forms.Panel()
        Me.label1 = New System.Windows.Forms.Label()
        Me.BarraTitulo = New System.Windows.Forms.Panel()
        Me.btnNormal = New System.Windows.Forms.Button()
        Me.btnMax = New System.Windows.Forms.Button()
        Me.btnMinimizar = New System.Windows.Forms.Button()
        Me.btnCerrar = New System.Windows.Forms.Button()
        Me.pictureBox8 = New System.Windows.Forms.PictureBox()
        Me.submenuAhorro = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NuevoAhorroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarAhorrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeAhorroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarTiposDeAhorroToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.submenuCertificados = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.NuevoCertificadoToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarCertificados = New System.Windows.Forms.ToolStripMenuItem()
        Me.TiposDeCertificadosToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConsultarTiposDeCertificadosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.submenuDescuentosCastigos = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.DescuentosCastigos = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescuentosCastigosAhorros = New System.Windows.Forms.ToolStripMenuItem()
        Me.panelMenu.SuspendLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.btnMenu, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.BarraTitulo.SuspendLayout()
        CType(Me.pictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.submenuAhorro.SuspendLayout()
        Me.submenuCertificados.SuspendLayout()
        Me.submenuDescuentosCastigos.SuspendLayout()
        Me.SuspendLayout()
        '
        'panelMenu
        '
        Me.panelMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.panelMenu.Controls.Add(Me.btnTasasInteres)
        Me.panelMenu.Controls.Add(Me.PictureBox9)
        Me.panelMenu.Controls.Add(Me.PictureBox7)
        Me.panelMenu.Controls.Add(Me.btnReportes)
        Me.panelMenu.Controls.Add(Me.btnMenu)
        Me.panelMenu.Controls.Add(Me.btnCertificado)
        Me.panelMenu.Controls.Add(Me.pictureBox5)
        Me.panelMenu.Controls.Add(Me.btnCupones)
        Me.panelMenu.Controls.Add(Me.pictureBox6)
        Me.panelMenu.Controls.Add(Me.btnBloqueos)
        Me.panelMenu.Controls.Add(Me.pictureBox3)
        Me.panelMenu.Controls.Add(Me.btnDescuentosCastigos)
        Me.panelMenu.Controls.Add(Me.lbFecha)
        Me.panelMenu.Controls.Add(Me.pictureBox4)
        Me.panelMenu.Controls.Add(Me.lblHora)
        Me.panelMenu.Controls.Add(Me.pictureBox2)
        Me.panelMenu.Controls.Add(Me.btnAhorros)
        Me.panelMenu.Controls.Add(Me.pictureBox1)
        Me.panelMenu.Controls.Add(Me.btnInicio)
        Me.panelMenu.Dock = System.Windows.Forms.DockStyle.Left
        Me.panelMenu.Location = New System.Drawing.Point(0, 43)
        Me.panelMenu.Name = "panelMenu"
        Me.panelMenu.Size = New System.Drawing.Size(230, 583)
        Me.panelMenu.TabIndex = 3
        '
        'btnTasasInteres
        '
        Me.btnTasasInteres.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnTasasInteres.FlatAppearance.BorderSize = 0
        Me.btnTasasInteres.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnTasasInteres.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.btnTasasInteres.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnTasasInteres.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.btnTasasInteres.ForeColor = System.Drawing.Color.Silver
        Me.btnTasasInteres.Image = CType(resources.GetObject("btnTasasInteres.Image"), System.Drawing.Image)
        Me.btnTasasInteres.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnTasasInteres.Location = New System.Drawing.Point(3, 356)
        Me.btnTasasInteres.Name = "btnTasasInteres"
        Me.btnTasasInteres.Size = New System.Drawing.Size(230, 40)
        Me.btnTasasInteres.TabIndex = 4
        Me.btnTasasInteres.Text = "Tasas Interés"
        Me.btnTasasInteres.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnTasasInteres.UseVisualStyleBackColor = True
        '
        'PictureBox9
        '
        Me.PictureBox9.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.PictureBox9.Location = New System.Drawing.Point(1, 402)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(2, 40)
        Me.PictureBox9.TabIndex = 2
        Me.PictureBox9.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.PictureBox7.Location = New System.Drawing.Point(1, 356)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(2, 40)
        Me.PictureBox7.TabIndex = 29
        Me.PictureBox7.TabStop = False
        '
        'btnReportes
        '
        Me.btnReportes.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnReportes.FlatAppearance.BorderSize = 0
        Me.btnReportes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnReportes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.btnReportes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReportes.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.btnReportes.ForeColor = System.Drawing.Color.Silver
        Me.btnReportes.Image = Global.SAH.My.Resources.Resources.reporte
        Me.btnReportes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReportes.Location = New System.Drawing.Point(0, 402)
        Me.btnReportes.Name = "btnReportes"
        Me.btnReportes.Size = New System.Drawing.Size(230, 40)
        Me.btnReportes.TabIndex = 12
        Me.btnReportes.Text = "Reportes"
        Me.btnReportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnReportes.UseVisualStyleBackColor = True
        '
        'btnMenu
        '
        Me.btnMenu.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMenu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMenu.Image = Global.SAH.My.Resources.Resources.menu2
        Me.btnMenu.Location = New System.Drawing.Point(182, 6)
        Me.btnMenu.Name = "btnMenu"
        Me.btnMenu.Size = New System.Drawing.Size(43, 37)
        Me.btnMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.btnMenu.TabIndex = 14
        Me.btnMenu.TabStop = False
        '
        'btnCertificado
        '
        Me.btnCertificado.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnCertificado.FlatAppearance.BorderSize = 0
        Me.btnCertificado.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnCertificado.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.btnCertificado.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCertificado.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.btnCertificado.ForeColor = System.Drawing.Color.Silver
        Me.btnCertificado.Image = Global.SAH.My.Resources.Resources.Mcertificado
        Me.btnCertificado.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCertificado.Location = New System.Drawing.Point(0, 172)
        Me.btnCertificado.Name = "btnCertificado"
        Me.btnCertificado.Size = New System.Drawing.Size(230, 40)
        Me.btnCertificado.TabIndex = 11
        Me.btnCertificado.Text = "Certificados"
        Me.btnCertificado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCertificado.UseVisualStyleBackColor = True
        '
        'pictureBox5
        '
        Me.pictureBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.pictureBox5.Location = New System.Drawing.Point(0, 310)
        Me.pictureBox5.Name = "pictureBox5"
        Me.pictureBox5.Size = New System.Drawing.Size(2, 40)
        Me.pictureBox5.TabIndex = 11
        Me.pictureBox5.TabStop = False
        '
        'btnCupones
        '
        Me.btnCupones.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnCupones.FlatAppearance.BorderSize = 0
        Me.btnCupones.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnCupones.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.btnCupones.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCupones.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.btnCupones.ForeColor = System.Drawing.Color.Silver
        Me.btnCupones.Image = CType(resources.GetObject("btnCupones.Image"), System.Drawing.Image)
        Me.btnCupones.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCupones.Location = New System.Drawing.Point(3, 218)
        Me.btnCupones.Name = "btnCupones"
        Me.btnCupones.Size = New System.Drawing.Size(230, 40)
        Me.btnCupones.TabIndex = 10
        Me.btnCupones.Text = "Cupones"
        Me.btnCupones.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCupones.UseVisualStyleBackColor = True
        '
        'pictureBox6
        '
        Me.pictureBox6.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.pictureBox6.Location = New System.Drawing.Point(0, 264)
        Me.pictureBox6.Name = "pictureBox6"
        Me.pictureBox6.Size = New System.Drawing.Size(2, 40)
        Me.pictureBox6.TabIndex = 9
        Me.pictureBox6.TabStop = False
        '
        'btnBloqueos
        '
        Me.btnBloqueos.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnBloqueos.FlatAppearance.BorderSize = 0
        Me.btnBloqueos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnBloqueos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.btnBloqueos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBloqueos.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.btnBloqueos.ForeColor = System.Drawing.Color.Silver
        Me.btnBloqueos.Image = Global.SAH.My.Resources.Resources.Bloqueos
        Me.btnBloqueos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnBloqueos.Location = New System.Drawing.Point(0, 264)
        Me.btnBloqueos.Name = "btnBloqueos"
        Me.btnBloqueos.Size = New System.Drawing.Size(230, 40)
        Me.btnBloqueos.TabIndex = 8
        Me.btnBloqueos.Text = "Bloqueos"
        Me.btnBloqueos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnBloqueos.UseVisualStyleBackColor = True
        '
        'pictureBox3
        '
        Me.pictureBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.pictureBox3.Location = New System.Drawing.Point(0, 218)
        Me.pictureBox3.Name = "pictureBox3"
        Me.pictureBox3.Size = New System.Drawing.Size(2, 40)
        Me.pictureBox3.TabIndex = 7
        Me.pictureBox3.TabStop = False
        '
        'btnDescuentosCastigos
        '
        Me.btnDescuentosCastigos.ContextMenuStrip = Me.submenuDescuentosCastigos
        Me.btnDescuentosCastigos.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnDescuentosCastigos.FlatAppearance.BorderSize = 0
        Me.btnDescuentosCastigos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnDescuentosCastigos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.btnDescuentosCastigos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDescuentosCastigos.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.btnDescuentosCastigos.ForeColor = System.Drawing.Color.Silver
        Me.btnDescuentosCastigos.Image = CType(resources.GetObject("btnDescuentosCastigos.Image"), System.Drawing.Image)
        Me.btnDescuentosCastigos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnDescuentosCastigos.Location = New System.Drawing.Point(3, 310)
        Me.btnDescuentosCastigos.Name = "btnDescuentosCastigos"
        Me.btnDescuentosCastigos.Size = New System.Drawing.Size(230, 40)
        Me.btnDescuentosCastigos.TabIndex = 6
        Me.btnDescuentosCastigos.Text = "Descuentos / Castigos"
        Me.btnDescuentosCastigos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnDescuentosCastigos.UseVisualStyleBackColor = True
        '
        'lbFecha
        '
        Me.lbFecha.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbFecha.AutoSize = True
        Me.lbFecha.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!)
        Me.lbFecha.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lbFecha.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lbFecha.Location = New System.Drawing.Point(73, 528)
        Me.lbFecha.Name = "lbFecha"
        Me.lbFecha.Size = New System.Drawing.Size(80, 20)
        Me.lbFecha.TabIndex = 28
        Me.lbFecha.Text = "24/5/2019"
        Me.lbFecha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pictureBox4
        '
        Me.pictureBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.pictureBox4.Location = New System.Drawing.Point(0, 172)
        Me.pictureBox4.Name = "pictureBox4"
        Me.pictureBox4.Size = New System.Drawing.Size(2, 40)
        Me.pictureBox4.TabIndex = 5
        Me.pictureBox4.TabStop = False
        '
        'lblHora
        '
        Me.lblHora.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblHora.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.25!)
        Me.lblHora.ForeColor = System.Drawing.SystemColors.ControlLightLight
        Me.lblHora.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblHora.Location = New System.Drawing.Point(7, 470)
        Me.lblHora.Margin = New System.Windows.Forms.Padding(0)
        Me.lblHora.Name = "lblHora"
        Me.lblHora.Size = New System.Drawing.Size(218, 47)
        Me.lblHora.TabIndex = 27
        Me.lblHora.Text = "21:49:45"
        Me.lblHora.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pictureBox2
        '
        Me.pictureBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.pictureBox2.Location = New System.Drawing.Point(0, 126)
        Me.pictureBox2.Name = "pictureBox2"
        Me.pictureBox2.Size = New System.Drawing.Size(2, 40)
        Me.pictureBox2.TabIndex = 3
        Me.pictureBox2.TabStop = False
        '
        'btnAhorros
        '
        Me.btnAhorros.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnAhorros.FlatAppearance.BorderSize = 0
        Me.btnAhorros.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnAhorros.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.btnAhorros.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAhorros.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.btnAhorros.ForeColor = System.Drawing.Color.Silver
        Me.btnAhorros.Image = Global.SAH.My.Resources.Resources.Ahorros
        Me.btnAhorros.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAhorros.Location = New System.Drawing.Point(0, 126)
        Me.btnAhorros.Name = "btnAhorros"
        Me.btnAhorros.Size = New System.Drawing.Size(230, 40)
        Me.btnAhorros.TabIndex = 2
        Me.btnAhorros.Text = "Ahorros"
        Me.btnAhorros.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAhorros.UseVisualStyleBackColor = True
        '
        'pictureBox1
        '
        Me.pictureBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.pictureBox1.Location = New System.Drawing.Point(0, 80)
        Me.pictureBox1.Name = "pictureBox1"
        Me.pictureBox1.Size = New System.Drawing.Size(2, 40)
        Me.pictureBox1.TabIndex = 1
        Me.pictureBox1.TabStop = False
        '
        'btnInicio
        '
        Me.btnInicio.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnInicio.FlatAppearance.BorderSize = 0
        Me.btnInicio.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnInicio.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.btnInicio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInicio.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.btnInicio.ForeColor = System.Drawing.Color.Silver
        Me.btnInicio.Image = Global.SAH.My.Resources.Resources.inicio
        Me.btnInicio.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnInicio.Location = New System.Drawing.Point(0, 80)
        Me.btnInicio.Name = "btnInicio"
        Me.btnInicio.Size = New System.Drawing.Size(230, 40)
        Me.btnInicio.TabIndex = 0
        Me.btnInicio.Text = "Inicio"
        Me.btnInicio.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnInicio.UseVisualStyleBackColor = True
        '
        'tmMostrarMenu
        '
        '
        'tmOcultarMenu
        '
        '
        'tmFechaHora
        '
        Me.tmFechaHora.Enabled = True
        '
        'panelContenedor
        '
        Me.panelContenedor.AutoScrollMargin = New System.Drawing.Size(0, 10)
        Me.panelContenedor.AutoSize = True
        Me.panelContenedor.BackColor = System.Drawing.Color.White
        Me.panelContenedor.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panelContenedor.Location = New System.Drawing.Point(230, 43)
        Me.panelContenedor.Name = "panelContenedor"
        Me.panelContenedor.Size = New System.Drawing.Size(885, 583)
        Me.panelContenedor.TabIndex = 11
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Arial", 12.0!)
        Me.label1.ForeColor = System.Drawing.Color.White
        Me.label1.Location = New System.Drawing.Point(44, 15)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(282, 18)
        Me.label1.TabIndex = 4
        Me.label1.Text = "SISTEMA DE AHORRO Y CAPTACIÓN"
        '
        'BarraTitulo
        '
        Me.BarraTitulo.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.BarraTitulo.Controls.Add(Me.btnNormal)
        Me.BarraTitulo.Controls.Add(Me.btnMax)
        Me.BarraTitulo.Controls.Add(Me.btnMinimizar)
        Me.BarraTitulo.Controls.Add(Me.btnCerrar)
        Me.BarraTitulo.Controls.Add(Me.pictureBox8)
        Me.BarraTitulo.Controls.Add(Me.label1)
        Me.BarraTitulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.BarraTitulo.Location = New System.Drawing.Point(0, 0)
        Me.BarraTitulo.Name = "BarraTitulo"
        Me.BarraTitulo.Size = New System.Drawing.Size(1115, 43)
        Me.BarraTitulo.TabIndex = 2
        '
        'btnNormal
        '
        Me.btnNormal.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNormal.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnNormal.FlatAppearance.BorderSize = 0
        Me.btnNormal.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNormal.Image = CType(resources.GetObject("btnNormal.Image"), System.Drawing.Image)
        Me.btnNormal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnNormal.Location = New System.Drawing.Point(1010, 0)
        Me.btnNormal.Name = "btnNormal"
        Me.btnNormal.Size = New System.Drawing.Size(43, 43)
        Me.btnNormal.TabIndex = 8
        Me.btnNormal.UseVisualStyleBackColor = True
        Me.btnNormal.Visible = False
        '
        'btnMax
        '
        Me.btnMax.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMax.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMax.FlatAppearance.BorderSize = 0
        Me.btnMax.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMax.Image = CType(resources.GetObject("btnMax.Image"), System.Drawing.Image)
        Me.btnMax.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnMax.Location = New System.Drawing.Point(1010, 0)
        Me.btnMax.Name = "btnMax"
        Me.btnMax.Size = New System.Drawing.Size(43, 43)
        Me.btnMax.TabIndex = 9
        Me.btnMax.UseVisualStyleBackColor = True
        '
        'btnMinimizar
        '
        Me.btnMinimizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMinimizar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnMinimizar.FlatAppearance.BorderSize = 0
        Me.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMinimizar.Image = CType(resources.GetObject("btnMinimizar.Image"), System.Drawing.Image)
        Me.btnMinimizar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnMinimizar.Location = New System.Drawing.Point(966, 0)
        Me.btnMinimizar.Name = "btnMinimizar"
        Me.btnMinimizar.Size = New System.Drawing.Size(43, 43)
        Me.btnMinimizar.TabIndex = 7
        Me.btnMinimizar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCerrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnCerrar.FlatAppearance.BorderSize = 0
        Me.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCerrar.Image = CType(resources.GetObject("btnCerrar.Image"), System.Drawing.Image)
        Me.btnCerrar.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnCerrar.Location = New System.Drawing.Point(1059, 2)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(39, 39)
        Me.btnCerrar.TabIndex = 6
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'pictureBox8
        '
        Me.pictureBox8.BackgroundImage = Global.SAH.My.Resources.Resources.LogoSistema
        Me.pictureBox8.Location = New System.Drawing.Point(10, 4)
        Me.pictureBox8.Name = "pictureBox8"
        Me.pictureBox8.Size = New System.Drawing.Size(35, 34)
        Me.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pictureBox8.TabIndex = 5
        Me.pictureBox8.TabStop = False
        '
        'submenuAhorro
        '
        Me.submenuAhorro.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.submenuAhorro.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.submenuAhorro.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.submenuAhorro.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoAhorroToolStripMenuItem, Me.ConsultarAhorrosToolStripMenuItem, Me.TiposDeAhorroToolStripMenuItem, Me.ConsultarTiposDeAhorroToolStripMenuItem})
        Me.submenuAhorro.Name = "ContextMenuStrip1"
        Me.submenuAhorro.ShowCheckMargin = True
        Me.submenuAhorro.Size = New System.Drawing.Size(288, 156)
        '
        'NuevoAhorroToolStripMenuItem
        '
        Me.NuevoAhorroToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.NuevoAhorroToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.NuevoAhorroToolStripMenuItem.Image = Global.SAH.My.Resources.Resources.nuevoAhorro
        Me.NuevoAhorroToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.NuevoAhorroToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.NuevoAhorroToolStripMenuItem.Name = "NuevoAhorroToolStripMenuItem"
        Me.NuevoAhorroToolStripMenuItem.Size = New System.Drawing.Size(287, 38)
        Me.NuevoAhorroToolStripMenuItem.Text = "Nuevo Ahorro"
        '
        'ConsultarAhorrosToolStripMenuItem
        '
        Me.ConsultarAhorrosToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.ConsultarAhorrosToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.ConsultarAhorrosToolStripMenuItem.Image = CType(resources.GetObject("ConsultarAhorrosToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ConsultarAhorrosToolStripMenuItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ConsultarAhorrosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ConsultarAhorrosToolStripMenuItem.Name = "ConsultarAhorrosToolStripMenuItem"
        Me.ConsultarAhorrosToolStripMenuItem.Size = New System.Drawing.Size(287, 38)
        Me.ConsultarAhorrosToolStripMenuItem.Text = "Consultar Ahorros"
        '
        'TiposDeAhorroToolStripMenuItem
        '
        Me.TiposDeAhorroToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.TiposDeAhorroToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.TiposDeAhorroToolStripMenuItem.Image = Global.SAH.My.Resources.Resources.lista
        Me.TiposDeAhorroToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.TiposDeAhorroToolStripMenuItem.Name = "TiposDeAhorroToolStripMenuItem"
        Me.TiposDeAhorroToolStripMenuItem.Size = New System.Drawing.Size(287, 38)
        Me.TiposDeAhorroToolStripMenuItem.Text = "Tipos de Ahorro"
        '
        'ConsultarTiposDeAhorroToolStripMenuItem
        '
        Me.ConsultarTiposDeAhorroToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.ConsultarTiposDeAhorroToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.ConsultarTiposDeAhorroToolStripMenuItem.Image = CType(resources.GetObject("ConsultarTiposDeAhorroToolStripMenuItem.Image"), System.Drawing.Image)
        Me.ConsultarTiposDeAhorroToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ConsultarTiposDeAhorroToolStripMenuItem.Name = "ConsultarTiposDeAhorroToolStripMenuItem"
        Me.ConsultarTiposDeAhorroToolStripMenuItem.Size = New System.Drawing.Size(287, 38)
        Me.ConsultarTiposDeAhorroToolStripMenuItem.Text = "Consultar Tipos de Ahorros"
        '
        'submenuCertificados
        '
        Me.submenuCertificados.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.submenuCertificados.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.submenuCertificados.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.submenuCertificados.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NuevoCertificadoToolStripMenuItem1, Me.ConsultarCertificados, Me.TiposDeCertificadosToolStripMenuItem2, Me.ConsultarTiposDeCertificadosToolStripMenuItem})
        Me.submenuCertificados.Name = "ContextMenuStrip1"
        Me.submenuCertificados.ShowCheckMargin = True
        Me.submenuCertificados.Size = New System.Drawing.Size(312, 156)
        '
        'NuevoCertificadoToolStripMenuItem1
        '
        Me.NuevoCertificadoToolStripMenuItem1.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.NuevoCertificadoToolStripMenuItem1.ForeColor = System.Drawing.Color.Silver
        Me.NuevoCertificadoToolStripMenuItem1.Image = Global.SAH.My.Resources.Resources.nuevoCertificado
        Me.NuevoCertificadoToolStripMenuItem1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.NuevoCertificadoToolStripMenuItem1.Name = "NuevoCertificadoToolStripMenuItem1"
        Me.NuevoCertificadoToolStripMenuItem1.Size = New System.Drawing.Size(311, 38)
        Me.NuevoCertificadoToolStripMenuItem1.Text = "Nuevo Certificado"
        '
        'ConsultarCertificados
        '
        Me.ConsultarCertificados.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.ConsultarCertificados.ForeColor = System.Drawing.Color.Silver
        Me.ConsultarCertificados.Image = Global.SAH.My.Resources.Resources.consultar
        Me.ConsultarCertificados.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ConsultarCertificados.Name = "ConsultarCertificados"
        Me.ConsultarCertificados.Size = New System.Drawing.Size(311, 38)
        Me.ConsultarCertificados.Text = "Consultar  Certificados"
        '
        'TiposDeCertificadosToolStripMenuItem2
        '
        Me.TiposDeCertificadosToolStripMenuItem2.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.TiposDeCertificadosToolStripMenuItem2.ForeColor = System.Drawing.Color.Silver
        Me.TiposDeCertificadosToolStripMenuItem2.Image = Global.SAH.My.Resources.Resources.lista
        Me.TiposDeCertificadosToolStripMenuItem2.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.TiposDeCertificadosToolStripMenuItem2.Name = "TiposDeCertificadosToolStripMenuItem2"
        Me.TiposDeCertificadosToolStripMenuItem2.Size = New System.Drawing.Size(311, 38)
        Me.TiposDeCertificadosToolStripMenuItem2.Text = "Tipos de Certificados"
        '
        'ConsultarTiposDeCertificadosToolStripMenuItem
        '
        Me.ConsultarTiposDeCertificadosToolStripMenuItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.ConsultarTiposDeCertificadosToolStripMenuItem.ForeColor = System.Drawing.Color.Silver
        Me.ConsultarTiposDeCertificadosToolStripMenuItem.Image = Global.SAH.My.Resources.Resources.consultar
        Me.ConsultarTiposDeCertificadosToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.ConsultarTiposDeCertificadosToolStripMenuItem.Name = "ConsultarTiposDeCertificadosToolStripMenuItem"
        Me.ConsultarTiposDeCertificadosToolStripMenuItem.Size = New System.Drawing.Size(311, 38)
        Me.ConsultarTiposDeCertificadosToolStripMenuItem.Text = "Consultar Tipos de Certificados"
        '
        'submenuDescuentosCastigos
        '
        Me.submenuDescuentosCastigos.BackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.submenuDescuentosCastigos.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.submenuDescuentosCastigos.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.submenuDescuentosCastigos.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DescuentosCastigos, Me.DescuentosCastigosAhorros})
        Me.submenuDescuentosCastigos.Name = "ContextMenuStrip1"
        Me.submenuDescuentosCastigos.ShowCheckMargin = True
        Me.submenuDescuentosCastigos.Size = New System.Drawing.Size(318, 102)
        '
        'DescuentosCastigos
        '
        Me.DescuentosCastigos.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.DescuentosCastigos.ForeColor = System.Drawing.Color.Silver
        Me.DescuentosCastigos.Image = Global.SAH.My.Resources.Resources.descuentos_castigos
        Me.DescuentosCastigos.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.DescuentosCastigos.Name = "DescuentosCastigos"
        Me.DescuentosCastigos.Size = New System.Drawing.Size(317, 38)
        Me.DescuentosCastigos.Text = "Descuentos / Castigos"
        '
        'DescuentosCastigosAhorros
        '
        Me.DescuentosCastigosAhorros.BackColor = System.Drawing.Color.FromArgb(CType(CType(29, Byte), Integer), CType(CType(34, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.DescuentosCastigosAhorros.ForeColor = System.Drawing.Color.Silver
        Me.DescuentosCastigosAhorros.Image = Global.SAH.My.Resources.Resources.consultar
        Me.DescuentosCastigosAhorros.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None
        Me.DescuentosCastigosAhorros.Name = "DescuentosCastigosAhorros"
        Me.DescuentosCastigosAhorros.Size = New System.Drawing.Size(317, 38)
        Me.DescuentosCastigosAhorros.Text = "Descuentos / Castigos / Ahorros"
        '
        'SAHFrmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1115, 626)
        Me.Controls.Add(Me.panelContenedor)
        Me.Controls.Add(Me.panelMenu)
        Me.Controls.Add(Me.BarraTitulo)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "SAHFrmPrincipal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "FrmPrincipal"
        Me.panelMenu.ResumeLayout(False)
        Me.panelMenu.PerformLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.btnMenu, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.BarraTitulo.ResumeLayout(False)
        Me.BarraTitulo.PerformLayout()
        CType(Me.pictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.submenuAhorro.ResumeLayout(False)
        Me.submenuCertificados.ResumeLayout(False)
        Me.submenuDescuentosCastigos.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents btnCupones As Button
    Private WithEvents btnBloqueos As Button
    Private WithEvents btnAhorros As Button
    Private WithEvents btnInicio As Button
    Private WithEvents panelMenu As Panel
    Private WithEvents pictureBox5 As PictureBox
    Private WithEvents pictureBox6 As PictureBox
    Private WithEvents pictureBox3 As PictureBox
    Private WithEvents pictureBox4 As PictureBox
    Private WithEvents pictureBox2 As PictureBox
    Private WithEvents pictureBox1 As PictureBox
    Friend WithEvents tmMostrarMenu As Timer
    Friend WithEvents tmOcultarMenu As Timer
    Private WithEvents lbFecha As Label
    Private WithEvents lblHora As Label
    Private WithEvents btnMenu As PictureBox
    Friend WithEvents tmFechaHora As Timer
    Private WithEvents label1 As Label
    Private WithEvents pictureBox8 As PictureBox
    Private WithEvents BarraTitulo As Panel
    Private WithEvents btnNormal As Button
    Private WithEvents btnMax As Button
    Private WithEvents btnMinimizar As Button
    Private WithEvents btnCerrar As Button
    Friend WithEvents submenuAhorro As ContextMenuStrip
    Friend WithEvents NuevoAhorroToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TiposDeAhorroToolStripMenuItem As ToolStripMenuItem
    Private WithEvents btnReportes As Button
    Private WithEvents btnCertificado As Button
    Private WithEvents btnDescuentosCastigos As Button
    Friend WithEvents submenuCertificados As ContextMenuStrip
    Friend WithEvents NuevoCertificadoToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents TiposDeCertificadosToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ConsultarTiposDeAhorroToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsultarTiposDeCertificadosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConsultarAhorrosToolStripMenuItem As ToolStripMenuItem
    Public WithEvents panelContenedor As Panel
    Private WithEvents PictureBox9 As PictureBox
    Private WithEvents PictureBox7 As PictureBox
    Private WithEvents btnTasasInteres As Button
    Friend WithEvents ConsultarCertificados As ToolStripMenuItem
    Friend WithEvents submenuDescuentosCastigos As ContextMenuStrip
    Friend WithEvents DescuentosCastigos As ToolStripMenuItem
    Friend WithEvents DescuentosCastigosAhorros As ToolStripMenuItem
End Class

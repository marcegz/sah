﻿Public Class SAHFrmConsultaCupones

    Dim negocioCupones As New SAHNegocioCupones
    Dim frmPrincipal As SAHFrmPrincipal

    Private Sub SAHFrmConsultaCupones_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cargarDatos()

    End Sub

    Sub cargarDatos()
        dgvCupones.DataSource = negocioCupones.getTablaConsultaCupones(txtConsultar.Text)
        modificaAtributosTabla()
    End Sub

    Sub modificaAtributosTabla()
        dgvCupones.Columns(0).HeaderText = "ID"
        dgvCupones.Columns(1).HeaderText = "Fecha Generado"
        dgvCupones.Columns(2).HeaderText = "Fecha Vencimiento"
        dgvCupones.Columns(3).HeaderText = "Estado Vencimiento"
        dgvCupones.Columns(4).HeaderText = "Intereses"
        dgvCupones.Columns(5).HeaderText = "Ahorro"
        dgvCupones.Columns(6).HeaderText = "Tipo Certificado"
    End Sub

    Private Sub txtConsultar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtConsultar.KeyUp
        cargarDatos()
    End Sub


End Class
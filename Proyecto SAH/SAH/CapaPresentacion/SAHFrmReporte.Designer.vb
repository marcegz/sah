﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SAHFrmReporte
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.Reporte = New System.Windows.Forms.Label()
        Me.dgvReporte = New System.Windows.Forms.DataGridView()
        Me.lNombre = New System.Windows.Forms.Label()
        Me.txtNombre = New System.Windows.Forms.TextBox()
        Me.lAsociado = New System.Windows.Forms.Label()
        Me.cbReportes = New System.Windows.Forms.ComboBox()
        Me.btnGenerarReporte = New System.Windows.Forms.Button()
        Me.cbAsociados = New System.Windows.Forms.ComboBox()
        Me.cbTipo = New System.Windows.Forms.ComboBox()
        Me.lTipo = New System.Windows.Forms.Label()
        Me.cbMoneda = New System.Windows.Forms.ComboBox()
        Me.lMoneda = New System.Windows.Forms.Label()
        Me.cbConsulta = New System.Windows.Forms.ComboBox()
        Me.lConsultar = New System.Windows.Forms.Label()
        Me.cbConsultaCupones = New System.Windows.Forms.ComboBox()
        Me.cbCertificados = New System.Windows.Forms.ComboBox()
        Me.lCertificado = New System.Windows.Forms.Label()
        Me.lTipoAhorro = New System.Windows.Forms.Label()
        Me.cbTipoAhorro = New System.Windows.Forms.ComboBox()
        Me.cbConsultaCuentas = New System.Windows.Forms.ComboBox()
        CType(Me.dgvReporte, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Reporte
        '
        Me.Reporte.AutoSize = True
        Me.Reporte.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Reporte.ForeColor = System.Drawing.Color.Black
        Me.Reporte.Location = New System.Drawing.Point(45, 40)
        Me.Reporte.Name = "Reporte"
        Me.Reporte.Size = New System.Drawing.Size(72, 21)
        Me.Reporte.TabIndex = 101
        Me.Reporte.Text = "Reportes"
        '
        'dgvReporte
        '
        Me.dgvReporte.AllowUserToAddRows = False
        Me.dgvReporte.AllowUserToDeleteRows = False
        Me.dgvReporte.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvReporte.BackgroundColor = System.Drawing.Color.White
        Me.dgvReporte.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvReporte.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.BackColor = System.Drawing.Color.LightGray
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReporte.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvReporte.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle5.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Segoe UI", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightBlue
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvReporte.DefaultCellStyle = DataGridViewCellStyle5
        Me.dgvReporte.GridColor = System.Drawing.Color.White
        Me.dgvReporte.Location = New System.Drawing.Point(28, 161)
        Me.dgvReporte.Name = "dgvReporte"
        Me.dgvReporte.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvReporte.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.dgvReporte.RowHeadersVisible = False
        Me.dgvReporte.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvReporte.Size = New System.Drawing.Size(811, 303)
        Me.dgvReporte.TabIndex = 100
        '
        'lNombre
        '
        Me.lNombre.AutoSize = True
        Me.lNombre.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lNombre.ForeColor = System.Drawing.Color.Black
        Me.lNombre.Location = New System.Drawing.Point(529, 40)
        Me.lNombre.Name = "lNombre"
        Me.lNombre.Size = New System.Drawing.Size(71, 21)
        Me.lNombre.TabIndex = 97
        Me.lNombre.Text = "Nombre:"
        Me.lNombre.Visible = False
        '
        'txtNombre
        '
        Me.txtNombre.BackColor = System.Drawing.Color.White
        Me.txtNombre.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombre.Location = New System.Drawing.Point(620, 37)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(206, 29)
        Me.txtNombre.TabIndex = 96
        Me.txtNombre.Visible = False
        Me.txtNombre.WordWrap = False
        '
        'lAsociado
        '
        Me.lAsociado.AutoSize = True
        Me.lAsociado.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lAsociado.ForeColor = System.Drawing.Color.Black
        Me.lAsociado.Location = New System.Drawing.Point(45, 112)
        Me.lAsociado.Name = "lAsociado"
        Me.lAsociado.Size = New System.Drawing.Size(76, 21)
        Me.lAsociado.TabIndex = 94
        Me.lAsociado.Text = "Asociado:"
        Me.lAsociado.Visible = False
        '
        'cbReportes
        '
        Me.cbReportes.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbReportes.FormattingEnabled = True
        Me.cbReportes.Items.AddRange(New Object() {"Autorizados de Ahorro Escolar ", "Autorizados de Otros Ahorros ", "Beneficiarios de Ahorro Escolar", "Beneficiarios de Otros Ahorros", "Beneficiarios por Asociado", "Bloqueos por Fecha y Estado", "Certificados Cancelados", "Certificados en Custodia", "Certificados por Asociado", "Certificados por Asociado y Moneda", "Certificados Vencidos", "Cuentas Activas por Asociado", "Cuentas de Ahorro Escolar", "Cupones por Asociado y Certificado", "Intereses Generados por Cupón", "Rangos para Cálculo de Intereses", "Reimpresión de Certificados", "Saldos de Cuentas de Ahorro", "Castigos por atraso", "Descuentos por Retiro Anticipado", "Tipos de Ahorro", "Transacciones por Cuenta y Tipo de Ahorro", "Vencimiento de Certificados", "Vencimiento de Cupones"})
        Me.cbReportes.Location = New System.Drawing.Point(123, 37)
        Me.cbReportes.Name = "cbReportes"
        Me.cbReportes.Size = New System.Drawing.Size(388, 29)
        Me.cbReportes.TabIndex = 103
        '
        'btnGenerarReporte
        '
        Me.btnGenerarReporte.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btnGenerarReporte.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGenerarReporte.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnGenerarReporte.FlatAppearance.BorderSize = 0
        Me.btnGenerarReporte.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnGenerarReporte.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.btnGenerarReporte.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGenerarReporte.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGenerarReporte.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnGenerarReporte.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGenerarReporte.Location = New System.Drawing.Point(334, 505)
        Me.btnGenerarReporte.Name = "btnGenerarReporte"
        Me.btnGenerarReporte.Size = New System.Drawing.Size(150, 36)
        Me.btnGenerarReporte.TabIndex = 104
        Me.btnGenerarReporte.Text = "Generar Reporte"
        Me.btnGenerarReporte.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGenerarReporte.UseVisualStyleBackColor = False
        '
        'cbAsociados
        '
        Me.cbAsociados.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAsociados.FormattingEnabled = True
        Me.cbAsociados.Location = New System.Drawing.Point(123, 109)
        Me.cbAsociados.Name = "cbAsociados"
        Me.cbAsociados.Size = New System.Drawing.Size(388, 29)
        Me.cbAsociados.TabIndex = 105
        Me.cbAsociados.Visible = False
        '
        'cbTipo
        '
        Me.cbTipo.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipo.FormattingEnabled = True
        Me.cbTipo.Items.AddRange(New Object() {"Estado", "Fecha"})
        Me.cbTipo.Location = New System.Drawing.Point(631, 37)
        Me.cbTipo.Name = "cbTipo"
        Me.cbTipo.Size = New System.Drawing.Size(195, 29)
        Me.cbTipo.TabIndex = 106
        Me.cbTipo.Visible = False
        '
        'lTipo
        '
        Me.lTipo.AutoSize = True
        Me.lTipo.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTipo.ForeColor = System.Drawing.Color.Black
        Me.lTipo.Location = New System.Drawing.Point(571, 40)
        Me.lTipo.Name = "lTipo"
        Me.lTipo.Size = New System.Drawing.Size(43, 21)
        Me.lTipo.TabIndex = 107
        Me.lTipo.Text = "Tipo:"
        Me.lTipo.Visible = False
        '
        'cbMoneda
        '
        Me.cbMoneda.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMoneda.FormattingEnabled = True
        Me.cbMoneda.Items.AddRange(New Object() {"Colones", "Dólares"})
        Me.cbMoneda.Location = New System.Drawing.Point(123, 109)
        Me.cbMoneda.Name = "cbMoneda"
        Me.cbMoneda.Size = New System.Drawing.Size(195, 29)
        Me.cbMoneda.TabIndex = 108
        Me.cbMoneda.Visible = False
        '
        'lMoneda
        '
        Me.lMoneda.AutoSize = True
        Me.lMoneda.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMoneda.ForeColor = System.Drawing.Color.Black
        Me.lMoneda.Location = New System.Drawing.Point(45, 112)
        Me.lMoneda.Name = "lMoneda"
        Me.lMoneda.Size = New System.Drawing.Size(70, 21)
        Me.lMoneda.TabIndex = 109
        Me.lMoneda.Text = "Moneda:"
        Me.lMoneda.Visible = False
        '
        'cbConsulta
        '
        Me.cbConsulta.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbConsulta.FormattingEnabled = True
        Me.cbConsulta.Items.AddRange(New Object() {"Asociado", "Moneda"})
        Me.cbConsulta.Location = New System.Drawing.Point(631, 37)
        Me.cbConsulta.Name = "cbConsulta"
        Me.cbConsulta.Size = New System.Drawing.Size(195, 29)
        Me.cbConsulta.TabIndex = 110
        Me.cbConsulta.Visible = False
        '
        'lConsultar
        '
        Me.lConsultar.AutoSize = True
        Me.lConsultar.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lConsultar.ForeColor = System.Drawing.Color.Black
        Me.lConsultar.Location = New System.Drawing.Point(534, 40)
        Me.lConsultar.Name = "lConsultar"
        Me.lConsultar.Size = New System.Drawing.Size(80, 21)
        Me.lConsultar.TabIndex = 111
        Me.lConsultar.Text = "Consultar:"
        Me.lConsultar.Visible = False
        '
        'cbConsultaCupones
        '
        Me.cbConsultaCupones.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbConsultaCupones.FormattingEnabled = True
        Me.cbConsultaCupones.Items.AddRange(New Object() {"Asociado", "Certificado"})
        Me.cbConsultaCupones.Location = New System.Drawing.Point(631, 37)
        Me.cbConsultaCupones.Name = "cbConsultaCupones"
        Me.cbConsultaCupones.Size = New System.Drawing.Size(195, 29)
        Me.cbConsultaCupones.TabIndex = 112
        Me.cbConsultaCupones.Visible = False
        '
        'cbCertificados
        '
        Me.cbCertificados.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCertificados.FormattingEnabled = True
        Me.cbCertificados.Location = New System.Drawing.Point(79, 109)
        Me.cbCertificados.Name = "cbCertificados"
        Me.cbCertificados.Size = New System.Drawing.Size(272, 29)
        Me.cbCertificados.TabIndex = 113
        Me.cbCertificados.Visible = False
        '
        'lCertificado
        '
        Me.lCertificado.AutoSize = True
        Me.lCertificado.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lCertificado.ForeColor = System.Drawing.Color.Black
        Me.lCertificado.Location = New System.Drawing.Point(45, 112)
        Me.lCertificado.Name = "lCertificado"
        Me.lCertificado.Size = New System.Drawing.Size(88, 21)
        Me.lCertificado.TabIndex = 114
        Me.lCertificado.Text = "Certificado:"
        Me.lCertificado.Visible = False
        '
        'lTipoAhorro
        '
        Me.lTipoAhorro.AutoSize = True
        Me.lTipoAhorro.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTipoAhorro.ForeColor = System.Drawing.Color.Black
        Me.lTipoAhorro.Location = New System.Drawing.Point(45, 112)
        Me.lTipoAhorro.Name = "lTipoAhorro"
        Me.lTipoAhorro.Size = New System.Drawing.Size(28, 21)
        Me.lTipoAhorro.TabIndex = 98
        Me.lTipoAhorro.Text = "ID:"
        Me.lTipoAhorro.Visible = False
        '
        'cbTipoAhorro
        '
        Me.cbTipoAhorro.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipoAhorro.FormattingEnabled = True
        Me.cbTipoAhorro.Location = New System.Drawing.Point(139, 109)
        Me.cbTipoAhorro.Name = "cbTipoAhorro"
        Me.cbTipoAhorro.Size = New System.Drawing.Size(272, 29)
        Me.cbTipoAhorro.TabIndex = 99
        Me.cbTipoAhorro.Visible = False
        '
        'cbConsultaCuentas
        '
        Me.cbConsultaCuentas.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbConsultaCuentas.FormattingEnabled = True
        Me.cbConsultaCuentas.Items.AddRange(New Object() {"Ahorro", "Tipo de Ahorro"})
        Me.cbConsultaCuentas.Location = New System.Drawing.Point(631, 37)
        Me.cbConsultaCuentas.Name = "cbConsultaCuentas"
        Me.cbConsultaCuentas.Size = New System.Drawing.Size(195, 29)
        Me.cbConsultaCuentas.TabIndex = 115
        Me.cbConsultaCuentas.Visible = False
        '
        'SAHFrmReporte
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(869, 544)
        Me.Controls.Add(Me.cbConsultaCuentas)
        Me.Controls.Add(Me.lCertificado)
        Me.Controls.Add(Me.cbCertificados)
        Me.Controls.Add(Me.cbConsultaCupones)
        Me.Controls.Add(Me.lConsultar)
        Me.Controls.Add(Me.cbConsulta)
        Me.Controls.Add(Me.lMoneda)
        Me.Controls.Add(Me.cbMoneda)
        Me.Controls.Add(Me.lTipo)
        Me.Controls.Add(Me.cbTipo)
        Me.Controls.Add(Me.cbAsociados)
        Me.Controls.Add(Me.btnGenerarReporte)
        Me.Controls.Add(Me.cbReportes)
        Me.Controls.Add(Me.Reporte)
        Me.Controls.Add(Me.dgvReporte)
        Me.Controls.Add(Me.cbTipoAhorro)
        Me.Controls.Add(Me.lTipoAhorro)
        Me.Controls.Add(Me.lNombre)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.lAsociado)
        Me.Name = "SAHFrmReporte"
        Me.Text = "Reportes"
        CType(Me.dgvReporte, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Private WithEvents Reporte As Label
    Friend WithEvents dgvReporte As DataGridView
    Private WithEvents lNombre As Label
    Public WithEvents txtNombre As TextBox
    Private WithEvents lAsociado As Label
    Friend WithEvents cbReportes As ComboBox
    Private WithEvents btnGenerarReporte As Button
    Friend WithEvents cbAsociados As ComboBox
    Friend WithEvents cbTipo As ComboBox
    Private WithEvents lTipo As Label
    Friend WithEvents cbMoneda As ComboBox
    Private WithEvents lMoneda As Label
    Friend WithEvents cbConsulta As ComboBox
    Private WithEvents lConsultar As Label
    Friend WithEvents cbConsultaCupones As ComboBox
    Friend WithEvents cbCertificados As ComboBox
    Private WithEvents lCertificado As Label
    Private WithEvents lTipoAhorro As Label
    Friend WithEvents cbTipoAhorro As ComboBox
    Friend WithEvents cbConsultaCuentas As ComboBox
End Class

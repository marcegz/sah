﻿Imports System.Drawing
Public Class SAHFrmSeleccionarBeneficiarios

    Dim negocioParientesAhorros As New SAHNegocioParientesAhorro
    Dim negocioAhorros As New SAHNegocioAhorros
    Dim idAsociado As Integer
    Dim idAhorro As Integer

    Public Sub New(idAsociadoU As Integer, idAhorroU As Integer)
        InitializeComponent()
        idAsociado = idAsociadoU
        idAhorro = idAhorroU
    End Sub

    Private Sub FrmSeleccionarBeneficiarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cargaDatos()
        cargarParientesAhorro()

    End Sub

    Sub cargarParientesAhorro()

        Dim cedula As String
        Dim idPersona, idPariente As Integer

        For Each oRow As DataGridViewRow In dgvParientes.Rows

            cedula = oRow.Cells(1).Value
            idPersona = NegocioAhorros.getIdPersona(cedula)
            idPariente = negocioParientesAhorros.getIdPariente(idPersona)

            If negocioParientesAhorros.verificarExisteParienteAhorro(idPariente, idAhorro) = True Then

                oRow.Cells(0).Value = True
            End If

        Next

    End Sub



    Sub cargaDatos()
        dgvParientes.DataSource = negocioParientesAhorros.getListaParientesAsociado(idAsociado)
        modificaAtributosTabla()
    End Sub

    Sub modificaAtributosTabla()
        dgvParientes.Columns(1).HeaderText = "Identificación"
        dgvParientes.Columns(2).HeaderText = "Nombre"
        dgvParientes.Columns(3).HeaderText = "Apellido Uno"
        dgvParientes.Columns(4).HeaderText = "Apellido Dos"
        dgvParientes.Columns(5).HeaderText = "Parentesco"
    End Sub

    Private Sub BtnCerrar_Click(sender As Object, e As EventArgs) Handles BtnCerrar.Click
        Me.Close()
    End Sub

    Private Sub btnCancelar_Click(sender As Object, e As EventArgs) Handles btnCancelar.Click
        If MsgBox("¿Está seguro que desea cancelar?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
           MsgBoxResult.Yes Then
            Me.Hide()
        End If
    End Sub

    Private Sub btnGuardar_Click(sender As Object, e As EventArgs) Handles btnGuardar.Click
        Dim cedula As String
        Dim idPersona, idPariente As Integer

        For Each oRow As DataGridViewRow In dgvParientes.Rows


            cedula = oRow.Cells(1).Value
            idPersona = negocioAhorros.getIdPersona(cedula)
            idPariente = negocioParientesAhorros.getIdPariente(idPersona)

            If oRow.Cells(0).Value = True Then

                If negocioParientesAhorros.verificarExisteParienteAhorro(idPariente, idAhorro) = False Then
                    negocioParientesAhorros.insertarParienteAhorro(idPariente, idAhorro)
                End If

            Else
                If negocioParientesAhorros.verificarExisteParienteAhorro(idPariente, idAhorro) Then
                    negocioParientesAhorros.eliminarParienteAhorro(idPariente, idAhorro)
                End If

            End If

        Next

        MsgBox("Cambios guardados exitosamente", MsgBoxStyle.Information, "Mensaje Información")

        Me.Hide()
    End Sub
End Class
﻿Public Class SAHFrmConsultaTiposCertificados

    Dim negocioTiposCertificados As New SAHNegocioTiposCertificados
    Dim frmPrincipal As SAHFrmPrincipal


    Private Sub FrmConsultaTiposAhorros_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cargarDatos()
        Dim btnclm As DataGridViewButtonColumn = New DataGridViewButtonColumn()
        btnclm.Name = "Consultar"
        btnclm.FlatStyle = FlatStyle.Flat
        dgvTiposCertificados.Columns.Add(btnclm)
        modificaAtributosTabla()
    End Sub

    Public Sub New(frmPrincipalU As SAHFrmPrincipal)
        InitializeComponent()
        frmPrincipal = frmPrincipalU
    End Sub

    Sub cargarDatos()
        dgvTiposCertificados.DataSource = negocioTiposCertificados.getTablaConsultaTiposCertificados(txtConsultar.Text)


    End Sub

    Sub modificaAtributosTabla()
        dgvTiposCertificados.Columns(0).HeaderText = "ID"
        dgvTiposCertificados.Columns(1).HeaderText = "Nombre"
        dgvTiposCertificados.Columns(2).HeaderText = "Descripción"
        dgvTiposCertificados.Columns(3).HeaderText = "Activo"
        dgvTiposCertificados.Columns(4).HeaderText = "Costo"
        dgvTiposCertificados.Columns(5).HeaderText = "Consultar"
    End Sub

    Private Sub txtConsultar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtConsultar.KeyUp
        cargarDatos()
    End Sub


    Private Sub dgvTiposCertificados_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvTiposCertificados.CellPainting
        If e.ColumnIndex >= 0 AndAlso Me.dgvTiposCertificados.Columns(e.ColumnIndex).Name = "Consultar" AndAlso e.RowIndex >= 0 Then

            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = TryCast(Me.dgvTiposCertificados.Rows(e.RowIndex).Cells("Consultar"), DataGridViewButtonCell)
            'Dim icoAtomico As Icon = New Icon("C:\Users\Marce\source\repos\project\Proyecto SAH\SAH\Resources\consultarICON.ico")
            Dim icoAtomico As Icon = New Icon("C:\Users\Marce\source\repos\project\Proyecto SAH\SAH\Resources\consultarICON.ico")
            e.Graphics.DrawIcon(icoAtomico, e.CellBounds.Left + 17, e.CellBounds.Top + 4)
            Me.dgvTiposCertificados.Rows(e.RowIndex).Height = icoAtomico.Height + 7
            Me.dgvTiposCertificados.Columns(e.ColumnIndex).Width = icoAtomico.Width + 35


            e.Handled = True
        End If
    End Sub

    Private Sub dgvTiposCertificados_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTiposCertificados.CellContentClick

        If e.ColumnIndex = 0 Then
            Dim rows As DataGridViewRow
            rows = dgvTiposCertificados.Rows(e.RowIndex)

            Dim tipoCertificado As SAHTiposCertificados
            tipoCertificado = negocioTiposCertificados.getTipoCertificado(rows.Cells(1).Value)

            Dim f As New SAHFrmTiposCertificados(tipoCertificado, frmPrincipal, New SAHFrmConsultaTiposCertificados(frmPrincipal))
            frmPrincipal.AbrirFormEnPanel(f)

        End If
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SAHFrmCertificados
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.cbReimpreso = New System.Windows.Forms.CheckBox()
        Me.lReimpreso = New System.Windows.Forms.Label()
        Me.cbTiposCertificados = New System.Windows.Forms.ComboBox()
        Me.cbAhorros = New System.Windows.Forms.ComboBox()
        Me.cbEstado = New System.Windows.Forms.ComboBox()
        Me.dtpFecha = New System.Windows.Forms.DateTimePicker()
        Me.lFechaVencimiento = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.lId = New System.Windows.Forms.Label()
        Me.txtId = New System.Windows.Forms.TextBox()
        Me.lEstado = New System.Windows.Forms.Label()
        Me.lAhorro = New System.Windows.Forms.Label()
        Me.lTipoCertificado = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.BtnCerrar = New System.Windows.Forms.Button()
        Me.btnEditar = New System.Windows.Forms.Button()
        Me.btnAtras = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.cbReimpreso)
        Me.GroupBox1.Controls.Add(Me.lReimpreso)
        Me.GroupBox1.Controls.Add(Me.cbTiposCertificados)
        Me.GroupBox1.Controls.Add(Me.cbAhorros)
        Me.GroupBox1.Controls.Add(Me.cbEstado)
        Me.GroupBox1.Controls.Add(Me.dtpFecha)
        Me.GroupBox1.Controls.Add(Me.lFechaVencimiento)
        Me.GroupBox1.Controls.Add(Me.PictureBox1)
        Me.GroupBox1.Controls.Add(Me.lId)
        Me.GroupBox1.Controls.Add(Me.txtId)
        Me.GroupBox1.Controls.Add(Me.lEstado)
        Me.GroupBox1.Controls.Add(Me.lAhorro)
        Me.GroupBox1.Controls.Add(Me.lTipoCertificado)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(94, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(708, 341)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Certificados"
        '
        'cbReimpreso
        '
        Me.cbReimpreso.AutoSize = True
        Me.cbReimpreso.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.cbReimpreso.Location = New System.Drawing.Point(587, 274)
        Me.cbReimpreso.Name = "cbReimpreso"
        Me.cbReimpreso.Size = New System.Drawing.Size(15, 14)
        Me.cbReimpreso.TabIndex = 99
        Me.cbReimpreso.UseVisualStyleBackColor = True
        Me.cbReimpreso.Visible = False
        '
        'lReimpreso
        '
        Me.lReimpreso.AutoSize = True
        Me.lReimpreso.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.lReimpreso.ForeColor = System.Drawing.Color.Black
        Me.lReimpreso.Location = New System.Drawing.Point(493, 267)
        Me.lReimpreso.Name = "lReimpreso"
        Me.lReimpreso.Size = New System.Drawing.Size(88, 21)
        Me.lReimpreso.TabIndex = 98
        Me.lReimpreso.Text = "Reimpreso:"
        Me.lReimpreso.Visible = False
        '
        'cbTiposCertificados
        '
        Me.cbTiposCertificados.AutoCompleteCustomSource.AddRange(New String() {"Planilla", "Depósito"})
        Me.cbTiposCertificados.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTiposCertificados.FormattingEnabled = True
        Me.cbTiposCertificados.Location = New System.Drawing.Point(188, 107)
        Me.cbTiposCertificados.Name = "cbTiposCertificados"
        Me.cbTiposCertificados.Size = New System.Drawing.Size(245, 29)
        Me.cbTiposCertificados.TabIndex = 97
        Me.cbTiposCertificados.Tag = ""
        '
        'cbAhorros
        '
        Me.cbAhorros.AutoCompleteCustomSource.AddRange(New String() {"Planilla", "Depósito"})
        Me.cbAhorros.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbAhorros.FormattingEnabled = True
        Me.cbAhorros.Location = New System.Drawing.Point(188, 155)
        Me.cbAhorros.Name = "cbAhorros"
        Me.cbAhorros.Size = New System.Drawing.Size(245, 29)
        Me.cbAhorros.TabIndex = 96
        Me.cbAhorros.Tag = ""
        '
        'cbEstado
        '
        Me.cbEstado.AutoCompleteCustomSource.AddRange(New String() {"Planilla", "Depósito"})
        Me.cbEstado.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbEstado.FormattingEnabled = True
        Me.cbEstado.Items.AddRange(New Object() {"Vigente", "Cancelado", "Vencido"})
        Me.cbEstado.Location = New System.Drawing.Point(188, 267)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Size = New System.Drawing.Size(245, 29)
        Me.cbEstado.TabIndex = 95
        Me.cbEstado.Tag = ""
        Me.cbEstado.Visible = False
        '
        'dtpFecha
        '
        Me.dtpFecha.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.dtpFecha.Location = New System.Drawing.Point(188, 207)
        Me.dtpFecha.Name = "dtpFecha"
        Me.dtpFecha.Size = New System.Drawing.Size(245, 29)
        Me.dtpFecha.TabIndex = 79
        '
        'lFechaVencimiento
        '
        Me.lFechaVencimiento.AutoSize = True
        Me.lFechaVencimiento.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lFechaVencimiento.ForeColor = System.Drawing.Color.Black
        Me.lFechaVencimiento.Location = New System.Drawing.Point(34, 213)
        Me.lFechaVencimiento.Name = "lFechaVencimiento"
        Me.lFechaVencimiento.Size = New System.Drawing.Size(143, 21)
        Me.lFechaVencimiento.TabIndex = 78
        Me.lFechaVencimiento.Text = "Fecha Vencimiento:"
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Image = Global.SAH.My.Resources.Resources.certificado
        Me.PictureBox1.Location = New System.Drawing.Point(480, 73)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(171, 172)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 77
        Me.PictureBox1.TabStop = False
        '
        'lId
        '
        Me.lId.AutoSize = True
        Me.lId.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lId.ForeColor = System.Drawing.Color.Black
        Me.lId.Location = New System.Drawing.Point(36, 60)
        Me.lId.Name = "lId"
        Me.lId.Size = New System.Drawing.Size(28, 21)
        Me.lId.TabIndex = 76
        Me.lId.Text = "ID:"
        '
        'txtId
        '
        Me.txtId.Enabled = False
        Me.txtId.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.txtId.Location = New System.Drawing.Point(188, 60)
        Me.txtId.Name = "txtId"
        Me.txtId.ReadOnly = True
        Me.txtId.Size = New System.Drawing.Size(245, 29)
        Me.txtId.TabIndex = 75
        '
        'lEstado
        '
        Me.lEstado.AutoSize = True
        Me.lEstado.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lEstado.ForeColor = System.Drawing.Color.Black
        Me.lEstado.Location = New System.Drawing.Point(36, 275)
        Me.lEstado.Name = "lEstado"
        Me.lEstado.Size = New System.Drawing.Size(59, 21)
        Me.lEstado.TabIndex = 74
        Me.lEstado.Text = "Estado:"
        Me.lEstado.Visible = False
        '
        'lAhorro
        '
        Me.lAhorro.AutoSize = True
        Me.lAhorro.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lAhorro.ForeColor = System.Drawing.Color.Black
        Me.lAhorro.Location = New System.Drawing.Point(36, 158)
        Me.lAhorro.Name = "lAhorro"
        Me.lAhorro.Size = New System.Drawing.Size(62, 21)
        Me.lAhorro.TabIndex = 73
        Me.lAhorro.Text = "Ahorro:"
        '
        'lTipoCertificado
        '
        Me.lTipoCertificado.AutoSize = True
        Me.lTipoCertificado.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTipoCertificado.ForeColor = System.Drawing.Color.Black
        Me.lTipoCertificado.Location = New System.Drawing.Point(34, 110)
        Me.lTipoCertificado.Name = "lTipoCertificado"
        Me.lTipoCertificado.Size = New System.Drawing.Size(122, 21)
        Me.lTipoCertificado.TabIndex = 72
        Me.lTipoCertificado.Text = "Tipo Certificado:"
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.FromArgb(CType(CType(13, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(142, Byte), Integer))
        Me.btnCancelar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnCancelar.FlatAppearance.BorderSize = 0
        Me.btnCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCancelar.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(491, 466)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(150, 36)
        Me.btnCancelar.TabIndex = 77
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnGuardar.FlatAppearance.BorderSize = 0
        Me.btnGuardar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnGuardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.Location = New System.Drawing.Point(291, 466)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(150, 36)
        Me.btnGuardar.TabIndex = 76
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'BtnCerrar
        '
        Me.BtnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnCerrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnCerrar.FlatAppearance.BorderSize = 0
        Me.BtnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCerrar.Image = Global.SAH.My.Resources.Resources.Cerrar
        Me.BtnCerrar.Location = New System.Drawing.Point(834, 2)
        Me.BtnCerrar.Name = "BtnCerrar"
        Me.BtnCerrar.Size = New System.Drawing.Size(32, 30)
        Me.BtnCerrar.TabIndex = 81
        Me.BtnCerrar.UseVisualStyleBackColor = True
        Me.BtnCerrar.Visible = False
        '
        'btnEditar
        '
        Me.btnEditar.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btnEditar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnEditar.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnEditar.FlatAppearance.BorderSize = 0
        Me.btnEditar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnEditar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.btnEditar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEditar.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnEditar.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnEditar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnEditar.Location = New System.Drawing.Point(291, 466)
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(150, 36)
        Me.btnEditar.TabIndex = 82
        Me.btnEditar.Text = "Editar"
        Me.btnEditar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnEditar.UseVisualStyleBackColor = False
        Me.btnEditar.Visible = False
        '
        'btnAtras
        '
        Me.btnAtras.BackColor = System.Drawing.Color.FromArgb(CType(CType(13, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(142, Byte), Integer))
        Me.btnAtras.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnAtras.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnAtras.FlatAppearance.BorderSize = 0
        Me.btnAtras.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnAtras.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAtras.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnAtras.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnAtras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAtras.Location = New System.Drawing.Point(491, 466)
        Me.btnAtras.Name = "btnAtras"
        Me.btnAtras.Size = New System.Drawing.Size(150, 36)
        Me.btnAtras.TabIndex = 83
        Me.btnAtras.Text = "Atrás"
        Me.btnAtras.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAtras.UseVisualStyleBackColor = False
        Me.btnAtras.Visible = False
        '
        'SAHFrmCertificados
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(869, 544)
        Me.Controls.Add(Me.btnAtras)
        Me.Controls.Add(Me.btnEditar)
        Me.Controls.Add(Me.BtnCerrar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "SAHFrmCertificados"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Private WithEvents btnCancelar As Button
    Private WithEvents btnGuardar As Button
    Friend WithEvents dtpFecha As DateTimePicker
    Private WithEvents lFechaVencimiento As Label
    Friend WithEvents PictureBox1 As PictureBox
    Private WithEvents lId As Label
    Public WithEvents txtId As TextBox
    Private WithEvents lEstado As Label
    Private WithEvents lAhorro As Label
    Private WithEvents lTipoCertificado As Label
    Private WithEvents BtnCerrar As Button
    Friend WithEvents cbTiposCertificados As ComboBox
    Friend WithEvents cbAhorros As ComboBox
    Friend WithEvents cbEstado As ComboBox
    Friend WithEvents cbReimpreso As CheckBox
    Private WithEvents lReimpreso As Label
    Private WithEvents btnEditar As Button
    Private WithEvents btnAtras As Button
End Class

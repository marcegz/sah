﻿Public Class SAHFrmConsultaTiposAhorros

    Dim negocioTiposAhorros As New SAHNegocioTiposAhorros
    Dim fPrincipal As New SAHFrmPrincipal
    Dim idUsuario As Integer

    Public Sub New(frmPrincipalU As SAHFrmPrincipal, idUsuarioU As Integer)
        InitializeComponent()
        fPrincipal = frmPrincipalU
        idUsuario = idUsuarioU

    End Sub

    Private Sub FrmConsultaTiposAhorros_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarTablaTiposAhorros()

        Dim btnclm As DataGridViewButtonColumn = New DataGridViewButtonColumn()
        btnclm.Name = "Consultar"
        btnclm.FlatStyle = FlatStyle.Flat

        dgvTiposAhorros.Columns.Add(btnclm)

        modificaAtributosTabla()
    End Sub


    Public Sub cargarTablaTiposAhorros()
        dgvTiposAhorros.DataSource = negocioTiposAhorros.getTablaConsultaTiposAhorros(txtConsultar.Text)
    End Sub


    Sub modificaAtributosTabla()
        dgvTiposAhorros.Columns(0).HeaderText = "ID"
        dgvTiposAhorros.Columns(1).HeaderText = "Nombre"
        dgvTiposAhorros.Columns(2).HeaderText = "Activo"
        dgvTiposAhorros.Columns(3).HeaderText = "Fecha Entrega"
        dgvTiposAhorros.Columns(4).HeaderText = "Personalizado"
        dgvTiposAhorros.Columns(5).HeaderText = "Consultar"
    End Sub

    Private Sub txtConsultar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtConsultar.KeyUp
        cargarTablaTiposAhorros()
    End Sub

    Private Sub dgvTiposAhorros_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvTiposAhorros.CellPainting

        If e.ColumnIndex >= 0 AndAlso Me.dgvTiposAhorros.Columns(e.ColumnIndex).Name = "Consultar" AndAlso e.RowIndex >= 0 Then

            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = TryCast(Me.dgvTiposAhorros.Rows(e.RowIndex).Cells("Consultar"), DataGridViewButtonCell)
            Dim icoAtomico As Icon = New Icon("C:\Users\Marce\source\repos\project\Proyecto SAH\SAH\Resources\consultarICON.ico")
            e.Graphics.DrawIcon(icoAtomico, e.CellBounds.Left + 17, e.CellBounds.Top + 4)
            Me.dgvTiposAhorros.Rows(e.RowIndex).Height = icoAtomico.Height + 7
            Me.dgvTiposAhorros.Columns(e.ColumnIndex).Width = icoAtomico.Width + 35

            e.Handled = True
        End If
    End Sub

    Private Sub dgvTiposAhorros_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTiposAhorros.CellContentClick


        If e.ColumnIndex = 0 Then

            Dim rows As DataGridViewRow
            rows = dgvTiposAhorros.Rows(e.RowIndex)
            Dim tipoAhorro As SAHTiposAhorros
            tipoAhorro = negocioTiposAhorros.getTipoAhorro(Convert.ToInt32(rows.Cells(1).Value))


            Dim fTipoAhorro As New SAHFrmTiposAhorros(fPrincipal, tipoAhorro, idUsuario)
            fPrincipal.AbrirFormEnPanel(fTipoAhorro)

        End If
    End Sub
End Class
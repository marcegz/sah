﻿Public Class SAHFrmReporte


    Dim negocioReportes As New SAHNegocioReportes
    Dim negocioAhorros As New SAHNegocioAhorros
    Dim negocioTiposAhorros As New SAHNegocioTiposAhorros


    Private Sub txtNombre_KeyUp(sender As Object, e As KeyEventArgs) Handles txtNombre.KeyUp
        If cbReportes.SelectedIndex = 15 Then
            dgvReporte.DataSource = negocioReportes.getTablaRangosCalculoInteres(txtNombre.Text)
        Else
            If cbReportes.SelectedIndex = 17 Then
                dgvReporte.DataSource = negocioAhorros.getTablaConsultaAhorros(txtNombre.Text)
            Else
                If cbReportes.SelectedIndex = 20 Then
                    dgvReporte.DataSource = negocioTiposAhorros.getTablaConsultaTiposAhorros(txtNombre.Text)

                End If
            End If
        End If
    End Sub

    Sub certificados()
        dgvReporte.Columns(0).HeaderText = "Fecha Generado"
        dgvReporte.Columns(1).HeaderText = "Fecha Vencimiento"
        dgvReporte.Columns(2).HeaderText = "Tipo Certitificado"
        dgvReporte.Columns(3).HeaderText = "Estado Vencimiento"
    End Sub


    Sub beneficiariosAutorizados()
        dgvReporte.Columns(0).HeaderText = "Cédula"
        dgvReporte.Columns(1).HeaderText = "Nombre"
        dgvReporte.Columns(2).HeaderText = "Apellido Uno"
        dgvReporte.Columns(3).HeaderText = "Apellido Dos"
    End Sub
    Private Sub cbReportes_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbReportes.SelectedValueChanged


        Dim eleccion As Integer = cbReportes.SelectedIndex

        Select Case eleccion

            Case 0
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaAutorizadosAhorroEscolar()
                beneficiariosAutorizados()

            Case 1
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaAutorizadosOtrosAhorros()
                beneficiariosAutorizados()

            Case 2
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaBeneficiariosAhorroEscolar()
                beneficiariosAutorizados()

            Case 3
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaBeneficiariosOtrosAhorros()
                beneficiariosAutorizados()

            Case 4
                lAsociado.Visible = True
                cbAsociados.Visible = True
                lTipo.Visible = False
                cbTipo.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = False
                cbConsulta.Visible = False
                cbConsultaCupones.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
                lNombre.Visible = False
                txtNombre.Visible = False
                cbConsultaCuentas.Visible = False

            Case 5
                lTipo.Visible = True
                cbTipo.Visible = True
                lAsociado.Visible = False
                cbAsociados.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = False
                cbConsulta.Visible = False
                cbConsultaCupones.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
                lNombre.Visible = False
                txtNombre.Visible = False
                cbConsultaCuentas.Visible = False

            Case 6
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaCertificadosCancelados()
                certificados()

            Case 7
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaCertificadosCustodia()
                certificados()

            Case 8
                lAsociado.Visible = True
                cbAsociados.Visible = True
                lAsociado.Visible = False
                cbAsociados.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = False
                cbConsulta.Visible = False
                cbConsultaCupones.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
                lNombre.Visible = False
                txtNombre.Visible = False
                cbConsultaCuentas.Visible = False

            Case 9
                lAsociado.Visible = False
                cbAsociados.Visible = False
                lAsociado.Visible = False
                cbAsociados.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = True
                cbConsulta.Visible = True
                cbConsultaCupones.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
                lNombre.Visible = False
                txtNombre.Visible = False
                cbConsultaCuentas.Visible = False

            Case 10
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaCertificadosVencidos()
                certificados()

            Case 11
                lAsociado.Visible = True
                cbAsociados.Visible = True
                lTipo.Visible = False
                cbTipo.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = False
                cbConsulta.Visible = False
                cbConsultaCupones.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
                lNombre.Visible = False
                txtNombre.Visible = False
                cbConsultaCuentas.Visible = False

            Case 12
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaCuentasAhorroEscolar()

            Case 13
                lNombre.Visible = False
                txtNombre.Visible = False
                cbConsultaCupones.Visible = True
                lConsultar.Visible = True
                lAsociado.Visible = False
                cbAsociados.Visible = False
                lTipo.Visible = False
                cbTipo.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = False
                cbConsulta.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
                cbConsultaCuentas.Visible = False

            Case 14
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaVencimientoCupones()

            Case 15
                lNombre.Visible = True
                txtNombre.Visible = True
                cbConsultaCupones.Visible = False
                lConsultar.Visible = True
                lAsociado.Visible = False
                cbAsociados.Visible = False
                lTipo.Visible = False
                cbTipo.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = False
                cbConsulta.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
                cbConsultaCuentas.Visible = False
                dgvReporte.DataSource = negocioReportes.getTablaRangosCalculoInteres("")

            Case 16
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaCertificadosReimpresos()

            Case 17
                lNombre.Visible = True
                txtNombre.Visible = True
                cbConsultaCupones.Visible = False
                lConsultar.Visible = True
                lAsociado.Visible = False
                cbAsociados.Visible = False
                lTipo.Visible = False
                cbTipo.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = False
                cbConsulta.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
                cbConsultaCuentas.Visible = False
                dgvReporte.DataSource = negocioAhorros.getTablaConsultaAhorros("")

            Case 18
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaCastigosAtrasos()

            Case 19
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaDescuentosRetiroAnticipado()

            Case 20
                lNombre.Visible = True
                txtNombre.Visible = True
                cbConsultaCupones.Visible = False
                lConsultar.Visible = True
                lAsociado.Visible = False
                cbAsociados.Visible = False
                lTipo.Visible = False
                cbTipo.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = False
                cbConsulta.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
                cbConsultaCuentas.Visible = False
                dgvReporte.DataSource = negocioTiposAhorros.getTablaConsultaTiposAhorros("")

            Case 21
                cbConsultaCuentas.Visible = True
                lNombre.Visible = False
                txtNombre.Visible = False
                cbConsultaCupones.Visible = False
                lConsultar.Visible = True
                lAsociado.Visible = False
                cbAsociados.Visible = False
                lTipo.Visible = False
                cbTipo.Visible = False
                lMoneda.Visible = False
                cbMoneda.Visible = False
                lConsultar.Visible = False
                cbConsulta.Visible = False
                lCertificado.Visible = False
                cbCertificados.Visible = False
            Case 22
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaVencimientoCertificados()
                dgvReporte.Columns(0).HeaderText = "Fecha Generado"
                dgvReporte.Columns(1).HeaderText = "Fecha Vencimiento"
                dgvReporte.Columns(2).HeaderText = "Estado Vencimiento"
                dgvReporte.Columns(3).HeaderText = "Tipo Certificado"

            Case 23
                ocultar()
                dgvReporte.DataSource = negocioReportes.getTablaVencimientoCupones()
                dgvReporte.Columns(0).HeaderText = "Fecha Generado"
                dgvReporte.Columns(1).HeaderText = "Fecha Vencimiento"
                dgvReporte.Columns(2).HeaderText = "Estado Vencimiento"
                dgvReporte.Columns(3).HeaderText = "Intereses"
                dgvReporte.Columns(4).HeaderText = "Tipo Certificado"

        End Select





    End Sub

    Sub ocultar()
        lNombre.Visible = False
        txtNombre.Visible = False
        cbConsultaCupones.Visible = False
        lConsultar.Visible = True
        lAsociado.Visible = False
        cbAsociados.Visible = False
        lTipo.Visible = False
        cbTipo.Visible = False
        lMoneda.Visible = False
        cbMoneda.Visible = False
        lConsultar.Visible = False
        cbConsulta.Visible = False
        lCertificado.Visible = False
        cbCertificados.Visible = False
        cbConsultaCuentas.Visible = False
    End Sub

    Private Sub FrmReporte_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cbReportes.SelectedIndex = 0


    End Sub

    Private Sub btnGenerarReporte_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click
        Dim reporte As New SAHReporteAhorros(cbReportes.SelectedIndex)
        reporte.Show()
    End Sub

    Private Sub cbAsociados_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbAsociados.SelectedValueChanged
        Dim info() As String
        info = cbAsociados.SelectedItem.ToString.Split("-")
        Dim cedula As String = info(0)
        Dim idAsociado As Integer = negocioAhorros.getIdAsociado(cedula)

        If cbReportes.SelectedIndex = 4 Then
            dgvReporte.DataSource = negocioReportes.getTablaBeneficiariosAsociado(idAsociado)
            beneficiariosAutorizados()
        Else
            If cbReportes.SelectedIndex = 8 Then
                dgvReporte.DataSource = negocioReportes.getTablaCertificadosAsociado(idAsociado)

            Else
                If cbReportes.SelectedIndex = 9 And cbConsulta.SelectedItem = "Asociado" Then
                    dgvReporte.DataSource = negocioReportes.getTablaCertificadosAsociadoMoneda(idAsociado, "", "Asociado")
                Else
                    If cbReportes.SelectedIndex = 11 Then
                        dgvReporte.DataSource = negocioReportes.getTablaCuentasActivasAsociado(idAsociado)
                    Else
                        If cbReportes.SelectedIndex = 13 Then
                            dgvReporte.DataSource = negocioReportes.getTablaCuponesAsociadoCertificado(idAsociado, "Asociado")

                        End If
                    End If
                End If
            End If

        End If

    End Sub

    Private Sub cbTipo_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbTipo.SelectedValueChanged
        dgvReporte.DataSource = negocioReportes.getTablaBloqueosFechaEstado(cbTipo.SelectedItem.ToString)
        dgvReporte.Columns(0).HeaderText = "ID Bloqueo"
        dgvReporte.Columns(1).HeaderText = "Tipo"
        dgvReporte.Columns(2).HeaderText = "Descripción"
        dgvReporte.Columns(3).HeaderText = "Fecha"
    End Sub

    Private Sub cbConsulta_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbConsulta.SelectedValueChanged

        If cbConsulta.SelectedItem = "Asociado" Then
            lAsociado.Visible = True
            cbAsociados.Visible = True
            lMoneda.Visible = False
            cbMoneda.Visible = False
        Else
            lAsociado.Visible = False
            cbAsociados.Visible = False
            lMoneda.Visible = True
            cbMoneda.Visible = True
        End If

    End Sub

    Private Sub cbMoneda_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbMoneda.SelectedValueChanged

        If cbReportes.SelectedIndex = 9 And cbConsulta.SelectedItem = "Moneda" Then
            dgvReporte.DataSource = negocioReportes.getTablaCertificadosAsociadoMoneda(0, cbMoneda.SelectedItem, "Moneda")
        End If

    End Sub

    Private Sub cbConsultaCupones_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbConsultaCupones.SelectedValueChanged


        If cbConsultaCupones.SelectedItem = "Asociado" Then
            lAsociado.Visible = True
            cbAsociados.Visible = True
            lCertificado.Visible = False
            cbCertificados.Visible = False
        Else
            lAsociado.Visible = False
            cbAsociados.Visible = False
            lCertificado.Visible = True
            cbCertificados.Visible = True
        End If

    End Sub

    Private Sub cbCertificados_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbCertificados.SelectedValueChanged
        Dim info() As String
        info = cbCertificados.SelectedItem.ToString.Split("-")
        Dim idCertificado As Integer = Integer.Parse(info(0))

        dgvReporte.DataSource = negocioReportes.getTablaCuponesAsociadoCertificado(idCertificado, "Certificado")

    End Sub

    Private Sub cbConsultaCuentas_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbConsultaCuentas.SelectedValueChanged
        If cbConsultaCuentas.SelectedItem = "Ahorro" Then

            cbTipoAhorro.DataSource = negocioAhorros.getTablaConsultaAhorros("")
            cbTipoAhorro.DisplayMember = "nombre"
            cbTipoAhorro.ValueMember = "idAhorro"
        Else
            cbTipoAhorro.DataSource = negocioTiposAhorros.getTiposAhorrosActivos()
            cbTipoAhorro.DisplayMember = "nombre"
            cbTipoAhorro.ValueMember = "idTipoAhorro"
        End If
    End Sub

    Private Sub cbTipoAhorro_SelectedValueChanged(sender As Object, e As EventArgs) Handles cbTipoAhorro.SelectedValueChanged

        If cbReportes.SelectedIndex = 21 And cbConsultaCuentas.SelectedItem = "Ahorro" Then
            dgvReporte.DataSource = negocioReportes.getTablaTransaccionesCuentaTipoAhorro("Ahorro", cbTipoAhorro.SelectedValue)
        Else
            If cbReportes.SelectedIndex = 21 And cbConsultaCuentas.SelectedItem = "Tipo Ahorro" Then
                dgvReporte.DataSource = negocioReportes.getTablaTransaccionesCuentaTipoAhorro("Tipo Ahorro", cbTipoAhorro.SelectedValue)

            End If
        End If

        dgvReporte.Columns(0).HeaderText = "Fecha"
        dgvReporte.Columns(1).HeaderText = "Monto"
        dgvReporte.Columns(2).HeaderText = "Detalle"
        dgvReporte.Columns(3).HeaderText = "Tipo"
        dgvReporte.Columns(4).HeaderText = "Ahorro"
        dgvReporte.Columns(5).HeaderText = "Tipo Ahorro"
    End Sub

    Private Sub dgvReporte_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvReporte.CellContentClick

    End Sub
End Class
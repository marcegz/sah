﻿Public Class SAHFrmConsultaDescuentosCastigosAhorros

    Dim f As New SAHFrmPrincipal
    Dim negocioDescuentosCastigosAhorros As New SAHNegocioDescuentosCastigosAhorros

    Private Sub FrmConsultaAhorros_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarTablaAhorros()

    End Sub

    Sub cargarTablaAhorros()
        dgvDC_A.DataSource = negocioDescuentosCastigosAhorros.getTablaDC_Ahorros(txtConsultar.Text)
        modificaAtributosTabla()
    End Sub

    Sub modificaAtributosTabla()
        dgvDC_A.Columns(0).HeaderText = "ID"
        dgvDC_A.Columns(1).HeaderText = "ID Ahorro"
        dgvDC_A.Columns(2).HeaderText = "Nombre Ahorro"
        dgvDC_A.Columns(3).HeaderText = "Meses"
        dgvDC_A.Columns(4).HeaderText = "Monto"
        dgvDC_A.Columns(5).HeaderText = "Fecha"
    End Sub

    Private Sub txtConsultar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtConsultar.KeyUp
        cargarTablaAhorros()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click

        Dim reporte As New SAHReporteAhorros(txtConsultar.Text)
        reporte.Show()


    End Sub


End Class
﻿Public Class SAHFrmConsultaCertificados

    Dim negocioCertificados As New SAHNegocioCertificados
    Dim frmPrincipal As SAHFrmPrincipal
    Dim idUsuario As Integer

    Private Sub SAHFrmConsultaCertificados_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        cargarDatos()
        Dim btnclm As DataGridViewButtonColumn = New DataGridViewButtonColumn()
        btnclm.Name = "Consultar"
        btnclm.FlatStyle = FlatStyle.Flat
        dgvCertificados.Columns.Add(btnclm)
        modificaAtributosTabla()
    End Sub

    Public Sub New(frmPrincipalU As SAHFrmPrincipal, idUsuarioU As Integer)
        InitializeComponent()
        frmPrincipal = frmPrincipalU
        idUsuario = idUsuarioU
    End Sub

    Sub cargarDatos()
        dgvCertificados.DataSource = negocioCertificados.getTablaConsultaCertificados(txtConsultar.Text)


    End Sub

    Sub modificaAtributosTabla()
        dgvCertificados.Columns(0).HeaderText = "ID"
        dgvCertificados.Columns(1).HeaderText = "Fecha Generado"
        dgvCertificados.Columns(2).HeaderText = "Fecha Vencimiento"
        dgvCertificados.Columns(3).HeaderText = "Estado Vencimiento"
        dgvCertificados.Columns(4).HeaderText = "Ahorro"
        dgvCertificados.Columns(5).HeaderText = "Tipo Certificado"
        dgvCertificados.Columns(6).HeaderText = "Consultar"
    End Sub

    Private Sub txtConsultar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtConsultar.KeyUp
        cargarDatos()
    End Sub


    Private Sub dgvTiposCertificados_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvCertificados.CellPainting
        If e.ColumnIndex >= 0 AndAlso Me.dgvCertificados.Columns(e.ColumnIndex).Name = "Consultar" AndAlso e.RowIndex >= 0 Then

            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = TryCast(Me.dgvCertificados.Rows(e.RowIndex).Cells("Consultar"), DataGridViewButtonCell)
            'Dim icoAtomico As Icon = New Icon("C:\Users\Marce\source\repos\project\Proyecto SAH\SAH\Resources\consultarICON.ico")
            Dim icoAtomico As Icon = New Icon("C:\Users\Marce\source\repos\project\Proyecto SAH\SAH\Resources\consultarICON.ico")
            e.Graphics.DrawIcon(icoAtomico, e.CellBounds.Left + 17, e.CellBounds.Top + 4)
            Me.dgvCertificados.Rows(e.RowIndex).Height = icoAtomico.Height + 7
            Me.dgvCertificados.Columns(e.ColumnIndex).Width = icoAtomico.Width + 35


            e.Handled = True
        End If
    End Sub

    Private Sub dgvCertificados_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCertificados.CellContentClick


        If e.ColumnIndex = 0 Then
            Dim rows As DataGridViewRow
            rows = dgvCertificados.Rows(e.RowIndex)

            Dim certificado As SAHCertificados
            certificado = negocioCertificados.getCertificado(rows.Cells(1).Value)

            Dim f As New SAHFrmCertificados(certificado, frmPrincipal, New SAHFrmConsultaCertificados(frmPrincipal, idUsuario), idUsuario)
            frmPrincipal.AbrirFormEnPanel(f)

        End If
    End Sub


End Class
﻿Imports System
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Linq
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Threading.Tasks
Imports System.Windows.Forms

Public Class SAHFrmPrincipal

    Dim negocioCupones As New SAHNegocioCupones
    Dim negocioBloqueos As New SAHNegocioBloqueos
    Dim negocioAhorros As New SAHNegocioAhorros

    Dim lx, ly As Integer
    Dim sw, sh As Integer

    Dim x, y As Integer
    Dim newpoint As Point
    Public idUsuario As Integer = 2


    'Mover Formulario-------------------------------------------------------------------------------------

    Private Sub BarraTitulo_MouseDown(sender As Object, e As MouseEventArgs) Handles BarraTitulo.MouseDown
        x = Control.MousePosition.X - Me.Location.X
        y = Control.MousePosition.Y - Me.Location.Y
    End Sub

    Private Sub BarraTitulo_MouseMove(sender As Object, e As MouseEventArgs) Handles BarraTitulo.MouseMove
        If e.Button = MouseButtons.Left Then
            newpoint = Control.MousePosition
            newpoint.X -= x
            newpoint.Y -= y
            Me.Location = newpoint
            Application.DoEvents()
        End If
    End Sub



    'Movimiento barra menu----------------------------------------------------------------------------------

    Private Sub btnMenu_Click(sender As Object, e As EventArgs) Handles btnMenu.Click
        If panelMenu.Width = 230 Then
            tmOcultarMenu.Enabled = True
        ElseIf (panelMenu.Width = 55) Then
            tmMostrarMenu.Enabled = True
        End If
    End Sub

    Private Sub tmMostrarMenu_Tick(sender As Object, e As EventArgs) Handles tmMostrarMenu.Tick
        If panelMenu.Width >= 230 Then
            tmMostrarMenu.Enabled = False
        Else
            panelMenu.Width = panelMenu.Width + 35
        End If
    End Sub



    Private Sub tmOcultarMenu_Tick(sender As Object, e As EventArgs) Handles tmOcultarMenu.Tick
        If panelMenu.Width <= 55 Then
            tmOcultarMenu.Enabled = False
        Else
            panelMenu.Width = panelMenu.Width - 35
        End If
    End Sub

    'Fecha y hora -----------------------------------------------------------------------------------------------

    Private Sub tmFechaHora_Tick(sender As Object, e As EventArgs) Handles tmFechaHora.Tick
        lbFecha.Text = DateTime.Now.ToShortDateString()
        lblHora.Text = DateTime.Now.ToString("HH:mm:ssss")

    End Sub
    'METODOS PARA CERRAR,MAXIMIZAR, MINIMIZAR FORMULARIO------------------------------------------------------

    Private Sub btnCerrar_Click(sender As Object, e As EventArgs) Handles btnCerrar.Click
        If MsgBox("¿Está seguro que desea salir?", MsgBoxStyle.Question & MsgBoxStyle.YesNo) =
            MsgBoxResult.Yes Then
            End
        End If
    End Sub

    Private Sub btnNormal_Click(sender As Object, e As EventArgs) Handles btnNormal.Click
        Size = New Size(sw, sh)
        Location = New Point(lx, ly)
        btnNormal.Visible = False
        btnMax.Visible = True
    End Sub

    Private Sub btnMinimizar_Click(sender As Object, e As EventArgs) Handles btnMinimizar.Click
        WindowState = FormWindowState.Minimized
    End Sub

    Private Sub btnMax_Click(sender As Object, e As EventArgs) Handles btnMax.Click
        lx = Location.X
        ly = Location.Y
        sw = Size.Width
        sh = Size.Height
        Size = Screen.PrimaryScreen.WorkingArea.Size
        Location = Screen.PrimaryScreen.WorkingArea.Location
        btnMax.Visible = False
        btnNormal.Visible = True
    End Sub


    Public Sub AbrirFormEnPanel(formHijo As Form)
        panelContenedor.Controls.Clear()
        formHijo.TopLevel = False
        formHijo.FormBorderStyle = FormBorderStyle.None
        formHijo.Dock = DockStyle.Fill
        panelContenedor.Controls.Add(formHijo)
        panelContenedor.Tag = formHijo
        formHijo.StartPosition = FormStartPosition.CenterParent
        formHijo.Show()
        panelContenedor.Update()
        panelContenedor.Refresh()
    End Sub

    ' BOTON AHORRO Y SUBMENU
    Private Sub btnAhorros_Click(sender As Object, e As EventArgs) Handles btnAhorros.Click
        submenuAhorro.Show(btnAhorros, 0, btnAhorros.Height)
    End Sub

    'Dim hijoa As FrmAhorro
    Private Sub NuevoAhorroToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles NuevoAhorroToolStripMenuItem.Click
        AbrirFormEnPanel(New SAHFrmAhorro(idUsuario, Me))
    End Sub

    Private Sub ConsultarAhorrosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultarAhorrosToolStripMenuItem.Click
        AbrirFormEnPanel(New SAHFrmConsultaAhorros(Me))
    End Sub

    Private Sub TiposDeAhorroToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles TiposDeAhorroToolStripMenuItem.Click
        If negocioAhorros.verificarUsuarioAdmin(idUsuario) Then
            AbrirFormEnPanel(New SAHFrmTiposAhorros(Me))
        Else
            MsgBox("Estimado usuario, usted no tiene permisos de acceso para esta sección", MsgBoxStyle.Critical, "SAH")

        End If
        
    End Sub

    Private Sub ConsultarTiposDeAhorroToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultarTiposDeAhorroToolStripMenuItem.Click
        AbrirFormEnPanel(New SAHFrmConsultaTiposAhorros(Me, idUsuario))
    End Sub

    ' OTROS BOTONES
    Private Sub btnTasasInteres_Click(sender As Object, e As EventArgs) Handles btnTasasInteres.Click
        AbrirFormEnPanel(New SAHFrmConsultaTasasInteres)
    End Sub

    Private Sub btnBloqueos_Click(sender As Object, e As EventArgs) Handles btnBloqueos.Click
        AbrirFormEnPanel(New SAHFrmConsultaBloqueos)
    End Sub

    Private Sub btnDescuentosCastigos_Click(sender As Object, e As EventArgs) Handles btnDescuentosCastigos.Click
        submenuDescuentosCastigos.Show(btnDescuentosCastigos, 0, btnDescuentosCastigos.Height)

    End Sub

    Private Sub btnCupones_Click(sender As Object, e As EventArgs) Handles btnCupones.Click
        AbrirFormEnPanel(New SAHFrmConsultaCupones)
    End Sub

    Private Sub btnCertificado_Click(sender As Object, e As EventArgs) Handles btnCertificado.Click
        submenuCertificados.Show(btnCertificado, 0, btnCertificado.Height)
    End Sub

    Private Sub NuevoCertificadoToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles NuevoCertificadoToolStripMenuItem1.Click
        AbrirFormEnPanel(New SAHFrmCertificados)
    End Sub

    Private Sub TiposDeCertificadosToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles TiposDeCertificadosToolStripMenuItem2.Click
        AbrirFormEnPanel(New SAHFrmTiposCertificados)
    End Sub

    Private Sub btnReportes_Click(sender As Object, e As EventArgs) Handles btnReportes.Click
        AbrirFormEnPanel(New SAHFrmReporte)
    End Sub

    Private Sub btnInicio_Click(sender As Object, e As EventArgs) Handles btnInicio.Click
        AbrirFormEnPanel(New SAHFrmInicio)
    End Sub

    Private Sub ConsultarCertificados_Click(sender As Object, e As EventArgs) Handles ConsultarCertificados.Click
        AbrirFormEnPanel(New SAHFrmConsultaCertificados(Me, idUsuario))
    End Sub

    Private Sub DescuentosCastigos_Click(sender As Object, e As EventArgs) Handles DescuentosCastigos.Click
        AbrirFormEnPanel(New SAHFrmDescuentosCastigos(Me))
    End Sub

    Private Sub panelContenedor_Paint(sender As Object, e As PaintEventArgs) Handles panelContenedor.Paint

    End Sub

    Private Sub DescuentosCastigosAhorros_Click(sender As Object, e As EventArgs) Handles DescuentosCastigosAhorros.Click
        AbrirFormEnPanel(New SAHFrmConsultaDescuentosCastigosAhorros)
    End Sub

    Private Sub ConsultarTiposDeCertificadosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConsultarTiposDeCertificadosToolStripMenuItem.Click
        AbrirFormEnPanel(New SAHFrmConsultaTiposCertificados(Me))
    End Sub

    Private Sub ConsultarAhorrosToolStripMenuItem_MouseHover(sender As Object, e As EventArgs) Handles ConsultarAhorrosToolStripMenuItem.MouseHover
        ConsultarAhorrosToolStripMenuItem.BackColor = Color.FromArgb(38, 45, 53)
    End Sub

    Private Sub FrmPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Abrir()
        AbrirFormEnPanel(New SAHFrmInicio)
        negocioCupones.generarCupones()
        'negocioBloqueos.generarBloqueo()
    End Sub

End Class
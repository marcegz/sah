﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SAHFrmAhorro
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.label5 = New System.Windows.Forms.Label()
        Me.txtIdAhorro = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtNombreAhorro = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lMonto = New System.Windows.Forms.Label()
        Me.lMontoInteres = New System.Windows.Forms.Label()
        Me.lInteres = New System.Windows.Forms.Label()
        Me.lTiempoDuracion = New System.Windows.Forms.Label()
        Me.lPorcentaje = New System.Windows.Forms.Label()
        Me.lTasaInteres = New System.Windows.Forms.Label()
        Me.cbEstado = New System.Windows.Forms.ComboBox()
        Me.lMontoAhorrado = New System.Windows.Forms.Label()
        Me.lEstado = New System.Windows.Forms.Label()
        Me.cbTipoPago = New System.Windows.Forms.ComboBox()
        Me.lTipoPago = New System.Windows.Forms.Label()
        Me.cbDuracion = New System.Windows.Forms.ComboBox()
        Me.cbCuotas = New System.Windows.Forms.ComboBox()
        Me.cbIntereses = New System.Windows.Forms.ComboBox()
        Me.BtnConsultarBloqueos = New System.Windows.Forms.Button()
        Me.BtnSeleccionarBeneficiarios = New System.Windows.Forms.Button()
        Me.txtMontoCuota = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cbDiaAplicacion = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lDuracion = New System.Windows.Forms.Label()
        Me.dtpFinalizacion = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dtpInicio = New System.Windows.Forms.DateTimePicker()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btnBuscarAutorizado = New System.Windows.Forms.Button()
        Me.txtNombreAutorizado = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCedulaAutorizado = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtNombreAsociado = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.cbMoneda = New System.Windows.Forms.ComboBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cbTiposAhorros = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCedulaAsociado = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnCancelar = New System.Windows.Forms.Button()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.BtnCerrar = New System.Windows.Forms.Button()
        Me.btnActualizar = New System.Windows.Forms.Button()
        Me.btnAtras = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label5.ForeColor = System.Drawing.Color.Black
        Me.label5.Location = New System.Drawing.Point(19, 53)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(81, 21)
        Me.label5.TabIndex = 48
        Me.label5.Text = "ID Ahorro:"
        '
        'txtIdAhorro
        '
        Me.txtIdAhorro.Enabled = False
        Me.txtIdAhorro.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.txtIdAhorro.Location = New System.Drawing.Point(150, 50)
        Me.txtIdAhorro.Name = "txtIdAhorro"
        Me.txtIdAhorro.Size = New System.Drawing.Size(218, 29)
        Me.txtIdAhorro.TabIndex = 47
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(19, 97)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(96, 21)
        Me.Label6.TabIndex = 46
        Me.Label6.Text = "Tipo Ahorro:"
        '
        'txtNombreAhorro
        '
        Me.txtNombreAhorro.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.txtNombreAhorro.Location = New System.Drawing.Point(552, 50)
        Me.txtNombreAhorro.Name = "txtNombreAhorro"
        Me.txtNombreAhorro.Size = New System.Drawing.Size(250, 29)
        Me.txtNombreAhorro.TabIndex = 45
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.lMonto)
        Me.GroupBox1.Controls.Add(Me.lMontoInteres)
        Me.GroupBox1.Controls.Add(Me.lInteres)
        Me.GroupBox1.Controls.Add(Me.lTiempoDuracion)
        Me.GroupBox1.Controls.Add(Me.lPorcentaje)
        Me.GroupBox1.Controls.Add(Me.lTasaInteres)
        Me.GroupBox1.Controls.Add(Me.cbEstado)
        Me.GroupBox1.Controls.Add(Me.lMontoAhorrado)
        Me.GroupBox1.Controls.Add(Me.lEstado)
        Me.GroupBox1.Controls.Add(Me.cbTipoPago)
        Me.GroupBox1.Controls.Add(Me.lTipoPago)
        Me.GroupBox1.Controls.Add(Me.cbDuracion)
        Me.GroupBox1.Controls.Add(Me.cbCuotas)
        Me.GroupBox1.Controls.Add(Me.cbIntereses)
        Me.GroupBox1.Controls.Add(Me.BtnConsultarBloqueos)
        Me.GroupBox1.Controls.Add(Me.BtnSeleccionarBeneficiarios)
        Me.GroupBox1.Controls.Add(Me.txtMontoCuota)
        Me.GroupBox1.Controls.Add(Me.Label15)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.Label13)
        Me.GroupBox1.Controls.Add(Me.cbDiaAplicacion)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.lDuracion)
        Me.GroupBox1.Controls.Add(Me.dtpFinalizacion)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.dtpInicio)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.btnBuscarAutorizado)
        Me.GroupBox1.Controls.Add(Me.txtNombreAutorizado)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.txtCedulaAutorizado)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.txtNombreAsociado)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.cbMoneda)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cbTiposAhorros)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtCedulaAsociado)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtIdAhorro)
        Me.GroupBox1.Controls.Add(Me.txtNombreAhorro)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.label5)
        Me.GroupBox1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Black
        Me.GroupBox1.Location = New System.Drawing.Point(27, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(838, 495)
        Me.GroupBox1.TabIndex = 52
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Ahorro"
        '
        'lMonto
        '
        Me.lMonto.AutoSize = True
        Me.lMonto.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMonto.ForeColor = System.Drawing.Color.Black
        Me.lMonto.Location = New System.Drawing.Point(550, 397)
        Me.lMonto.Name = "lMonto"
        Me.lMonto.Size = New System.Drawing.Size(40, 21)
        Me.lMonto.TabIndex = 98
        Me.lMonto.Text = "0.00"
        Me.lMonto.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lMonto.Visible = False
        '
        'lMontoInteres
        '
        Me.lMontoInteres.AutoSize = True
        Me.lMontoInteres.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMontoInteres.ForeColor = System.Drawing.Color.Black
        Me.lMontoInteres.Location = New System.Drawing.Point(274, 451)
        Me.lMontoInteres.Name = "lMontoInteres"
        Me.lMontoInteres.Size = New System.Drawing.Size(40, 21)
        Me.lMontoInteres.TabIndex = 97
        Me.lMontoInteres.Text = "0.00"
        Me.lMontoInteres.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lMontoInteres.Visible = False
        '
        'lInteres
        '
        Me.lInteres.AutoSize = True
        Me.lInteres.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lInteres.ForeColor = System.Drawing.Color.Black
        Me.lInteres.Location = New System.Drawing.Point(193, 451)
        Me.lInteres.Name = "lInteres"
        Me.lInteres.Size = New System.Drawing.Size(75, 21)
        Me.lInteres.TabIndex = 96
        Me.lInteres.Text = "Intereses:"
        Me.lInteres.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lInteres.Visible = False
        '
        'lTiempoDuracion
        '
        Me.lTiempoDuracion.AutoSize = True
        Me.lTiempoDuracion.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTiempoDuracion.ForeColor = System.Drawing.Color.Black
        Me.lTiempoDuracion.Location = New System.Drawing.Point(550, 352)
        Me.lTiempoDuracion.Name = "lTiempoDuracion"
        Me.lTiempoDuracion.Size = New System.Drawing.Size(0, 21)
        Me.lTiempoDuracion.TabIndex = 95
        Me.lTiempoDuracion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lTiempoDuracion.Visible = False
        '
        'lPorcentaje
        '
        Me.lPorcentaje.AutoSize = True
        Me.lPorcentaje.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lPorcentaje.ForeColor = System.Drawing.Color.Black
        Me.lPorcentaje.Location = New System.Drawing.Point(127, 451)
        Me.lPorcentaje.Name = "lPorcentaje"
        Me.lPorcentaje.Size = New System.Drawing.Size(0, 21)
        Me.lPorcentaje.TabIndex = 94
        Me.lPorcentaje.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lPorcentaje.Visible = False
        '
        'lTasaInteres
        '
        Me.lTasaInteres.AutoSize = True
        Me.lTasaInteres.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTasaInteres.ForeColor = System.Drawing.Color.Black
        Me.lTasaInteres.Location = New System.Drawing.Point(19, 451)
        Me.lTasaInteres.Name = "lTasaInteres"
        Me.lTasaInteres.Size = New System.Drawing.Size(93, 21)
        Me.lTasaInteres.TabIndex = 93
        Me.lTasaInteres.Text = "Tasa Interés:"
        Me.lTasaInteres.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lTasaInteres.Visible = False
        '
        'cbEstado
        '
        Me.cbEstado.AutoCompleteCustomSource.AddRange(New String() {"Planilla", "Depósito"})
        Me.cbEstado.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbEstado.FormattingEnabled = True
        Me.cbEstado.Items.AddRange(New Object() {"Vigente", "Cancelado", "Finalizado", "Completado"})
        Me.cbEstado.Location = New System.Drawing.Point(150, 394)
        Me.cbEstado.Name = "cbEstado"
        Me.cbEstado.Size = New System.Drawing.Size(218, 29)
        Me.cbEstado.TabIndex = 92
        Me.cbEstado.Tag = ""
        Me.cbEstado.Visible = False
        '
        'lMontoAhorrado
        '
        Me.lMontoAhorrado.AutoSize = True
        Me.lMontoAhorrado.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lMontoAhorrado.ForeColor = System.Drawing.Color.Black
        Me.lMontoAhorrado.Location = New System.Drawing.Point(397, 397)
        Me.lMontoAhorrado.Name = "lMontoAhorrado"
        Me.lMontoAhorrado.Size = New System.Drawing.Size(129, 21)
        Me.lMontoAhorrado.TabIndex = 90
        Me.lMontoAhorrado.Text = "Monto Ahorrado:"
        Me.lMontoAhorrado.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lMontoAhorrado.Visible = False
        '
        'lEstado
        '
        Me.lEstado.AutoSize = True
        Me.lEstado.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lEstado.ForeColor = System.Drawing.Color.Black
        Me.lEstado.Location = New System.Drawing.Point(19, 397)
        Me.lEstado.Name = "lEstado"
        Me.lEstado.Size = New System.Drawing.Size(59, 21)
        Me.lEstado.TabIndex = 88
        Me.lEstado.Text = "Estado:"
        Me.lEstado.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lEstado.Visible = False
        '
        'cbTipoPago
        '
        Me.cbTipoPago.AutoCompleteCustomSource.AddRange(New String() {"Planilla", "Depósito"})
        Me.cbTipoPago.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTipoPago.FormattingEnabled = True
        Me.cbTipoPago.Items.AddRange(New Object() {"Planilla", "Depósito"})
        Me.cbTipoPago.Location = New System.Drawing.Point(150, 349)
        Me.cbTipoPago.Name = "cbTipoPago"
        Me.cbTipoPago.Size = New System.Drawing.Size(218, 29)
        Me.cbTipoPago.TabIndex = 87
        Me.cbTipoPago.Tag = ""
        '
        'lTipoPago
        '
        Me.lTipoPago.AutoSize = True
        Me.lTipoPago.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lTipoPago.ForeColor = System.Drawing.Color.Black
        Me.lTipoPago.Location = New System.Drawing.Point(19, 352)
        Me.lTipoPago.Name = "lTipoPago"
        Me.lTipoPago.Size = New System.Drawing.Size(81, 21)
        Me.lTipoPago.TabIndex = 86
        Me.lTipoPago.Text = "Tipo Pago:"
        '
        'cbDuracion
        '
        Me.cbDuracion.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDuracion.FormattingEnabled = True
        Me.cbDuracion.Items.AddRange(New Object() {"3 Meses", "4 Meses", "5 Meses", "6 Meses", "7 Meses", "8 Meses", "9 Meses", "12 Meses", "18 Meses", "24 Meses", "36 Meses", "48 Meses", "60 Meses"})
        Me.cbDuracion.Location = New System.Drawing.Point(552, 349)
        Me.cbDuracion.Name = "cbDuracion"
        Me.cbDuracion.Size = New System.Drawing.Size(250, 29)
        Me.cbDuracion.TabIndex = 85
        Me.cbDuracion.Visible = False
        '
        'cbCuotas
        '
        Me.cbCuotas.AutoCompleteCustomSource.AddRange(New String() {"Colones", "Dólares"})
        Me.cbCuotas.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbCuotas.FormattingEnabled = True
        Me.cbCuotas.Items.AddRange(New Object() {"Semanal", "Quincenal", "Mensual"})
        Me.cbCuotas.Location = New System.Drawing.Point(150, 264)
        Me.cbCuotas.Name = "cbCuotas"
        Me.cbCuotas.Size = New System.Drawing.Size(218, 29)
        Me.cbCuotas.TabIndex = 84
        Me.cbCuotas.Tag = ""
        '
        'cbIntereses
        '
        Me.cbIntereses.AutoCompleteCustomSource.AddRange(New String() {"Colones", "Dólares"})
        Me.cbIntereses.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbIntereses.FormattingEnabled = True
        Me.cbIntereses.Items.AddRange(New Object() {"Mensual", "Trimestral", "Semestral", "Anual"})
        Me.cbIntereses.Location = New System.Drawing.Point(552, 261)
        Me.cbIntereses.Name = "cbIntereses"
        Me.cbIntereses.Size = New System.Drawing.Size(250, 29)
        Me.cbIntereses.TabIndex = 83
        Me.cbIntereses.Tag = ""
        '
        'BtnConsultarBloqueos
        '
        Me.BtnConsultarBloqueos.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnConsultarBloqueos.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.BtnConsultarBloqueos.Cursor = System.Windows.Forms.Cursors.Default
        Me.BtnConsultarBloqueos.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.BtnConsultarBloqueos.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.BtnConsultarBloqueos.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.BtnConsultarBloqueos.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnConsultarBloqueos.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnConsultarBloqueos.ForeColor = System.Drawing.Color.White
        Me.BtnConsultarBloqueos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnConsultarBloqueos.Location = New System.Drawing.Point(610, 451)
        Me.BtnConsultarBloqueos.Name = "BtnConsultarBloqueos"
        Me.BtnConsultarBloqueos.Size = New System.Drawing.Size(192, 30)
        Me.BtnConsultarBloqueos.TabIndex = 82
        Me.BtnConsultarBloqueos.Text = "Consultar Bloqueos"
        Me.BtnConsultarBloqueos.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnConsultarBloqueos.UseVisualStyleBackColor = False
        Me.BtnConsultarBloqueos.Visible = False
        '
        'BtnSeleccionarBeneficiarios
        '
        Me.BtnSeleccionarBeneficiarios.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnSeleccionarBeneficiarios.BackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.BtnSeleccionarBeneficiarios.Cursor = System.Windows.Forms.Cursors.Default
        Me.BtnSeleccionarBeneficiarios.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.BtnSeleccionarBeneficiarios.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.BtnSeleccionarBeneficiarios.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(38, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(53, Byte), Integer))
        Me.BtnSeleccionarBeneficiarios.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnSeleccionarBeneficiarios.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnSeleccionarBeneficiarios.ForeColor = System.Drawing.Color.White
        Me.BtnSeleccionarBeneficiarios.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnSeleccionarBeneficiarios.Location = New System.Drawing.Point(388, 451)
        Me.BtnSeleccionarBeneficiarios.Name = "BtnSeleccionarBeneficiarios"
        Me.BtnSeleccionarBeneficiarios.Size = New System.Drawing.Size(216, 30)
        Me.BtnSeleccionarBeneficiarios.TabIndex = 81
        Me.BtnSeleccionarBeneficiarios.Text = "Seleccionar Beneficiarios"
        Me.BtnSeleccionarBeneficiarios.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnSeleccionarBeneficiarios.UseVisualStyleBackColor = False
        Me.BtnSeleccionarBeneficiarios.Visible = False
        '
        'txtMontoCuota
        '
        Me.txtMontoCuota.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.txtMontoCuota.Location = New System.Drawing.Point(150, 221)
        Me.txtMontoCuota.Name = "txtMontoCuota"
        Me.txtMontoCuota.Size = New System.Drawing.Size(218, 29)
        Me.txtMontoCuota.TabIndex = 76
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.ForeColor = System.Drawing.Color.Black
        Me.Label15.Location = New System.Drawing.Point(19, 229)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(104, 21)
        Me.Label15.TabIndex = 77
        Me.Label15.Text = "Monto Cuota:"
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label14.ForeColor = System.Drawing.Color.Black
        Me.Label14.Location = New System.Drawing.Point(19, 267)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(132, 21)
        Me.Label14.TabIndex = 74
        Me.Label14.Text = "Frecuencia Cuota:"
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.Color.Black
        Me.Label13.Location = New System.Drawing.Point(397, 267)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(153, 21)
        Me.Label13.TabIndex = 72
        Me.Label13.Text = "Frecuencia Intereses:"
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'cbDiaAplicacion
        '
        Me.cbDiaAplicacion.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbDiaAplicacion.FormattingEnabled = True
        Me.cbDiaAplicacion.Items.AddRange(New Object() {"Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"})
        Me.cbDiaAplicacion.Location = New System.Drawing.Point(552, 221)
        Me.cbDiaAplicacion.Name = "cbDiaAplicacion"
        Me.cbDiaAplicacion.Size = New System.Drawing.Size(250, 29)
        Me.cbDiaAplicacion.TabIndex = 70
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Location = New System.Drawing.Point(397, 224)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(132, 21)
        Me.Label12.TabIndex = 69
        Me.Label12.Text = "Día de Aplicación:"
        '
        'lDuracion
        '
        Me.lDuracion.AutoSize = True
        Me.lDuracion.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lDuracion.ForeColor = System.Drawing.Color.Black
        Me.lDuracion.Location = New System.Drawing.Point(397, 352)
        Me.lDuracion.Name = "lDuracion"
        Me.lDuracion.Size = New System.Drawing.Size(76, 21)
        Me.lDuracion.TabIndex = 68
        Me.lDuracion.Text = "Duración:"
        Me.lDuracion.TextAlign = System.Drawing.ContentAlignment.TopCenter
        Me.lDuracion.Visible = False
        '
        'dtpFinalizacion
        '
        Me.dtpFinalizacion.Enabled = False
        Me.dtpFinalizacion.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFinalizacion.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFinalizacion.Location = New System.Drawing.Point(552, 304)
        Me.dtpFinalizacion.Name = "dtpFinalizacion"
        Me.dtpFinalizacion.Size = New System.Drawing.Size(250, 29)
        Me.dtpFinalizacion.TabIndex = 66
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.ForeColor = System.Drawing.Color.Black
        Me.Label10.Location = New System.Drawing.Point(397, 310)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(138, 21)
        Me.Label10.TabIndex = 65
        Me.Label10.Text = "Fecha Finalización:"
        '
        'dtpInicio
        '
        Me.dtpInicio.CalendarFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInicio.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpInicio.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpInicio.Location = New System.Drawing.Point(150, 304)
        Me.dtpInicio.Name = "dtpInicio"
        Me.dtpInicio.Size = New System.Drawing.Size(218, 29)
        Me.dtpInicio.TabIndex = 64
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(19, 310)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(94, 21)
        Me.Label9.TabIndex = 63
        Me.Label9.Text = "Fecha Inicio:"
        '
        'btnBuscarAutorizado
        '
        Me.btnBuscarAutorizado.BackColor = System.Drawing.Color.White
        Me.btnBuscarAutorizado.BackgroundImage = Global.SAH.My.Resources.Resources.lupaaa
        Me.btnBuscarAutorizado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnBuscarAutorizado.Location = New System.Drawing.Point(341, 179)
        Me.btnBuscarAutorizado.Name = "btnBuscarAutorizado"
        Me.btnBuscarAutorizado.Size = New System.Drawing.Size(27, 29)
        Me.btnBuscarAutorizado.TabIndex = 62
        Me.btnBuscarAutorizado.UseVisualStyleBackColor = False
        '
        'txtNombreAutorizado
        '
        Me.txtNombreAutorizado.BackColor = System.Drawing.SystemColors.Window
        Me.txtNombreAutorizado.Enabled = False
        Me.txtNombreAutorizado.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.txtNombreAutorizado.Location = New System.Drawing.Point(552, 180)
        Me.txtNombreAutorizado.Name = "txtNombreAutorizado"
        Me.txtNombreAutorizado.Size = New System.Drawing.Size(250, 29)
        Me.txtNombreAutorizado.TabIndex = 60
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(397, 183)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(151, 21)
        Me.Label7.TabIndex = 61
        Me.Label7.Text = "Nombre Autorizado:"
        '
        'txtCedulaAutorizado
        '
        Me.txtCedulaAutorizado.Enabled = False
        Me.txtCedulaAutorizado.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.txtCedulaAutorizado.Location = New System.Drawing.Point(150, 180)
        Me.txtCedulaAutorizado.Name = "txtCedulaAutorizado"
        Me.txtCedulaAutorizado.Size = New System.Drawing.Size(218, 29)
        Me.txtCedulaAutorizado.TabIndex = 58
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Location = New System.Drawing.Point(19, 183)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(108, 21)
        Me.Label8.TabIndex = 59
        Me.Label8.Text = "ID Autorizado:"
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'txtNombreAsociado
        '
        Me.txtNombreAsociado.BackColor = System.Drawing.SystemColors.Window
        Me.txtNombreAsociado.Enabled = False
        Me.txtNombreAsociado.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.txtNombreAsociado.Location = New System.Drawing.Point(552, 136)
        Me.txtNombreAsociado.Name = "txtNombreAsociado"
        Me.txtNombreAsociado.Size = New System.Drawing.Size(250, 29)
        Me.txtNombreAsociado.TabIndex = 56
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(397, 140)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(138, 21)
        Me.Label4.TabIndex = 57
        Me.Label4.Text = "Nombre Asociado:"
        '
        'cbMoneda
        '
        Me.cbMoneda.AutoCompleteCustomSource.AddRange(New String() {"Colones", "Dólares"})
        Me.cbMoneda.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbMoneda.FormattingEnabled = True
        Me.cbMoneda.Items.AddRange(New Object() {"Colones", "Dólares"})
        Me.cbMoneda.Location = New System.Drawing.Point(552, 94)
        Me.cbMoneda.Name = "cbMoneda"
        Me.cbMoneda.Size = New System.Drawing.Size(250, 29)
        Me.cbMoneda.TabIndex = 55
        Me.cbMoneda.Tag = ""
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(397, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(125, 21)
        Me.Label3.TabIndex = 54
        Me.Label3.Text = "Tipo de Moneda:"
        '
        'cbTiposAhorros
        '
        Me.cbTiposAhorros.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbTiposAhorros.FormattingEnabled = True
        Me.cbTiposAhorros.Location = New System.Drawing.Point(150, 94)
        Me.cbTiposAhorros.Name = "cbTiposAhorros"
        Me.cbTiposAhorros.Size = New System.Drawing.Size(218, 29)
        Me.cbTiposAhorros.TabIndex = 53
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(397, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(149, 21)
        Me.Label2.TabIndex = 52
        Me.Label2.Text = "Nombre del Ahorro:"
        '
        'txtCedulaAsociado
        '
        Me.txtCedulaAsociado.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.txtCedulaAsociado.Location = New System.Drawing.Point(150, 136)
        Me.txtCedulaAsociado.Name = "txtCedulaAsociado"
        Me.txtCedulaAsociado.Size = New System.Drawing.Size(218, 29)
        Me.txtCedulaAsociado.TabIndex = 50
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(19, 140)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(95, 21)
        Me.Label1.TabIndex = 51
        Me.Label1.Text = "ID Asociado:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'btnCancelar
        '
        Me.btnCancelar.BackColor = System.Drawing.Color.FromArgb(CType(CType(13, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(142, Byte), Integer))
        Me.btnCancelar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnCancelar.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnCancelar.FlatAppearance.BorderSize = 0
        Me.btnCancelar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnCancelar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCancelar.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnCancelar.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCancelar.Location = New System.Drawing.Point(471, 519)
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(150, 36)
        Me.btnCancelar.TabIndex = 93
        Me.btnCancelar.Text = "Cancelar"
        Me.btnCancelar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCancelar.UseVisualStyleBackColor = False
        '
        'btnGuardar
        '
        Me.btnGuardar.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnGuardar.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnGuardar.FlatAppearance.BorderSize = 0
        Me.btnGuardar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnGuardar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnGuardar.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnGuardar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnGuardar.Location = New System.Drawing.Point(245, 519)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(150, 36)
        Me.btnGuardar.TabIndex = 92
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnGuardar.UseVisualStyleBackColor = False
        '
        'BtnCerrar
        '
        Me.BtnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.BtnCerrar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BtnCerrar.FlatAppearance.BorderSize = 0
        Me.BtnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnCerrar.Image = Global.SAH.My.Resources.Resources.Cerrar
        Me.BtnCerrar.Location = New System.Drawing.Point(862, 3)
        Me.BtnCerrar.Name = "BtnCerrar"
        Me.BtnCerrar.Size = New System.Drawing.Size(32, 30)
        Me.BtnCerrar.TabIndex = 80
        Me.BtnCerrar.UseVisualStyleBackColor = True
        Me.BtnCerrar.Visible = False
        '
        'btnActualizar
        '
        Me.btnActualizar.BackColor = System.Drawing.Color.FromArgb(CType(CType(214, Byte), Integer), CType(CType(61, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.btnActualizar.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnActualizar.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnActualizar.FlatAppearance.BorderSize = 0
        Me.btnActualizar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnActualizar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.btnActualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnActualizar.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnActualizar.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnActualizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnActualizar.Location = New System.Drawing.Point(245, 519)
        Me.btnActualizar.Name = "btnActualizar"
        Me.btnActualizar.Size = New System.Drawing.Size(150, 36)
        Me.btnActualizar.TabIndex = 94
        Me.btnActualizar.Text = "Editar"
        Me.btnActualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnActualizar.UseVisualStyleBackColor = False
        Me.btnActualizar.Visible = False
        '
        'btnAtras
        '
        Me.btnAtras.BackColor = System.Drawing.Color.FromArgb(CType(CType(13, Byte), Integer), CType(CType(93, Byte), Integer), CType(CType(142, Byte), Integer))
        Me.btnAtras.Cursor = System.Windows.Forms.Cursors.Default
        Me.btnAtras.FlatAppearance.BorderColor = System.Drawing.Color.DimGray
        Me.btnAtras.FlatAppearance.BorderSize = 0
        Me.btnAtras.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(69, Byte), Integer), CType(CType(76, Byte), Integer))
        Me.btnAtras.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(4, Byte), Integer), CType(CType(41, Byte), Integer), CType(CType(68, Byte), Integer))
        Me.btnAtras.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAtras.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.btnAtras.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnAtras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnAtras.Location = New System.Drawing.Point(471, 519)
        Me.btnAtras.Name = "btnAtras"
        Me.btnAtras.Size = New System.Drawing.Size(150, 36)
        Me.btnAtras.TabIndex = 95
        Me.btnAtras.Text = "Atrás"
        Me.btnAtras.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnAtras.UseVisualStyleBackColor = False
        Me.btnAtras.Visible = False
        '
        'SAHFrmAhorro
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(896, 567)
        Me.Controls.Add(Me.btnAtras)
        Me.Controls.Add(Me.btnActualizar)
        Me.Controls.Add(Me.btnCancelar)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.BtnCerrar)
        Me.Name = "SAHFrmAhorro"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ahorro"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents label5 As Label
    Public WithEvents txtIdAhorro As TextBox
    Private WithEvents Label6 As Label
    Public WithEvents txtNombreAhorro As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents cbTiposAhorros As ComboBox
    Private WithEvents Label2 As Label
    Public WithEvents txtCedulaAsociado As TextBox
    Private WithEvents Label1 As Label
    Public WithEvents txtNombreAsociado As TextBox
    Private WithEvents Label4 As Label
    Friend WithEvents cbMoneda As ComboBox
    Private WithEvents Label3 As Label
    Public WithEvents txtNombreAutorizado As TextBox
    Private WithEvents Label7 As Label
    Public WithEvents txtCedulaAutorizado As TextBox
    Private WithEvents Label8 As Label
    Private WithEvents Label13 As Label
    Friend WithEvents cbDiaAplicacion As ComboBox
    Private WithEvents Label12 As Label
    Private WithEvents lDuracion As Label
    Friend WithEvents dtpFinalizacion As DateTimePicker
    Private WithEvents Label10 As Label
    Friend WithEvents dtpInicio As DateTimePicker
    Private WithEvents Label9 As Label
    Public WithEvents txtMontoCuota As TextBox
    Private WithEvents Label15 As Label
    Private WithEvents Label14 As Label
    Friend WithEvents btnBuscarAutorizado As Button
    Private WithEvents btnCancelar As Button
    Private WithEvents btnGuardar As Button
    Private WithEvents BtnConsultarBloqueos As Button
    Private WithEvents BtnSeleccionarBeneficiarios As Button
    Private WithEvents BtnCerrar As Button
    Friend WithEvents cbCuotas As ComboBox
    Friend WithEvents cbIntereses As ComboBox
    Friend WithEvents cbTipoPago As ComboBox
    Private WithEvents lTipoPago As Label
    Private WithEvents lMontoAhorrado As Label
    Private WithEvents lEstado As Label
    Private WithEvents btnActualizar As Button
    Friend WithEvents cbEstado As ComboBox
    Private WithEvents btnAtras As Button
    Private WithEvents lPorcentaje As Label
    Private WithEvents lTasaInteres As Label
    Private WithEvents lTiempoDuracion As Label
    Friend WithEvents cbDuracion As ComboBox
    Private WithEvents lInteres As Label
    Private WithEvents lMontoInteres As Label
    Private WithEvents lMonto As Label
End Class

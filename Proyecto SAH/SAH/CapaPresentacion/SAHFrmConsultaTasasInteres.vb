﻿Public Class SAHFrmConsultaTasasInteres

    Dim f As New SAHFrmPrincipal
    Dim negocioTasasInteres As New SAHNegocioTasasInteres

    Private Sub FrmConsultaTasasInteres_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cargarTablaTasasInteres()
    End Sub


    Sub cargarTablaTasasInteres()
        dgvTasasInteres.DataSource = negocioTasasInteres.getTablaTasasInteres(txtConsultar.Text)
        modificaAtributosTabla()
    End Sub

    Sub modificaAtributosTabla()
        dgvTasasInteres.Columns(0).HeaderText = "Rango Días"
        dgvTasasInteres.Columns(1).HeaderText = "Rango Meses"
        dgvTasasInteres.Columns(2).HeaderText = "Porcentaje"
        dgvTasasInteres.Columns(3).HeaderText = "Nombre"
    End Sub

    Private Sub txtConsultar_KeyUp(sender As Object, e As KeyEventArgs) Handles txtConsultar.KeyUp
        cargarTablaTasasInteres()
    End Sub

    Private Sub btnGenerarReporte_Click(sender As Object, e As EventArgs) Handles btnGenerarReporte.Click

        Dim reporte As New SAHReporteAhorros(txtConsultar.Text)
        reporte.Show()


    End Sub

End Class